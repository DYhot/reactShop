import arrayTreeFilter from "array-tree-filter";
import areaData from "@/utils/area";
import { validateEmail, validatePhone } from "@/utils/validate";

/**
 *
 * 格式化字符串*显示
 * @param {string} {string}字符串
 * @returns {string}
 */
export function encryptionString(str) {
    if (validatePhone(str)) {
        return str.replace(
            new RegExp("(\\d{3})\\d{4}(\\d{4})", "gm"),
            "$1****$2"
        );
    }
    if (validateEmail(str)) {
        return str.replace(/(.{2}).+(.{2}@.+)/g, "$1****$2");
    }
}
/**
 *
 * 格式化金额显示
 * @param {string} {amount}金额
 * @returns {string}
 */
export function formateAmount(amount) {
    let d = amount;
    d = Math.round(d * 100) / 100;
    const price = (d + "").split(".");
    price[1] = price[1] ? `${(price[1] + "000").substring(0, 2)}` : "00";
    d = price.join(".");
    toFixed(d, 2);
    return d;
}
// toFixed 修复js精度丢失问题
export function toFixed(num, s) {
    var times = Math.pow(10, s);
    var des = num * times + 0.5;
    des = parseInt(des, 10) / times;
    return des + "";
}
/**
 *
 * 格式化省市区地址
 * @param {string} {area}地址id
 * @returns {string}
 */
export function formateArea(area) {
    const value = area.split(",");
    if (!value) {
        return "";
    }
    const treeChildren = arrayTreeFilter(
        areaData,
        (c, level) => c.value === value[level]
    );
    return treeChildren.map(v => v.label).join("");
}
/**
 *
 * 获取页面参数path?key1=value1&key2=value2
 * @param {string} {paramName}参数名称
 * @returns {string}
 */
export function GetUrlParam(url, paramName) {
    var arr = url.split("?");

    if (arr.length > 1) {
        var paramArr = arr[1].split("&");
        var arr;
        for (var i = 0; i < paramArr.length; i++) {
            arr = paramArr[i].split("=");

            if (arr != null && arr[0] == paramName) {
                return arr[1];
            }
        }
        return "";
    } else {
        return "";
    }
}
