
/**
 *
 * 图片压缩
 * @param {string} {files}图片文件数组
 * @returns {string}
 */
export function zipImage(files) {
    if (Object.prototype.toString.call(files) !== "[object Array]") {
        return;
    }
    return new Promise((resolve, reject) => {
        let createPromise = key => {
            return new Promise((resolve, reject) => {
                let fReader = new FileReader();
                fReader.readAsDataURL(files[key]);
                fReader.onload = e => {
                    zip(e.target.result, files[key].name).then(data => {
                        resolve(data);
                    }); //压缩逻辑
                };
            }).catch(err => {
                console.error(`${files[key].name}压缩错误,${err}`);
            });
        };
        var promiseArray = [];
        for (var i = 0, len = files.length; i < len; i++) {
            promiseArray.push(createPromise(i));
        }
        Promise.all(promiseArray)
            .then(data => {
                resolve(data);
            })
            .catch(err => {
                console.error(`图片循环压缩错误,${err}`);
            });
    });
}
/**
 *
 * 图片压缩
 * @param {string} {base64}图片文件base64
 * @returns {string}
 */
export function zip(base64, filename) {
    let img = new Image();
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");
    let compressionRatio = 0.5;
    //获取用户拍摄图片的旋转角度
    let orientation = getOrientation(base64ToArrayBuffer(base64)); //1 0°  3 180°  6 90°  8 -90°
    img.src = base64;
    return new Promise((resolve, reject) => {
        img.onload = () => {
            let width = img.width,
                height = img.height;
            //图片旋转到 正向
            if (orientation == 3) {
                canvas.width = width;
                canvas.height = height;
                ctx.rotate(Math.PI);
                ctx.drawImage(img, -width, -height, width, height);
            } else if (orientation == 6) {
                canvas.width = height;
                canvas.height = width;
                ctx.rotate(Math.PI / 2);
                ctx.drawImage(img, 0, -height, width, height);
            } else if (orientation == 8) {
                canvas.width = height;
                canvas.height = width;
                ctx.rotate(-Math.PI / 2);
                ctx.drawImage(img, -width, 0, width, height);
            } else {
                //不旋转原图
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
            }
            //第一次粗压缩
            let base64 = canvas.toDataURL("image/jpeg", compressionRatio); //0.1-表示将原图10M变成1M 10-表示将原图1M变成10M
            //100保证图片容量 0.05保证不失真
            console.log(
                "第一次粗压缩",
                base64.length / 1024,
                "kb，压缩率",
                compressionRatio
            );
            //第二次细压缩
            while (base64.length / 1024 > 300 && compressionRatio > 0.01) {
                console.log("while");
                compressionRatio -= 0.01;
                base64 = canvas.toDataURL("image/jpeg", compressionRatio); //0.1-表示将原图10M变成1M 10-表示将原图1M变成10M
                console.log(
                    "第二次细压缩",
                    base64.length / 1024,
                    "kb，压缩率",
                    compressionRatio
                );
            }
            resolve(dataURLtoFile(base64, filename));
        };
    }).catch(err => {
        console.error(`图片压缩错误2,${err}`);
    });
}
/**
 * 获取jpg图片的exif的角度
 * @param
 * @return
 */
export function getOrientation(arrayBuffer) {
    var dataView = new DataView(arrayBuffer);
    var length = dataView.byteLength;
    var orientation;
    var exifIDCode;
    var tiffOffset;
    var firstIFDOffset;
    var littleEndian;
    var endianness;
    var app1Start;
    var ifdStart;
    var offset;
    var i;
    // // Only handle JPEG image (start by 0xFFD8)
    if (dataView.getUint8(0) === 0xff && dataView.getUint8(1) === 0xd8) {
        offset = 2;
        while (offset < length) {
            if (
                dataView.getUint8(offset) === 0xff &&
                dataView.getUint8(offset + 1) === 0xe1
            ) {
                app1Start = offset;
                break;
            }
            offset++;
        }
    }
    if (app1Start) {
        exifIDCode = app1Start + 4;
        tiffOffset = app1Start + 10;
        if (getStringFromCharCode(dataView, exifIDCode, 4) === "Exif") {
            endianness = dataView.getUint16(tiffOffset);
            littleEndian = endianness === 0x4949;
            if (littleEndian || endianness === 0x4d4d /* bigEndian */) {
                if (
                    dataView.getUint16(tiffOffset + 2, littleEndian) === 0x002a
                ) {
                    firstIFDOffset = dataView.getUint32(
                        tiffOffset + 4,
                        littleEndian
                    );
                    if (firstIFDOffset >= 0x00000008) {
                        ifdStart = tiffOffset + firstIFDOffset;
                    }
                }
            }
        }
    }
    if (ifdStart) {
        length = dataView.getUint16(ifdStart, littleEndian);
        for (i = 0; i < length; i++) {
            offset = ifdStart + i * 12 + 2;
            if (
                dataView.getUint16(offset, littleEndian) ===
                0x0112 /* Orientation */
            ) {
                // 8 is the offset of the current tag's value
                offset += 8;
                // Get the original orientation value
                orientation = dataView.getUint16(offset, littleEndian);
                // Override the orientation with its default value for Safari (#120)
                if (true) {
                    dataView.setUint16(offset, 1, littleEndian);
                }
                break;
            }
        }
    }
    return orientation;
}
/**
 * Unicode码转字符串  ArrayBuffer对象 Unicode码转字符串
 * @param
 * @return
 */
export function getStringFromCharCode(dataView, start, length) {
    var str = "";
    var i;
    for (i = start, length += start; i < length; i++) {
        str += String.fromCharCode(dataView.getUint8(i));
    }
    return str;
}
/**
 * base64转ArrayBuffer对象
 * @param base64
 * @return buffer
 */
export function base64ToArrayBuffer(base64) {
    base64 = base64.replace(/^data\:([^\;]+)\;base64,/gim, "");
    var binary = atob(base64);
    var len = binary.length;
    var buffer = new ArrayBuffer(len);
    var view = new Uint8Array(buffer);
    for (var i = 0; i < len; i++) {
        view[i] = binary.charCodeAt(i);
    }
    return buffer;
}
/**
 *
 * 图片转为base64
 * @param {string} {url}图片路径
 * @returns {string}
 */
export function getImgBase64(url) {
    var base64 = "";
    var img = new Image();
    img.src = url;
    img.onload = () => {
        base64 = getBase64Image(img);
        // console.log(base64);
    };
}
export function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, img.width, img.height);
    var ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
    var dataURL = canvas.toDataURL("image/" + ext);
    return dataURL;
}
/**
 *
 * base64转为file文件
 * base64传入后台太大，将base64转为file文件传入后台
 * @param {string} {dataurl}base64码
 * @param {string} {filename}文件名
 * @returns {string}
 */
export function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(","),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);
    while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
}