// import Cookies from './node_modules/js-cookie'

const TokenKey = 'loginToken'
const inFifteenMinutes = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)

// export function getCookie(param) {
//   if (!param) {
//     return Cookies.get(TokenKey)
//   }else{
//     return Cookies.get(param)
//   }
// }

// export function setCookie(name,param) {
//   if (!name) {
//     return Cookies.set(TokenKey, param, {expires: inFifteenMinutes})
//   }else{
//     return Cookies.set(name, param, {expires: inFifteenMinutes})
//   }
// }

// export function removeCookie(name) {
//     if (!name) {
//         return Cookies.remove(TokenKey)
//     }else{
//         return Cookies.remove(name)
//     }
// }

export function setStorage(name,value) {
  if(!window.localStorage){
    alert("浏览器不支持localstorage");
    return false;
  }else{
    var storage=window.localStorage;
   return storage.setItem(name,value);
  }
}
export function getStorage(name) {
  if(!window.localStorage){
    alert("浏览器不支持localstorage");
    return false;
  }else if(!name){
    var storage=window.localStorage;
    return storage.getItem(TokenKey);
  }else{
    var storage=window.localStorage;
    return storage.getItem(name);
  }
}
