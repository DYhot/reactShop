/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "../styles/openMember.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import Loading from "@/components/Loading/Loading";
import { List, Checkbox, Modal, Button, Toast } from "antd-mobile";
import { getUserInfo, openGreenCard } from "@/api/user";
const CheckboxItem = Checkbox.CheckboxItem;
const alert = Modal.alert;
import DefaultImage from "@/components/DefaultImage/index";
import { encryptionString } from "@/utils/tool";

class OpenMember extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            btnLoading: false,
            greenType: null,
            payType: null,
            gender:null,
            userName: "",
            headImg: "",
            green_card_time: "",
            isOpenGreenCard: false,
            totalAmount: 0,
            greenTypeData: [
                {
                    id: 0,
                    label: "年卡.365天",
                    describe: "相当于约0.24元/天",
                    originalPrice: "176",
                    presentPrice: "88",
                    foldNumber: "5",
                    selected: true
                },
                {
                    id: 1,
                    label: "季卡.90天",
                    describe: "相当于约0.33元/天",
                    originalPrice: "44",
                    presentPrice: "30.8",
                    foldNumber: "7",
                    selected: false
                }
            ],
            payTypeData: [
                {
                    id: 0,
                    label: "微信支付",
                    icon: "#iconweixinzhifu",
                    selected: true
                },
                {
                    id: 1,
                    label: "支付宝支付",
                    icon: "#iconzhifubaozhifu",
                    selected: false
                }
            ]
        };
    }

    componentDidMount() {
        this.getUserInfoData();
        this.setState({
            payType: this.state.payTypeData[0],
            greenType: this.state.greenTypeData[0],
            totalAmount: this.state.greenTypeData[0].presentPrice
        });
    }
    getUserInfoData() {
        Loading.show();
        getUserInfo().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    gender:data.user_gender,
                    userName: data.user_name,
                    isOpenGreenCard: data.green_card,
                    headImg: data.user_head_img,
                    green_card_time: data.green_card_time
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    payTypeChange = val => {
        const { payTypeData } = this.state;
        let payType;
        payTypeData.forEach((item, index) => {
            if (item.id == val) {
                item.selected = true;
                payType = item;
            } else {
                item.selected = false;
            }
        });
        this.setState({
            payTypeData,
            payType
        });
    };
    greenCardChange = val => {
        const { greenTypeData } = this.state;
        let greenType, totalAmount;
        greenTypeData.forEach((item, index) => {
            if (item.id == val) {
                item.selected = true;
                greenType = item;
                totalAmount = item.presentPrice;
            } else {
                item.selected = false;
            }
        });
        this.setState({
            greenTypeData,
            greenType,
            totalAmount
        });
    };
    sureSubmit = () => {
        const { greenType, payType, totalAmount } = this.state;
        const alertInstance = alert("", "确定立即开通绿卡?", [
            {
                text: "取消",
                onPress: () => {},
                style: "default"
            },
            {
                text: "确定",
                onPress: () => {
                    alertInstance.close();
                    Loading.show();
                    const params = {
                        payId: payType.id,
                        greenCardId: greenType.id,
                        totalAmount
                    };
                    openGreenCard(params).then(res => {
                        Loading.hide();
                        if (!res) {
                            return;
                        }
                        if (res.code === 0) {
                            this.props.history.push({
                                pathname: "/result",
                                state: { payType, totalAmount }
                            });
                        } else {
                            Toast.fail(res.msg, 3);
                        }
                    });
                }
            }
        ]);
    };
    render() {
        const {
            greenTypeData,
            payTypeData,
            btnLoading,
            gender,
            userName,
            headImg,
            green_card_time,
            isOpenGreenCard
        } = this.state;
        return (
            <div className="openMember-wrapper">
                <div className="head-wrapper">
                    <div className="left">
                        {headImg ? (
                            <DefaultImage src={headImg} />
                        ) : gender == 1 ? (
                            <CustomIcon
                                icon="#iconicon-test"
                                width="45px"
                                height="45px"
                                color="#fff"
                            />
                        ) : (
                            <CustomIcon
                                icon="#iconicon-test1"
                                width="45px"
                                height="45px"
                                color="#fff"
                            />
                        )}
                    </div>
                    <div className="right">
                        <p>{encryptionString(userName)}</p>
                        {isOpenGreenCard == 0 ? <p>未开通绿卡</p> : null}
                        {isOpenGreenCard == 1 ? (
                            <p>
                                <span>有效期:{green_card_time}</span>
                            </p>
                        ) : null}
                    </div>
                </div>
                <div className="introduce-wrapper">
                    <div className="title">
                        开通绿卡享三大权益,预计每单省6.66元
                    </div>
                    <div className="van-grid">
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#iconvip"
                                    width="35px"
                                    height="35px"
                                    color="#00c35d"
                                />
                            </span>
                            <span className="bottom-title">专享券</span>
                        </div>
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#iconyouhui-"
                                    width="35px"
                                    height="35px"
                                    color="#00c35d"
                                />
                            </span>
                            <span className="bottom-title">专享特价</span>
                        </div>
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#iconiconset0219"
                                    width="35px"
                                    height="35px"
                                    color="#00c35d"
                                />
                            </span>
                            <span className="bottom-title">2倍积分</span>
                        </div>
                    </div>
                </div>
                <List className="green-style">
                    {greenTypeData.map(item => (
                        <CheckboxItem
                            key={item.id}
                            defaultChecked={item.selected}
                            checked={item.selected}
                            onChange={() => this.greenCardChange(item.id)}
                        >
                            <span className="foldNumber">
                                {item.foldNumber}折
                            </span>
                            <span className="label">{item.label}</span>
                            <span className="describe">{item.describe}</span>
                            <p className="price">
                                <span>￥{item.presentPrice}</span>
                                <del>￥{item.originalPrice}</del>
                            </p>
                        </CheckboxItem>
                    ))}
                </List>
                <List renderHeader={() => "支付方式"} className="pay-style">
                    {payTypeData.map(i => (
                        <CheckboxItem
                            key={i.id}
                            defaultChecked={i.selected}
                            checked={i.selected}
                            onChange={() => this.payTypeChange(i.id)}
                        >
                            <span className="icon_wrapper">
                                <CustomIcon
                                    icon={i.icon}
                                    width="20px"
                                    height="20px"
                                    color="#00D15B"
                                />
                            </span>
                            <span className="checkBox_text">{i.label}</span>
                        </CheckboxItem>
                    ))}
                </List>
                <div className="button-wrapper">
                    <Button
                        type="primary"
                        disabled={btnLoading}
                        loading={btnLoading}
                        onClick={() => this.sureSubmit()}
                    >
                        立即支付
                    </Button>
                </div>
            </div>
        );
    }
}

export default OpenMember;
