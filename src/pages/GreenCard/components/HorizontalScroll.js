import React from "react";
import BScroll from "better-scroll";
import "../styles/horizontalScroll.scss";
import { getCategoryMenu } from "@/api/category";

class HorizontalScroll extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            total: 0,
            currentTab: "",
            goodsListArray: [],
            goodsTotal: 0,
            activeTab: null,
            tabsArray: []
        };
    }

    componentDidMount() {
        this.props.onRef(this);
        const wrapper = document.querySelector(".wrapper");
        //选中DOM中定义的 .wrapper 进行初始化
        const scroll = new BScroll(wrapper, {
            scrollX: true, //开启横向滚动
            click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
            scrollY: false //关闭竖向滚动
        });
        // this.getCategoryData();
    }
    getCategoryData() {
        const { categoryId } = this.props;
        // this.setLoading(true);
        getCategoryMenu().then(res => {
            // Toast.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                if (res.data.records.length > 0) {
                    let tabs = [];
                    data.forEach((item, index) => {
                        const target = {
                            title: item.categoryName,
                            key: item.id
                        };
                        tabs.push(target);
                    });
                    console.log(666, categoryId);
                    this.setState(
                        {
                            activeTab: categoryId ? categoryId : tabs[0].key,
                            total: res.data.total,
                            tabsArray: tabs
                        },
                        () => {
                            this.props.getGoodsByCategoryIdData(
                                this.state.activeTab
                            );
                        }
                    );
                }
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    tabClick = row => {
        const {activeTab}=this.state;
        if(row.key==activeTab){
            return;
        }
        this.setState({
            activeTab: row.key
        });
        this.props.getGoodsByCategoryIdData(row.key);
    };
    componentWillUnmount() {
        //处理逻辑
        console.log("组件卸载");
    }
    renderContent = () => {
        const { activeTab, tabsArray } = this.state;
        return tabsArray.length > 0
            ? tabsArray.map((item, index) => {
                  return (
                      <li
                          key={item.key}
                          className={`item ${
                              activeTab == item.key ? "tab-active" : ""
                          }`}
                          onClick={() => {
                              this.tabClick(item);
                          }}
                      >
                          {item.title}
                      </li>
                  );
              })
            : null;
    };

    render() {
        return (
            <div className="wrapper horizontalScroll-wrapper">
                <ul className="content">{this.renderContent()}</ul>
            </div>
        );
    }
}

export default HorizontalScroll;
