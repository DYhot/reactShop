import React from "react";
import "./styles/index.scss";
import { Button, WhiteSpace, Toast } from "antd-mobile";
import CustomIcon from "@/components/Icon/CustomIcon";
import rocketsURL from "@/assets/images/mine/rockets.png";
import HorizontalScroll from "./components/HorizontalScroll";
import { getGoodsByCategoryId } from "@/api/category";
import { addGoodToCart } from "@/api/cart";
import { getCouponList ,getCoupon} from "@/api/coupon";
import IconCart from "@/components/Icon/IconCart";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";

class GreenCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageNum: 0,
            pageSize: 10,
            total: 0,
            current:1,
            size:100,
            currentTab: "",
            goodsListArray: [],
            goodsTotal: 0,
            showLoading: false,
            categoryId: null,
            showButton: false,
            weekly_total: 0,
            everyDay_total: 0,
            weeklyListArray: [],
            everyDayListArray: []
        };
    }
    componentDidMount() {
        this.getCouponListData();
        // 添加滚动监听
        window.addEventListener("scroll", this.getScrollTop);
    }
    // 组件将要卸载，取消监听window滚动事件
    componentWillUnmount() {
        window.removeEventListener("scroll", this.getScrollTop);
    }
    getScrollTop = () => {
        const scrollTop =
            window.pageYOffset ||
            document.documentElement.scrollTop ||
            document.body.scrollTop;
        const showButton = scrollTop > 550 ? true : false;
        this.setState({
            showButton
        });
    };
    onRef = ref => {
        this.child = ref;
    };
    openGreenCard() {
        this.props.history.push("/greenCard/openMember");
    }
    getCouponListData() {
        const {current,size}=this.state;
        const params={
            current,
            size,
        }
        Loading.show();
        getCouponList(params).then(res => {
            Loading.hide();
            this.child.getCategoryData();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    weekly_total: res.data.weekly_total,
                    everyDay_total: res.data.everyDay_total,
                    weeklyListArray: data ? data.weekly : [],
                    everyDayListArray: data ? data.everyDay : []
                });
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    getGoodsByCategoryIdData(id) {
        const { pageNum,pageSize } = this.state;
        let params = {
            id,
            current: pageNum + 1,
            size: pageSize
        };
        Loading.show();
        getGoodsByCategoryId(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    goodsTotal: res.data.total,
                    goodsListArray: data ? data : []
                });
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    skipToDetail(id) {
        this.props.history.push({ pathname: `/detail/${id}` });
    }
    renderContent = tab => {
        const { value, currentTab, goodsListArray } = this.state;
        // console.log(123, tab,currentTab.key,tab.key);
        return (
            <div
                style={{
                    display: "flex",
                    alignItems: "flex-start",
                    justifyContent: "center",
                    backgroundColor: "#fff"
                }}
                className="good_list_wrapper"
            >
                {goodsListArray.map(i => (
                    <div
                        className="list_item"
                        key={i.id}
                        onClick={() => {
                            this.skipToDetail(i.id);
                        }}
                    >
                        <div className="list_item_left">
                            <DefaultImage src={i.url} />
                        </div>
                        <div className="list_item_right">
                            <div className="right_top">
                                <p className="title">{i.title}</p>
                                <p className="describe">{i.describe}</p>
                            </div>
                            <div className="right_bottom">
                                <p className="amount">
                                    <span>￥{i.price}</span>
                                    <span
                                        onClick={e => {
                                            e.nativeEvent.stopImmediatePropagation();
                                            e.stopPropagation();
                                            this.props.history.push(
                                                "/greenCard/openMember"
                                            );
                                        }}
                                    >
                                        开通绿卡
                                    </span>
                                </p>
                                <div
                                    className="iconCartWrapper"
                                    onClick={e => this.addToCart(i, e)}
                                >
                                    {i.count > 0 ? (
                                        <IconCart />
                                    ) : (
                                        <span className="sold-out">售罄</span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        );
    };
    addToCart = (item, e) => {
        e.nativeEvent.stopImmediatePropagation();
        e.stopPropagation();
        if(item.count<1){
            return;
        }
        Loading.show();
        addGoodToCart({ id: item.id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("成功加入购物车", 1);
            } else {
                Toast.info(res.msg, 3);
            }
        });
    };
    couponClick = item => {
        Loading.show();        
        getCoupon({ couponId: item.id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("领取成功", 1);
            } else {
                Toast.info(res.msg, 1);
            }
        });
    };
    render() {
        const {
            showLoading,
            categoryId,
            showButton,
            weeklyListArray,
            everyDayListArray
        } = this.state;
        return (
            <div className="greenCard_wrapper">
                <div className="vip_bg">
                    <DefaultImage
                        src={require("../../assets/images/mine/vip.png")}
                    />
                </div>
                <div className="vipPrivilege">
                    <p className="title">绿卡特权</p>
                    <div className="van-grid">
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#iconvip"
                                    width="25px"
                                    height="25px"
                                    color="#000"
                                />
                            </span>
                            <span className="bottom-title">专享券</span>
                        </div>
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#iconyouhui-"
                                    width="26px"
                                    height="26px"
                                    color="#000"
                                />
                            </span>
                            <span className="bottom-title">专享特价</span>
                        </div>
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#iconiconset0219"
                                    width="23px"
                                    height="23px"
                                    color="#000"
                                />
                            </span>
                            <span className="bottom-title">2倍积分</span>
                        </div>
                        <div className="van-grid-item">
                            <span className="van-grid-icon">
                                <CustomIcon
                                    icon="#icon19"
                                    width="25px"
                                    height="25px"
                                    color="#000"
                                />
                            </span>
                            <span className="bottom-title">更多特权</span>
                        </div>
                    </div>
                    <div className="am-btn-wrapper">
                        <Button onClick={() => this.openGreenCard()}>
                            <span className="bottom-title">5折开通绿卡</span>
                        </Button>
                    </div>
                    <div className="coupons">
                        <p className="coupons-title">
                            <span className="number">1</span>
                            <span className="desc">
                                绿卡专享券 天天领取优惠
                            </span>
                        </p>
                        <p className="todayCouns">
                            <span>今日专享券</span>
                            <span>每天0点更新</span>
                        </p>
                        <div className="quanBox">
                            {everyDayListArray.length > 0
                                ? everyDayListArray.map(item => (
                                      <div className="quan" key={item.id}>
                                          <div className="money">
                                              <span>¥</span>
                                              <span>{item.discountAmount}</span>
                                          </div>
                                          <div className="couponsConditions">
                                              满{item.totalAmount}元使用
                                          </div>
                                          {item.type == 0 ? (
                                              <div className="couponsScope">
                                                  全场通用
                                              </div>
                                          ) : null}
                                          {item.type == 1 ? (
                                              <div className="couponsScope">
                                                  指定{item.categoryName}商品使用
                                              </div>
                                          ) : null}
                                          <div
                                              className="getCopons"
                                              onClick={() => {
                                                  this.couponClick(
                                                      item
                                                  );
                                              }}
                                          >
                                              立即领取
                                          </div>
                                      </div>
                                  ))
                                :(<p className="empty-data">暂无优惠券</p>)}
                        </div>
                        <p className="todayCouns">本周专享券</p>
                        <div className="quanBox">
                            {weeklyListArray.length > 0
                                ? weeklyListArray.map(item => (
                                      <div className="quan" key={item.id}>
                                          <div className="money">
                                              <span>¥</span>
                                              {item.discountAmount}
                                          </div>
                                          <div className="couponsConditions">
                                              满{item.totalAmount}元使用
                                          </div>
                                          {item.type == 0 ? (
                                              <div className="couponsScope">
                                                  全场通用
                                              </div>
                                          ) : null}
                                          {item.type == 1 ? (
                                              <div className="couponsScope">
                                                  指定{item.categoryName}类商品使用
                                              </div>
                                          ) : null}
                                          <div
                                              className="getCopons"
                                              onClick={() => {
                                                  this.couponClick(item);
                                              }}
                                          >
                                              立即领取
                                          </div>
                                      </div>
                                  ))
                                : (<p className="empty-data">暂无优惠券</p>)}
                        </div>
                    </div>
                    <WhiteSpace />
                    <div className="coupons-two">
                        <div className="coupons-title">
                            <span className="number">2</span>
                            <span className="desc">
                                绿卡专享券 天天领取优惠
                            </span>
                        </div>
                        <div className="integralBox">
                            <div className="leftBox">
                                <div className="leftBoxTitle">您当前购物</div>
                                <div className="leftBoxSubTitle">
                                    返积分为<i>1倍</i>
                                </div>
                            </div>
                            <div className="rightBox">
                                <div className="rightBoxTitle">
                                    开通绿卡购物
                                </div>
                                <div className="rightBoxSubTitle">
                                    返积分为<i>2倍</i>
                                </div>
                                <div className="line"></div>
                                <DefaultImage src={rocketsURL}/>
                            </div>
                        </div>
                        <div>
                            <Button onClick={() => this.openGreenCard()}>
                                立即开启积分加速
                            </Button>
                        </div>
                    </div>
                    <WhiteSpace />
                    <div className="coupons-three">
                        <div className="coupons-title">
                            <span className="number">3</span>
                            <span className="desc">绿卡专享特价</span>
                        </div>
                        <div className="coupons-three-content">
                            <HorizontalScroll
                                onRef={this.onRef}
                                categoryId={categoryId}
                                getGoodsByCategoryIdData={id => {
                                    this.getGoodsByCategoryIdData(id);
                                }}
                            />
                            <div className="list_content">
                                {this.renderContent()}
                            </div>
                            <p className="bottom-text">更多特权敬请期待...</p>
                        </div>
                    </div>
                </div>
                {showButton ? (
                    <div className="open-button">
                        <Button>
                            <span>
                                年卡<i>88元</i>
                                <del>180元</del>
                            </span>
                            <span onClick={() => this.openGreenCard()}>
                                开通绿卡
                            </span>
                        </Button>
                    </div>
                ) : null}
            </div>
        );
    }
}
export default GreenCard;
