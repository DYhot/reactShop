/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./index.scss";
import CustomIcon from "@/components/Icon/CustomIcon";

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: [
                {
                    id: 0,
                    title: "优惠促销",
                    content: "想知道王一博保持元气的小秘密吗？4月21日王一博宝洁星光元气课堂开讲啦",
                    time: "2020-04-09",
                    type: 0,
                    color:"#FF2A00",
                    icon:'#iconyouhui-1',
                    width:'25'
                },
                {
                    id: 1,
                    title: "账户通知",
                    content: "您有一张神秘优惠券待领取，满69件10，点击领取",
                    time: "2020-04-10",
                    type: 1,
                    color:"#FF7C00",
                    icon:'#icondaizhifu1',
                    width:'25'
                },
                {
                    id: 2,
                    title: "交易物流",
                    content: "订单[现货2019年新书]已配送完成，期待您分享商品评价与购物心得，点击评价",
                    time: "2020-04-10",
                    type: 2,
                    color:"#00CF4D",
                    icon:'#iconwuliu',
                    width:'30'
                },
                {
                    id: 3,
                    title: "互动消息",
                    content: "荣耀V30好，还是新出的Redmi k30Pro好？",
                    time: "2020-04-10",
                    type: 3,
                    color:"#00CCE3",
                    icon:'#iconxiaoxi',
                    width:'30'
                },
                {
                    id: 4,
                    title: "系统消息",
                    content: "荣耀V30好，还是新出的Redmi k30Pro好？",
                    time: "2020-04-10",
                    type: 4,
                    color:"#0089F1",
                    icon:'#iconset',
                    width:'25'
                }
            ]
        };
    }

    componentDidMount() {}
    renderContent = () => {
        const { listData } = this.state;
        return (
            <div className="container">
                {listData.map(item => (
                    <div className="list-item" key={item.id}>
                        <div className="left">
                            <div className="icon-wrapper" style={{backgroundColor:item.color}}>
                                <div className="small-point"></div>
                                <CustomIcon
                                    icon={item.icon}
                                    width={`${item.width}px`}
                                    height={`${item.width}px`}
                                    color="#fff"
                                />
                            </div>
                        </div>
                        <div className="right">
                            <p className="title">
                                <span>{item.title}</span>
                                <span>{item.time}</span>
                            </p>
                            <p className="content">
                                <span>{item.content}</span>
                            </p>
                        </div>
                    </div>
                ))}
            </div>
        );
    };
    render() {
        return (
            <div className="notification-wrapper">{this.renderContent()}</div>
        );
    }
}

export default Notification;
