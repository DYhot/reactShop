/**
 * Created by Dengyang on 12/1/2019.
 */
import React from "react";
import {
    Button,
    WhiteSpace,
    WingBlank,
    List,
    InputItem,
    Toast,
    Switch,
    Picker,
    Tag,
    Modal
} from "antd-mobile";
import "../styles/addaddress.scss";
import { validatePhone } from "@/utils/validate";
import { createForm } from "rc-form";
// import { district, provinceLite } from 'antd-mobile-demo-data';
import areaData from "@/utils/area";
import {
    addUserAdress,
    getUserAdressById,
    updateUserAdress,
    addAdressTag,
    updateUserAdressTagStatus
} from "@/api/address";
import CustomIcon from "@/components/Icon/CustomIcon";
const prompt = Modal.prompt;
class Addaddress extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            phone: "",
            // area: ['340000', '341500', '341502'],
            area: [],
            areaText: "",
            areaDeatil: "",
            zipCode: "",
            isDefault: false,
            phoneError: false,
            status: false,
            addressId: "",
            tagList: [],
            tagName: "",
            addStatus: false
        };
    }
    componentDidMount() {
        const params = this.props.match.params.id;
        if (params) {
            // 获取页面初始数据
            this.setState(
                {
                    status: true,
                    addressId: params
                },
                () => {
                    this.getUserAdressByIdData();
                }
            );
        } else {
            this.setState({
                status: false,
                addressId: ""
            });
        }
    }
    getUserAdressByIdData() {
        const { addressId } = this.state;
        const params = { addressId };
        Toast.loading();
        getUserAdressById(params).then(res => {
            Toast.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    tagList: data.list,
                    name: data.receive_name,
                    phone: data.receive_phone,
                    area: data.receive_area.split(","),
                    areaDeatil: data.receive_detail_address,
                    zipCode: data.receive_zip,
                    isDefault: data.receive_isdefault == 0 ? false : true
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    phoneErrorClick = () => {
        if (this.state.phoneError) {
            Toast.info("请输入正确的11位手机号码");
        }
    };
    phoneChange = value => {
        // if (value.replace(/\s/g, "").length < 11) {
        //     this.setState({
        //         phoneError: true
        //     });
        // } else {
        //     this.setState({
        //         phoneError: false
        //     });
        // }
        this.setState({
            phone: value
        });
    };

    areaDismiss(e) {
        console.log("dismiss", e);
    }
    submit() {
        const {
            name,
            phone,
            area,
            areaDeatil,
            zipCode,
            isDefault,
            status,
            addressId
        } = this.state;
        const query = {
            name,
            phone: phone.replace(/\s+/g, ""),
            area,
            areaDeatil,
            zipCode,
            isDefault: isDefault ? 1 : 0
        };
        console.log(666, this.state.areaText);
        if (!name) {
            Toast.info("姓名不能为空", 1);
            return;
        }
        // if (!validatePhone(phone.replace(/\s+/g, ""))) {
        //     Toast.info("电话号码不正确", 1);
        //     return;
        // }
        if (area.length < 1) {
            Toast.info("地区不能为空", 1);
            return;
        }
        if (!areaDeatil) {
            Toast.info("详细地址不能为空", 1);
            return;
        }
        // if (!zipCode) {
        //     Toast.info("邮编不能为空", 1);
        //     return;
        // }
        if (status) {
            query.addressId = addressId;
            updateUserAdress(query).then(res => {
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    Toast.info(res.msg, 2, () => {
                        this.props.history.push("/address");
                    });
                } else {
                    Toast.info(res.msg, 1);
                }
            });
        } else {
            addUserAdress(query).then(res => {
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    Toast.info(res.msg, 1, () => {
                        this.props.history.push("/address");
                    });
                } else {
                    Toast.info(res.msg, 1);
                }
            });
        }
    }

    addTagSubmit(value) {
        if (!value.replace(/\s+/g, "")) {
            Toast.info("标签不能为空", 1);
            return;
        }
        if (value.length > 5) {
            Toast.info("标签最多5个字符", 1);
            return;
        }
        const query = {
            tagName: value,
            addressId: this.state.addressId
        };
        addAdressTag(query).then(res => {
            if (!res) {
                return;
            }
            if (res.code === 0) {
                this.getUserAdressByIdData();
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    setTag = item => {
        const query = {
            addressTagId: item.id,
            tagStatus: item.tag_status == 1 ? 0 : 1
        };
        updateUserAdressTagStatus(query).then(res => {
            if (!res) {
                return;
            }
            if (res.code === 0) {
                this.getUserAdressByIdData();
            } else {
                Toast.info(res.msg, 1);
            }
        });
    };
    render() {
        const { getFieldProps } = this.props.form;
        const { tagList } = this.state;
        return (
            <div className="addaddress_wrapper">
                <div className="addaddress_content">
                    <div className="form_wrapper">
                        <List renderHeader={() => ""}>
                            <InputItem
                                {...getFieldProps("autofocus", {
                                    initialValue: this.state.name
                                })}
                                placeholder="收货人姓名"
                                onChange={value => {
                                    this.setState({
                                        name: value
                                    });
                                }}
                                ref={el => (this.inputRef = el)}
                            >
                                姓名
                            </InputItem>
                            <InputItem
                                {...getFieldProps("phone", {
                                    initialValue: this.state.phone
                                })}
                                type="phone"
                                placeholder="收货人手机号码"
                                error={this.state.phoneError}
                                onErrorClick={this.phoneErrorClick}
                                onChange={this.phoneChange}
                            >
                                电话
                            </InputItem>
                            <Picker
                                extra="选择省 / 市 / 区"
                                data={areaData}
                                {...getFieldProps("district", {
                                    initialValue: this.state.area
                                })}
                                onOk={e => {
                                    this.setState({
                                        area: e
                                    });
                                }}
                                onDismiss={e => this.areaDismiss(e)}
                                format={(labels) => { return labels.join('-')}}
                            >
                                <List.Item arrow="horizontal">地区</List.Item>
                            </Picker>
                            <InputItem
                                {...getFieldProps("focus", {
                                    initialValue: this.state.areaDeatil
                                })}
                                placeholder="街道门牌、楼层房间号等信息"
                                onChange={value => {
                                    this.setState({
                                        areaDeatil: value
                                    });
                                }}
                                ref={el => (this.inputRef = el)}
                            >
                                详细地址
                            </InputItem>
                            <InputItem
                                {...getFieldProps("focus", {
                                    initialValue: this.state.zipCode
                                })}
                                placeholder="邮政编码"
                                onChange={value => {
                                    this.setState({
                                        zipCode: value
                                    });
                                }}
                                ref={el => (this.inputRef = el)}
                            >
                                邮政编码
                            </InputItem>
                            <List.Item
                                extra={
                                    <div className="tag_wrapper">
                                        {tagList.map((item, index) => {
                                            return (
                                                <Button
                                                    key={index}
                                                    className={
                                                        item.tag_status == 1
                                                            ? "tag_active"
                                                            : ""
                                                    }
                                                    onClick={item =>
                                                        this.setTag(
                                                            tagList[index]
                                                        )
                                                    }
                                                >
                                                    {item.address_tag_name}
                                                </Button>
                                            );
                                        })}
                                        <Button
                                            className="add_tag"
                                            type="primary"
                                            onClick={() =>
                                                prompt(
                                                    "",
                                                    "请输入标签名字",
                                                    [
                                                        { text: "取消" },
                                                        {
                                                            text: "确定",
                                                            onPress: value =>
                                                                this.addTagSubmit(
                                                                    value
                                                                )
                                                        }
                                                    ],
                                                    "default",
                                                    null,
                                                    ["请输入标签名字"]
                                                )
                                            }
                                        >
                                            <span>
                                                <CustomIcon
                                                    icon="#iconjia"
                                                    width="15px"
                                                    height="15px"
                                                    color="#333"
                                                />
                                            </span>
                                        </Button>
                                    </div>
                                }
                            >
                                标签
                            </List.Item>
                            <List.Item
                                extra={
                                    <Switch
                                        checked={this.state.isDefault}
                                        onChange={() => {
                                            this.setState({
                                                isDefault: !this.state.isDefault
                                            });
                                        }}
                                    />
                                }
                            >
                                设为默认收货地址
                            </List.Item>
                        </List>
                    </div>
                    <div className="button_wrapper">
                        <Button type="primary" onClick={() => this.submit()}>
                            <span>保存</span>
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}
const AddaddressContainer = createForm()(Addaddress);
export default AddaddressContainer;
