/**
 * Created by Dengyang on 12/1/2019.
 */
import React, { Fragment } from "react";
import {
    Button,
    WhiteSpace,
    Toast,
    List,
    SwipeAction,
    PullToRefresh,
    ListView,
    WingBlank
} from "antd-mobile";
import CustomIcon from "@/components/Icon/CustomIcon";
import "./styles/index.scss";
import noAddress from "@/assets/images/order/noAddress.png";
import {
    getUserAdress,
    delUserAdress,
    updateUserAdressStatus
} from "@/api/address";
import LoadingCircle from "@/components/Loading/LoadingCircle"; // 加载等待时的提示模块
import arrayTreeFilter from "array-tree-filter";
import areaData from "@/utils/area";
import Loading from "@/components/Loading/Loading";

class Address extends React.Component {
    constructor(props) {
        super(props);
        const dataSource = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
        });
        this.state = {
            current: 1,
            size: 10,
            total: 0,
            receiveAddressData: [],
            dataSource,
            height: document.documentElement.clientHeight,
            isShowContent: false, // 控制页面再数据请求后显示
            refreshing: false, // 是否显示刷新状态
            isLoading: true ,// 是否显示加载状态
        };
    }
    componentDidMount() {
        // console.log('读取参数用:', this.props.location.search);
        console.log('读取参数用:', this.props.location.state);
        this.getUserAdressData();
    }
    getSel(area) {
        const value = area.split(",");
        if (!value) {
            return "";
        }
        const treeChildren = arrayTreeFilter(
            areaData,
            (c, level) => c.value === value[level]
        );
        return treeChildren.map(v => v.label).join("");
    }
    setLoading(isLoading) {
        if (isLoading) {
            this.setState({
                isLoading
            });
            return;
        }
        setTimeout(() => {
            console.log(222, isLoading);
            this.setState({
                isLoading
            });
        }, 1000);
    }
    getUserAdressData() {
        const {
            current,
            size,
            refreshing,
            receiveAddressData,
            dataSource
        } = this.state;
        const params = { current, size };
        Loading.show();
        getUserAdress(params).then(res => {
        Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                if (res.data.records.length > 0) {
                    const ListArray = [
                        ...receiveAddressData,
                        ...res.data.records
                    ];
                    this.setState({
                        total: res.data.total,
                        current: current + 1,
                        isShowContent: true,
                        refreshing: false,
                        // isLoading: false,
                        receiveAddressData: ListArray,
                        dataSource: dataSource.cloneWithRows(ListArray) // 数据源dataSource
                    });
                    this.setLoading(false);
                } else {
                    this.setState({
                        isShowContent: false
                    });
                }
            } else {
                Toast.info(res.msg, 3);
            }
        });
    }
    handleDelete(row) {
        this.setLoading(true);
        delUserAdress({ id: row.id }).then(res => {
            if (!res) {
                return;
            }
            if (res.code == 0) {
                Toast.info(res.msg, 3);
                this.setState(
                    {
                        current: 1,
                        total: 0,
                        receiveAddressData: []
                    },
                    () => {
                        this.getUserAdressData();
                    }
                );
            }
            Toast.hide();
        });
    }
    handleSetDefault(row) {
        this.setLoading(true);
        const params = {
            id: row.id,
            isdefault: row.receive_isdefault == 1 ? 0 : 1
        };
        updateUserAdressStatus(params).then(res => {
            this.setLoading(false);
            if (!res) {
                return;
            }
            if (res.code == 0) {
                Toast.info(res.msg, 3);
                this.setState(
                    {
                        current: 1,
                        total: 0,
                        receiveAddressData: []
                    },
                    () => {
                        this.getUserAdressData();
                    }
                );
            }
            Toast.hide();
        });
    }
    // 下拉刷新
    onRefresh = () => {
        this.setState({ refreshing: true });
        this.setState(
            {
                current: 1,
                total: 0,
                receiveAddressData: []
            },
            () => {
                this.getUserAdressData();
            }
        );
    };
    // 加载更多
    onEndReached = () => {
        const { isLoading, total, current, size } = this.state;
        // console.log(1111,isLoading, total, current)
        // const info=`${isLoading},${total},${current},${size},${Boolean(total < (current )*size+1)}`
        // Toast.info(`${info}`, 3);
        if (isLoading || total + size < current * size + 1) {
            Toast.hide();
            return;
        }
        this.setState(
            {
                isLoading: true
            },
            () => {
                this.getUserAdressData();
            }
        );
    };
    skipPage() {
        this.props.history.push("./address/addaddress");
    }
    itemClick=(e,rowData)=>{
        const params = this.props.location.state;
        if (params) {
            const { type, prePageState } = params;
            if(type==='order'){
                this.props.history.push({
                    pathname: "/settlement",
                    state: { rowData, prePageState}
                });
            }
        }else{
            return;
        }
    }
    render() {
        const {
            dataSource,
            receiveAddressData,
            size,
            isLoading,
            isShowContent,
            refreshing
        } = this.state;
        const { history } = this.props;
        const swipeRow = isdefault => {
            return <div></div>;
        };
        // 定义Row，从数据源(dataSurce)中接受一条数据循环到ListView
        const renderRow = (rowData, sectionID, rowID) => {
            // console.log(456, rowData, sectionID, rowID);
            return (
                <SwipeAction
                    key={rowID}
                    style={{ backgroundColor: "transparent" }}
                    autoClose
                    right={[
                        {
                            text: "设为默认",
                            onPress: () => this.handleSetDefault(rowData),
                            style: {
                                backgroundColor: "#ddd",
                                color: "#333",
                            }
                        },
                        {
                            text: "删除",
                            onPress: () => this.handleDelete(rowData),
                            style: {
                                backgroundColor: "#F4333C",
                                color: "white"
                            }
                        }
                    ]}
                    onOpen={() =>
                        console.log("global open", rowData.receive_isdefault)
                    }
                    onClose={() => console.log("global close")}
                >
                    <List.Item onClick={e => this.itemClick(e,rowData)}>
                        <div className="list_item">
                            <div className="list_item_main">
                                <span>{rowData.receive_name}</span>
                                <span>{rowData.receive_phone}</span>
                                {rowData.receive_isdefault == 1 && (
                                    <span className="default_tag">默认</span>
                                )}
                                {rowData.tagList.map((item, index) => {
                                    if (item.tag_status == 1) {
                                        return (
                                            <span
                                                className="default_label"
                                                key={index}
                                            >
                                                {item.address_tag_name}
                                            </span>
                                        );
                                    }
                                })}
                            </div>
                            <div className="list_item_detail">
                                <span>
                                    {this.getSel(rowData.receive_area)}
                                    {rowData.receive_detail_address}
                                </span>
                                <span
                                    onClick={() =>
                                        history.push(
                                            `/address/addaddress/${rowData.id}`
                                        )
                                    }
                                >
                                    <CustomIcon
                                        icon="#iconbianji"
                                        width="20px"
                                        height="20px"
                                        color="#000"
                                    />
                                </span>
                            </div>
                        </div>
                    </List.Item>
                </SwipeAction>
            );
        };

        return (
            <div className="address_wrapper">
                {isShowContent ? (
                    <div className="address_content">
                        <ListView
                            ref={el => (this.lv = el)}
                            dataSource={dataSource}
                            renderFooter={() => (
                                <div
                                    className=""
                                    style={{ padding: 30, textAlign: "center" }}
                                >
                                    {isLoading
                                        ? "正在加载..."
                                        : "真的已经到底了"}
                                </div>
                            )}
                            renderRow={renderRow}
                            useBodyScroll={true}
                            distanceToRefresh="50"
                            pullToRefresh={
                                <PullToRefresh
                                    refreshing={refreshing}
                                    onRefresh={this.onRefresh}
                                />
                            }
                            onEndReached={this.onEndReached}
                            onEndReachedThreshold={30}
                            pageSize={size}
                        />
                    </div>
                ) : (
                    <div className="no_address">
                        <div>
                            <img src={noAddress} alt="" />
                            <p>还没有添加过地址,赶紧去添加一个吧</p>
                        </div>
                    </div>
                )}

                <div className="add_btn_wrapper">
                    <Button
                        icon={
                            <CustomIcon
                                icon="#iconjia"
                                width="12px"
                                height="12px"
                                color="#fff"
                            />
                        }
                        onClick={() => this.skipPage()}
                    >
                        <span>增加新地址</span>
                    </Button>
                    <WhiteSpace />
                </div>
            </div>
        );
    }
}
export default Address;
