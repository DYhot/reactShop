/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import { Tabs, ListView, Toast, List, SearchBar } from "antd-mobile";
import { getCategoryMenu } from "@/api/category";
import { getGoodsByCategoryId } from "@/api/category";
import Loading from "@/components/Loading/Loading";
import BScroll from "better-scroll";
import CustomIcon from "@/components/Icon/CustomIcon";
import DefaultImage from "@/components/DefaultImage/index";
import IconCart from "@/components/Icon/IconCart";
import PullRefresh from "@/components/PullRefresh/index";

class Discover extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            noMore: false,
            empty: true,
            freshTime: "",
            total: 0,
            pageNum: 0,
            pageSize: 10,
            init: false,
            // current: 1,
            // size: 10,
            // total: 0,
            tabsArray: [],
            activeTab: null,
            foldStatus: false,
            goodsListArray: [],
            value: null
        };
        this.horizontalScroll = null;
    }

    componentDidMount() {
        const wrapper = document.querySelector(".wrapper");
        //选中DOM中定义的 .wrapper 进行初始化
        this.horizontalScroll = new BScroll(wrapper, {
            scrollX: true, //开启横向滚动
            click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
            scrollY: false //关闭竖向滚动
        });
        this.getCategoryData();
    }
    getCategoryData() {
        Loading.show();
        getCategoryMenu().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                if (res.data.records.length > 0) {
                    let tabs = [];
                    data.forEach((item, index) => {
                        const target = {
                            title: item.categoryName,
                            key: item.id
                        };
                        tabs.push(target);
                    });
                    this.setState(
                        {
                            activeTab: tabs.length > 0 ? tabs[0].key : null,
                            total: res.data.total,
                            tabsArray: tabs
                        },
                        () => {
                            this.getGoodsByCategoryIdData({ id: tabs[0].key });
                        }
                    );
                }
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    getGoodsByCategoryIdData({ id = null, value = null }) {
        const { pageNum, pageSize } = this.state;
        let params = {
            current: pageNum + 1,
            size: pageSize
        };
        const { activeTab } = this.state;
        // let params = {};
        value
            ? (params.searchValue = value)
            : id
            ? (params.id = id)
            : (params.id = activeTab);

        Loading.show();
        getGoodsByCategoryId(params).then(res => {
            Loading.hide();
            this.setState({
                showLoading: false
            });
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const total = res.data.total;

                this.setState({
                    empty: data.length > 0 ? false : true,
                    noMore:
                        total > (1 - 1) * pageSize + pageSize ? false : true,
                    pageNum: 1,
                    init: true,
                    goodsTotal: total,
                    goodsListArray: data ? data : []
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    getMoreData = () => {
        const { pageNum, pageSize, activeTab } = this.state;
        let params = {
            current: pageNum + 1,
            size: pageSize
        };
        params.id = activeTab;
        Loading.show();
        return new Promise((resolve, reject) => {
            getGoodsByCategoryId(params).then(res => {
                Loading.hide();
                this.setState({
                    showLoading: false
                });
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    const data = res.data.records;
                    const total = res.data.total;

                    this.setState(
                        {
                            empty: data.length > 0 ? false : true,
                            noMore:
                                total > (pageNum ) * pageSize + pageSize
                                    ? false
                                    : true,
                            pageNum:
                                total > (pageNum - 1) * pageSize + pageSize
                                    ? pageNum + 1
                                    : pageNum,
                            init: true,
                            goodsTotal: total,
                            goodsListArray: this.state.goodsListArray.concat(
                                data
                            )
                        },
                        () => {
                            console.log(123,this.state.noMore,this.state.pageNum)
                            resolve();
                        }
                    );
                } else {
                    Toast.info(res.msg, 1);
                }
            });
        });
    };
    tabClick = row => {
        const { activeTab } = this.state;
        if (row.key == activeTab) {
            return;
        }
        this.setState({
            activeTab: row.key,
            pageNum:0,
        },()=>{
            this.getGoodsByCategoryIdData({ id: row.key });
        });
    };
    foldTabClick = row => {
        this.foldClick();
        this.setState(
            {
                activeTab: row.key,
                pageNum:0,
            },
            () => {
                const el = this.refs[`transverseLi${row.key}`];
                this.horizontalScroll.scrollToElement(el, 500);
                this.getGoodsByCategoryIdData({ id: row.key });
            }
        );
    };
    renderTransverseMenu = () => {
        const { activeTab, tabsArray } = this.state;
        return tabsArray.length > 0
            ? tabsArray.map((item, index) => {
                  return (
                      <li
                          ref={`transverseLi${item.key}`}
                          key={item.key}
                          className={`item ${
                              activeTab == item.key ? "tab-active" : ""
                          }`}
                          onClick={() => {
                              this.tabClick(item);
                          }}
                      >
                          {item.title}
                      </li>
                  );
              })
            : null;
    };
    renderFoldMenu = () => {
        const { activeTab, tabsArray } = this.state;
        return tabsArray.length > 0
            ? tabsArray.map((item, index) => {
                  return (
                      <li
                          key={item.key}
                          className={`item ${
                              activeTab == item.key ? "tab-active" : ""
                          }`}
                          onClick={() => {
                              this.foldTabClick(item);
                          }}
                      >
                          {item.title}
                      </li>
                  );
              })
            : null;
    };
    renderContent = tab => {
        const { goodsListArray, init } = this.state;
        return (
            <div className="list-wrapper">
                {goodsListArray.length > 0 ? (
                    goodsListArray.map(item => (
                        <div className="list-item" key={item.id}>
                            <div
                                className="top-row"
                                onClick={() => {
                                    this.skipToDetail(item.id);
                                }}
                            >
                                <DefaultImage src={item.url} />
                            </div>
                            <div className="middle-row">
                                <p className="title">{item.title}</p>
                                <p className="describe">{item.describe}</p>
                            </div>
                            <div className="bottom-row">
                                <div className="text-wrapper">
                                    <span>￥{item.price}</span>
                                    {item.price != item.original_price && (
                                        <del className="amount_del">
                                            ￥{item.original_price}
                                        </del>
                                    )}
                                </div>
                                <div
                                    className="iconCartWrapper"
                                    onClick={e => this.addToCart(item, e)}
                                >
                                    {item.count > 0 ? (
                                        <IconCart />
                                    ) : (
                                        <span className="sold-out">售罄</span>
                                    )}
                                </div>
                            </div>
                        </div>
                    ))
                ) : init ? (
                    <div className="empty_wrapper">
                        <span className="icon_wrapper">
                            <CustomIcon
                                icon="#iconzanwushuju"
                                width="45px"
                                height="45px"
                                color="#999"
                            />
                            <p>暂无数据</p>
                        </span>
                    </div>
                ) : null}
            </div>
        );
    };
    foldClick = () => {
        this.setState({
            foldStatus: !this.state.foldStatus
        });
    };
    skipToDetail(id) {
        this.props.history.push({ pathname: `/detail/${id}` });
    }
    onSubmit = value => {
        this.setState({
            value
        });
        if (value) {
            this.getGoodsByCategoryIdData({ value });
        }
    };
    onClear = () => {
        const { value } = this.state;
        if (value) {
            this.getGoodsByCategoryIdData({ id: this.state.activeTab });
        }
    };
    render() {
        const { goodsListArray, foldStatus, noMore, empty } = this.state;
        return (
            <div className="discover-wrapper">
                <div className="head-search">
                    <SearchBar
                        placeholder="搜索菜谱、食材"
                        maxLength={8}
                        onSubmit={value => this.onSubmit(value)}
                        onClear={value => this.onClear()}
                    />
                </div>
                <div className="nav-wrapper">
                    <div className="head">
                        <div className="left">
                            <span>今日推荐</span>
                        </div>
                    </div>
                    <div className="wrapper content-wrapper">
                        <ul className="content">
                            {this.renderTransverseMenu()}
                        </ul>
                    </div>
                    <div
                        className="fold-btn"
                        onClick={() => {
                            this.foldClick();
                        }}
                    >
                        <span>{foldStatus ? "收起" : "全部"}</span>
                        <span>
                            {foldStatus ? (
                                <CustomIcon
                                    icon="#iconjiantouxia"
                                    width="15px"
                                    height="15px"
                                    color="#999"
                                />
                            ) : (
                                <CustomIcon
                                    icon="#iconjiantoushang"
                                    width="15px"
                                    height="15px"
                                    color="#999"
                                />
                            )}
                        </span>
                    </div>
                    {foldStatus ? (
                        <div className="menu-wrapper">
                            <p className="title">菜单分类</p>
                            <div className="list-wrapper">
                                {this.renderFoldMenu()}
                            </div>
                        </div>
                    ) : null}
                </div>
                <div className="content-list-wrapper">
                    {/* {this.renderContent()} */}
                    <PullRefresh
                        data={goodsListArray}
                        empty={empty}
                        noMore={noMore}
                        pullUp={this.getMoreData}
                        isPullDown={false}
                    >
                        {this.renderContent()}
                    </PullRefresh>
                </div>
            </div>
        );
    }
}

export default Discover;
