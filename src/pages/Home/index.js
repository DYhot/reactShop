/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import { Carousel, WingBlank, Tabs, Button, Toast } from "antd-mobile";
import { getUserOrderList } from "@/api/order";
import { getGoodsList } from "@/api/goods";
import { addGoodToCart } from "@/api/cart";
import { getMenu ,getBanner} from "@/api/home";
import Loading from "@/components/Loading/Loading";
import CustomIcon from "@/components/Icon/CustomIcon";
import IconCart from "@/components/Icon/IconCart";
import ToTop from "@/components/BackToTop/ToTop";
import DefaultImage from "@/components/DefaultImage/index";
import VipIcon from "./components/VipIcon";
import InfiniteScroll from "react-infinite-scroller";
import imgLoadURL from "@/assets/images/common/loading/img-load.png";
import LazyLoad from "react-lazyload";


class Home extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            menuData: [],
            defaultBannerData: [{
                id:0,
                url:imgLoadURL
            }],
            bannerData: [],
            imgHeight: 176,
            tabs: [
                { title: "全部", key: 0 },
                { title: "晚餐", key: 1 },
                { title: "人气", key: 2 },
                { title: "心选", key: 3 }
            ],
            currentTabKey: null,
            current: 1,
            size: 10,
            total: 0,
            goodsListArray: [],
            hour: 2,
            minutes: 58,
            seconds: 59
        };
    }

    componentDidMount() {
        // simulate img loading
        this.timer = setInterval(() => this.CountDown(), 1000);
        this.getMenuData();
        this.getBannerData();
        this.getGoodsListData();
    }
    CountDown = () => {
        const { hour, minutes, seconds } = this.state;
        if (hour == 0 && minutes == 0 && seconds == 0) {
            clearInterval(this.timer);
        } else if (hour >= 0) {
            if (minutes >= 0) {
                if (seconds > 0) {
                    this.setState({
                        seconds: seconds - 1
                    });
                } else if (seconds == 0) {
                    this.setState({
                        minutes: minutes - 1,
                        seconds: 59
                    });
                }
            } else {
                this.setState({
                    hour: hour - 1,
                    minutes: minutes - 1,
                    seconds: 59
                });
            }
        }
    };
    getMenuData() {
        Loading.show();
        getMenu().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    menuData: data
                });
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    getBannerData() {
        Loading.show();
        getBanner().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    bannerData: data
                });
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    getGoodsListData() {
        Loading.show();
        const { current, size, currentTabKey, tabs } = this.state;
        const params = {
            current,
            size: 1000
        };
        getGoodsList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const total = res.data.total;
                this.setState({
                    total,
                    current: this.state.current + 1,
                    // hasMore: res.data.hasNextPage,
                    goodsListArray: this.state.goodsListArray.concat(data)
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    tabsChange = val => {
        const {currentTabKey}=this.state;
        if(val.key==currentTabKey){
            return;
        }
        this.setState(
            {
                currentTabKey: val.key
            },
            () => {
                this.getGoodsListData(val.key);
            }
        );
    };
    addToCart = (item, e) => {
        e.nativeEvent.stopImmediatePropagation();
        e.stopPropagation();
        if (item.count < 1) {
            return;
        }
        Loading.show();
        console.log(33, item);
        addGoodToCart({ id: item.id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("成功加入购物车", 1);
            } else {
                Toast.info(res.msg, 1);
            }
        });
    };
    skipToDetail(id) {
        this.props.history.push({ pathname: `/detail/${id}` });
    }
    renderContent = tab => {
        const { goodsListArray } = this.state;
        return (
            <div className="content-wrapper">
                {goodsListArray.length > 0 ? (
                    goodsListArray.map(item => (
                        <div className="list-item" key={item.id}>
                            <div
                                className="top-row"
                                onClick={() => {
                                    this.skipToDetail(item.id);
                                }}
                            >
                                <LazyLoad width={"50px"}>
                                    <DefaultImage src={item.url} />
                                </LazyLoad>
                            </div>
                            <div className="middle-row">
                                <p className="title">{item.title}</p>
                                <p className="describe">{item.describe}</p>
                            </div>
                            <div className="bottom-row">
                                <div className="text-wrapper">
                                    <span>￥{item.price}</span>
                                    {item.price != item.original_price && (
                                        <del className="amount_del">
                                            ￥{item.original_price}
                                        </del>
                                    )}
                                </div>
                                <div
                                    className="iconCartWrapper"
                                    onClick={e => this.addToCart(item, e)}
                                >
                                    {item.count > 0 ? (
                                        <IconCart />
                                    ) : (
                                        <span className="sold-out">售罄</span>
                                    )}
                                </div>
                            </div>
                        </div>
                    ))
                ) : (
                    <div className="nodata-wrapper">
                        {/* <img src={noDataImg} alt=""/> */}
                        <p>暂无数据</p>
                    </div>
                )}
                {/* </InfiniteScroll> */}
            </div>
        );
    };
    render() {
        const {
            menuData,
            tabs,
            currentTabKey,
            hour,
            minutes,
            seconds,
            goodsListArray,
            bannerData,
            defaultBannerData
        } = this.state;
        return (
            //             <InfiniteScroll
            //     pageStart={1}
            //     loadMore={()=>{this.getGoodsListData()}}
            //     hasMore={true || false}
            //     loader={<div className="loader" key={0}>Loading ...</div>}
            // >
            <div className="home-wrapper">
                {/* <InfiniteScroll
                    pageStart={1}
                    loadMore={()=>{this.getGoodsListData()}}
                    hasMore={true || false}
                    loader={<div className="loader" key={0}>Loading ...</div>}
                    useWindow={true}
                > */}
                <ToTop />
                <div className="banner-wrapper">
                    <WingBlank>
                        <Carousel
                            autoplay={false}
                            infinite
                            beforeChange={(from, to) =>
                                console.log(`slide from ${from} to ${to}`)
                            }
                            afterChange={index =>
                                console.log("slide to", index)
                            }
                        >
                            {bannerData.length>0?bannerData.map(val => (
                                <a
                                    key={val}
                                    href={val.link_href}
                                    style={{
                                        display: "inline-block",
                                        width: "100%",
                                        height: this.state.imgHeight,
                                        textAlign:'center'
                                    }}
                                >
                                    <LazyLoad width={"100%"}>
                                        <img
                                            src={val.url}
                                            alt=""
                                            style={{
                                                width: "100%",
                                                verticalAlign: "top"
                                            }}
                                            onLoad={() => {
                                                // fire window resize event to change height
                                                window.dispatchEvent(
                                                    new Event("resize")
                                                );
                                                this.setState({
                                                    imgHeight: "auto"
                                                });
                                            }}
                                        />
                                    </LazyLoad>
                                </a>
                            )):<a
                            key={defaultBannerData[0].id}
                            style={{
                                display: "inline-block",
                                width: "100%",
                                height: this.state.imgHeight,
                                textAlign:'center'
                            }}
                        >
                            <LazyLoad width={"100%"}>
                                <img
                                    src={defaultBannerData[0].url}
                                    alt=""
                                    style={{
                                        height: "100%",
                                        verticalAlign: "top"
                                    }}
                                    onLoad={() => {
                                        // fire window resize event to change height
                                        window.dispatchEvent(
                                            new Event("resize")
                                        );
                                        this.setState({
                                            imgHeight: "auto"
                                        });
                                    }}
                                />
                            </LazyLoad>
                        </a>}
                        </Carousel>
                    </WingBlank>
                    <div className="nav-wrapper">
                        {menuData.map(val => (
                            <div
                                className="list-item"
                                key={val.id}
                                onClick={() => {
                                    this.props.history.push({
                                        pathname: `/category`,
                                        search: `?type=${val.id}`
                                    });
                                }}
                            >
                                <DefaultImage src={val.url} />
                                <p>{val.categoryName}</p>
                            </div>
                        ))}
                    </div>
                    <div
                        className="open-member"
                        onClick={() => {
                            this.props.history.push({
                                pathname: `/greenCard`
                            });
                        }}
                    >
                        <div className="left">
                            <VipIcon />
                            <span>加入会员每年预计节省806元</span>
                        </div>
                        <div>5折开卡></div>
                    </div>
                    <div className="limit-time-wrapper">
                        <div className="head">
                            <div className="left">
                                <span>限时抢购</span>
                                <span className="cutDown-time">
                                    <span>{hour}</span>:<span>{minutes}</span>:
                                    <span>{seconds}</span>
                                </span>
                            </div>
                            <div
                                className="right"
                                onClick={() => {
                                    this.props.history.push({
                                        pathname: `/category`
                                    });
                                }}
                            >
                                更多
                            </div>
                        </div>
                        <div className="swiper-wrapper">
                            {this.renderContent()}
                        </div>
                    </div>
                    <div className="features-wrapper">
                        <div className="head">
                            <div className="left">
                                <span>特色专区</span>
                            </div>
                        </div>
                        <div className="content-wrapper">
                            <div className="list-item">
                                <p className="title">新品尝鲜</p>
                                <p className="describe">不时不食,又闻棱角香</p>
                                <div className="image-wrapper">
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[0].url}
                                        />
                                    ) : null}
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[2].url}
                                        />
                                    ) : null}
                                </div>
                            </div>
                            <div className="list-item">
                                <p className="title">十二月爆款</p>
                                <p className="describe">
                                    世界之大不过一盘番茄炒蛋
                                </p>
                                <div className="image-wrapper">
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[3].url}
                                        />
                                    ) : null}
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[24].url}
                                        />
                                    ) : null}
                                </div>
                            </div>
                            <div className="list-item">
                                <p className="title">VIP专享</p>
                                <p className="describe">阳光玫瑰 VIP只要12.9</p>
                                <div className="image-wrapper">
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[9].url}
                                        />
                                    ) : null}
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[10].url}
                                        />
                                    ) : null}
                                </div>
                            </div>
                            <div className="list-item">
                                <div className="item">
                                    <p className="title">吃什么</p>
                                    <p className="describe">童年落花生</p>
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[11].url}
                                        />
                                    ) : null}
                                </div>
                                <div className="item">
                                    <p className="title">平价菜场</p>
                                    <p className="describe">豆芽0.99</p>
                                    {goodsListArray.length > 0 ? (
                                        <DefaultImage
                                            src={goodsListArray[13].url}
                                        />
                                    ) : null}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="goods-list-wrapper">
                        <Tabs
                            tabs={tabs}
                            swipeable={false}
                            distanceToChangeTab={1}
                            page={currentTabKey}
                            tabBarBackgroundColor="##F4F4F4"
                            tabBarActiveTextColor="#00c35d"
                            tabBarInactiveTextColor="#6B6B6B"
                            tabBarUnderlineStyle={{
                                border: "1px #00c35d solid",
                                width: "15%",
                                marginLeft: "5%"
                            }}
                            tabBarTextStyle={{
                                fontSize: "14px",
                                fontWeight: "bold"
                            }}
                            renderTabBar={props => (
                                <Tabs.DefaultTabBar {...props} page={4} />
                            )}
                            onChange={val => {
                                this.tabsChange(val);
                            }}
                        >
                            {this.renderContent}
                        </Tabs>
                    </div>
                </div>
                {/* </InfiniteScroll> */}
            </div>
        );
    }
}

export default Home;
