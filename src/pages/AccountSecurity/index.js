/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import { List, WhiteSpace } from "antd-mobile";
const Item = List.Item;
const MobileDetect = require("mobile-detect");
class AccountSecurity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        var md = new MobileDetect(window.navigator.userAgent);
        console.log(111, md);
        console.log(md.mobile()); // 'Sony'
        console.log(md.phone()); // 'Sony'
        console.log(md.tablet()); // null
        console.log(md.userAgent()); // 'Safari'
        console.log(md.os()); // 'AndroidOS'
        console.log(md.is("iPhone")); // false
        console.log(md.is("bot")); // false
        console.log(md.version("Webkit")); // 534.3
        console.log(md.versionStr("Build")); // '4.1.A.0.562'
        console.log(md.match("playstation|xbox")); // false
    }

    render() {
        return (
            <div className="accountSecurity-wrapper">
                <List className="paySetting-list">
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push(
                                "/accountSecurity/passwordManage"
                            );
                        }}
                    >
                        密码管理
                    </Item>
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push(
                                "/accountSecurity/passwordManage"
                            );
                        }}
                    >
                        支付管理
                    </Item>
                    <WhiteSpace />
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push(
                                "/accountSecurity/gesturesPassword"
                            );
                        }}
                    >
                        手势
                    </Item>
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push("/accountSecurity/faceSet");
                        }}
                    >
                        面容
                    </Item>
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push(
                                "/accountSecurity/faceScan"
                            );
                        }}
                    >
                        刷脸
                    </Item>
                    <WhiteSpace />
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push(
                                "/accountSecurity/equipmentManage"
                            );
                        }}
                    >
                        设备管理
                    </Item>
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push("/accountSecurity/facePay");
                        }}
                    >
                        授权管理
                    </Item>
                    <WhiteSpace />
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push("/accountSecurity/facePay");
                        }}
                    >
                        账户注销
                    </Item>
                </List>
            </div>
        );
    }
}

export default AccountSecurity;
