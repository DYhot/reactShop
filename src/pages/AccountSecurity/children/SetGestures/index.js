/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "../../styles/setGestures.scss";
import GestureUnlock from "@/components/GestureUnlock";

class SetGestures extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            logoImg: "",
            firstPwd: "", // 用来存创建密码时第一次输入的密码，便于和第二次比较
            regOrLogin: "reg", // 传给子组件用于判断是注册还是登陆
            isShowConfirm: false // 是否显示confirm密码
        };
    }

    componentDidMount() {}
    onceDraw = e => {
        if (e.success) {
            console.log("第一次");
            this.isShowConfirm = true;
        }
    };
    fromNinePoint(e) {
        if (e.success) {
            console.log("父组件：", e.pwd, "手势密码设置完成，登录");
            this.send(e.pwd.join(""));
        }
    }
    send = gesPwd => {
        console.log("手势密码：", gesPwd);
        this.$axios
            .post("http://*****/*****/****/gesturePasswordSetup", {
                username: "123",
                gesturePassword: gesPwd // 手势密码
            })
            .then(res => {
                console.log("返回的数据：", res);
                let flag = res.data.flagStr;
                if (flag === "Succ") {
                    console.log("设置成功");
                }
            })
            .catch(res => {
                console.log("报错：", res);
            });
    };
    // 跳过
    skip = () => {
        this.$router.push({ name: "/" });
    };
    render() {
        const { logoImg, firstPwd, regOrLogin, isShowConfirm } = this.state;
        return (
            <div className="setGestures-wrapper">
                {/* <!--首次登陆设置手势密码--> */}
                <div className="createGesture">
                    <div className="picture">
                        <img src={logoImg} alt="" />
                    </div>
                    <div className="words">
                        {!isShowConfirm ? <p></p> : null}
                        {!isShowConfirm ? <p></p> : null}
                        {!isShowConfirm ? <p></p> : null}
                        {!isShowConfirm ? <p></p> : null}
                    </div>
                    {/* <!--下面这是模拟登录时传密码过去--> */}
                    {/* <!-- <gestureUnlock @firstDown="onceDraw" @onDrawDone='fromNinePoint' :fatherPassword="[1,2,3,4,5]"></gestureUnlock> --> */}
                    {/* //   <!--此页面是创建密码，需要输入两次，组件不传值，fatherPassword默认是一个空数组--> */}
                    <GestureUnlock
                        firstDown={() => {
                            this.onceDraw();
                        }}
                        onDrawDone={() => {
                            this.fromNinePoint();
                        }}
                    ></GestureUnlock>
                    {/* //   <!-- @firstDown="onceDraw"是第一次输入密码的事件  @onDrawDone='fromNinePoint'第二次完成密码的事件 --> */}
                    <div className="bottom">
                        <p></p>
                        <div>
                            <a
                                className="btn_text"
                                onClick={() => {
                                    this.skip();
                                }}
                            >
                                {" "}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SetGestures;
