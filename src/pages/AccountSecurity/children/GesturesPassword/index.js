/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "../../styles/gesturesPassword.scss";
import { List, Switch, WhiteSpace } from "antd-mobile";
const Item = List.Item;
class GesturesPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showGetures: false,
            checked: false
        };
    }

    componentDidMount() {}

    render() {
        const {showGetures}=this.state;
        return (
            <div className="gesturesPassword-wrapper">
                <List className="gesturesPassword-list">
                    <Item
                        extra={
                            <Switch
                                checked={this.state.showGetures}
                                onChange={() => {
                                    this.setState({
                                        showGetures: !this.state.showGetures
                                    },()=>{
                                        if(this.state.showGetures){
                                            this.props.history.push('/accountSecurity/setGestures')
                                        }
                                    });
                                }}
                            />
                        }
                    >
                        手势密码开关
                    </Item>
                    <WhiteSpace />
                    {showGetures ? (
                        <Fragment>
                            <Item
                                extra={
                                    <Switch
                                        checked={this.state.checked}
                                        onChange={() => {
                                            this.setState({
                                                checked: !this.state.checked
                                            });
                                        }}
                                    />
                                }
                            >
                                显示手势轨迹
                            </Item>
                            <WhiteSpace />
                            <Item
                                extra=""
                                arrow="horizontal"
                                onClick={() => {
                                    // this.props.history.push("/accountSecurity/facePay");
                                }}
                            >
                                修改手势密码
                            </Item>
                            <Item
                                extra=""
                                arrow="horizontal"
                                onClick={() => {
                                    // this.props.history.push("/accountSecurity/facePay");
                                }}
                            >
                                忘记手势密码
                            </Item>
                        </Fragment>
                    ) : null}
                </List>
                <div className="prompt-option">
                    <span>手势密码同时也是您的辅助登录密码</span>
                </div>
            </div>
        );
    }
}

export default GesturesPassword;
