/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "../../styles/faceScan.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import { List, Button, Modal, Toast, Switch } from "antd-mobile";
import Loading from "@/components/Loading/Loading";
const Item = List.Item;
const alert = Modal.alert;

class FaceScan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            checked: false
        };
    }

    componentDidMount() {}
    openSubmit = () => {
        const { isOpen } = this.state;
        Loading.show();
        setTimeout(() => {
            this.setState(
                {
                    isOpen: !isOpen
                },
                () => {
                    Loading.hide();
                    Toast.success("开通成功", 1);
                }
            );
        }, 500);
    };
    submit = () => {
        const { isOpen } = this.state;
        const alertInstance = alert(
            "确定要注销FaceID吗?",
            "注销后每次需要输入密码",
            [
                {
                    text: "取消",
                    onPress: () => {},
                    style: "default"
                },
                {
                    text: "确定",
                    onPress: () => {
                        alertInstance.close();
                        Loading.show();
                        setTimeout(() => {
                            this.setState(
                                {
                                    isOpen: !isOpen,
                                    checked: !this.state.checked
                                },
                                () => {
                                    Loading.hide();
                                    Toast.success("注销成功", 1);
                                }
                            );
                        }, 500);
                        // const params = {
                        //     payId: payType.id,
                        //     totalAmount,
                        //     order_num
                        // };
                        // this.setState({
                        //     btnLoading: true,
                        //     showFacePay: true,
                        // });
                        // Loading.show();
                        // payOrder(params).then(res => {
                        //     this.setState({
                        //         btnLoading: false
                        //     });
                        //     Loading.hide();
                        //     if (!res) {
                        //         return;
                        //     }
                        //     if (res.code === 0) {
                        //         this.props.history.push({
                        //             pathname: "/result",
                        //             state: { payType, totalAmount }
                        //         });
                        //     } else if ([-3, -4].includes(res.code)) {
                        //         Toast.fail(res.msg, 1, () => {
                        //             this.props.history.push({
                        //                 pathname: "/myOrder",
                        //                 state: { key: 2 }
                        //             });
                        //         });
                        //     } else {
                        //         Toast.fail(res.msg, 1);
                        //     }
                        // });
                    }
                }
            ]
        );
    };
    render() {
        const { isOpen } = this.state;
        return (
            <div className="faceScan-wrapper">
                <div className="faceScan-list">
                    <div className="icon-wrapper">
                        <CustomIcon
                            icon="#iconicon-xin_shualian"
                            width="80px"
                            height="80px"
                            color="#0398FF"
                        />
                    </div>
                    <div className="describe">
                        <p>开启刷脸功能，体验便捷认证</p>
                    </div>
                    <div className="option-list">
                        <List>
                            <Item
                                extra={
                                    <Switch
                                        checked={this.state.checked}
                                        onChange={() => {
                                            this.setState({
                                                checked: !this.state.checked
                                            });
                                        }}
                                    />
                                }
                            >
                                刷脸
                            </Item>
                            <Item
                                extra="未验证"
                                arrow="horizontal"
                                onClick={() => {}}
                            >
                                人脸验证
                            </Item>
                        </List>
                    </div>
                    <div className="prompt-option">
                        <span>人脸验证后，您可刷脸使用登录和支付，更多功能敬请期待</span>
                    </div>
                </div>
            </div>
        );
    }
}

export default FaceScan;
