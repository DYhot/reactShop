/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "../../styles/equipmentManage.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import { List, Button, Modal, Toast, Switch } from "antd-mobile";
import Loading from "@/components/Loading/Loading";
const Item = List.Item;
const alert = Modal.alert;

class EquipmentManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: [
                {
                    id: 0,
                    name: "DY的iphone",
                    time: "2020-04-23",
                    type: 1
                },
                {
                    id: 1,
                    name: "Yang的iphone",
                    time: "2020-04-23",
                    type: 0
                }
            ]
        };
    }

    componentDidMount() {}
    renderContent = () => {
        const { listData } = this.state;
        return (
            <div className="container">
                {listData.map(item => (
                    <div className="list-item" key={item.id}>
                        <div className="left">
                            <div className="title">{item.name}</div>
                            <div className="subtitle">
                                <span>最近登录时间:</span>
                                <span>{item.time}</span>
                            </div>
                        </div>
                        <div className="right">
                            {item.type == 1 ? (
                                <span className="current-text">当前设备</span>
                            ) : null}
                            <span className="icon-wrapper">
                                <CustomIcon
                                    icon="#iconb-1"
                                    width="15px"
                                    height="15px"
                                    color="#CECED3"
                                />
                            </span>
                        </div>
                    </div>
                ))}
            </div>
        );
    };
    render() {
        return (
            <div className="equipmentManage-wrapper">
                <div className="title">管理您登录过的设备</div>
                <div className="container">{this.renderContent()}</div>
            </div>
        );
    }
}

export default EquipmentManage;
