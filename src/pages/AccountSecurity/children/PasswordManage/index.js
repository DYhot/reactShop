/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "../../styles/passwordManage.scss";
import { List, WhiteSpace } from "antd-mobile";
const Item = List.Item;
class PasswordManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {}

    render() {
        return (
            <div className="passwordManage-wrapper">
                <List className="passwordManage-list">
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            this.props.history.push("/user/forgetpwd");
                        }}
                    >
                        登录密码
                    </Item>
                    <Item
                        extra=""
                        arrow="horizontal"
                        onClick={() => {
                            // this.props.history.push("/accountSecurity/faceSet");
                        }}
                    >
                        支付密码
                    </Item>
                </List>
            </div>
        );
    }
}

export default PasswordManage;
