/**
 * Created by DengYang Li on 04/05/2019.
 */
import React ,{Fragment}from "react";
import "./styles/index.scss";
import { Steps } from "antd-mobile";
import { getLogisticsByOrdernum } from "@/api/order";
import Loading from "@/components/Loading/Loading";
import url from "url";
import CustomIcon from "@/components/Icon/CustomIcon";

class AfterSale extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            listData: []
        };
    }

    componentDidMount() {
        const query = url.parse(this.props.location.search, true).query;
        // this.getLogisticsByOrdernumData(query.order_num);
    }
    getLogisticsByOrdernumData(order_num) {
        Loading.show();
        getLogisticsByOrdernum({ order_num }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    listData: data
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    render() {
        const { listData } = this.state;
        return (
            <div className="afterSale-wrapper">
               
            </div>
        );
    }
}

export default AfterSale;
