/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "../styles/couponPopup.scss";
import {
    Modal,
    List,
    Button,
    WhiteSpace,
    WingBlank,
    Tabs,
    Radio,
    Badge,
    Checkbox
} from "antd-mobile";
import noCoupon from "@/assets/images/order/coupon-empty.png";
const CheckboxItem = Checkbox.CheckboxItem;
const RadioItem = Radio.RadioItem;
const tabs = [
    { title: <Badge text={"3"}>可使用优惠券</Badge>, key: "t1" },
    { title: <Badge text={"0"}>不可使用优惠券</Badge>, key: "t2" }
];
class CouponPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            modal2: false,
            value: "",
            couponAmount:0,
            currentTab: tabs[0],
            canUseData: [
                {
                    id: 0,
                    condition:"无使用门槛",
                    type:"通用优惠券",
                    amount:1.5,
                    period_validity:"2021.02.20",
                    selected: false
                },
                {
                    id: 1,
                    condition:"无使用门槛",
                    type:"通用优惠券",
                    amount:2.5,
                    period_validity:"2021.03.01",
                    selected: false
                }
            ]
        };
    }
    componentDidMount() {
        this.props.onCouponRef(this);
    }
    onCouponOpen = key => {
        this.setState({
            [key]: true
        });
    };
    onClose = key => () => {
        console.log(222,key)
        this.setState({
            [key]: false
        });
    };
    modalAfterClose = () => {
        const { currentTab, couponAmount } = this.state;
        // console.log("yyy",currentTab, couponAmount);
        this.props.setCouponAmount(couponAmount);
    };
    onChange = value => {
        console.log("checkbox");
        this.setState({
            value
        });
    };
    checkboxOnChange = val => {
        // console.log("xxx",val)
        const { canUseData } = this.state;
        let amount=0;
        canUseData.forEach((item, index) => {
            if (item.id == val) {
                item.selected = true;
                amount=item.amount;
            } else {
                item.selected = false;
            }
        });
        this.setState({
            canUseData,
            modal2:false,
            couponAmount:amount
        });
    };
    noUseCoupon=()=>{
        const { canUseData } = this.state;
        canUseData.forEach((item, index) => {
                item.selected = false;
        });
        this.setState({
            canUseData,
            modal2:false,
            couponAmount:0
        });
    }
    renderContent = tab => {
        const { canUseData, value, currentTab } = this.state;
        // console.log(123, tab, currentTab.key, tab.key);
        return (
            <div
                style={{
                    display: "flex",
                    alignItems: "flex-start",
                    justifyContent: "center",
                    height: "100%",
                    backgroundColor: "#fff"
                }}
                className="radio-wrapper"
            >
                <List className="coupon-checkbox-wrapper">
                    {tab.key==='t1'?canUseData.map(i => (
                        <CheckboxItem
                            key={i.id}
                            defaultChecked={i.selected}
                            checked={i.selected}
                            onChange={() => this.checkboxOnChange(i.id)}
                        >
                            <div className="coupon-item">
                                <div className="coupon-item-left">
                                    <p className="right-amount">{i.amount}元</p>
                                    <p>{i.condition}</p>
                                    <p>最多优惠{i.amount}元</p>
                                </div>
                                <div className="coupon-item-right">
                                    <p className="right-title">{i.type}</p>
                                    <p>
                                        <span>有效期:</span>
                                        <span>{i.period_validity}</span>
                                    </p>
                                </div>
                            </div>
                        </CheckboxItem>
                    )):<div className="noCoupon">
                        <img src={noCoupon} alt=""/>
                        <p>暂无优惠券</p>
                    </div>}
                </List>
            </div>
        );
    };

    render() {
        return (
            <WingBlank>
                <div>
                    <Modal
                        popup
                        className="couponPopup_wrapper"
                        visible={this.state.modal2}
                        onClose={this.onClose("modal2")}
                        animationType="slide-up"
                        afterClose={() => {
                            this.modalAfterClose();
                        }}
                    >
                        <List
                            renderHeader={() => <div>优惠券</div>}
                            className="popup-list"
                        >
                            <div style={{ height: 400 }}>
                                <WhiteSpace />
                                <Tabs
                                    tabs={tabs}
                                    initialPage={"t1"}
                                    onChange={val => {
                                        console.log(22, val);
                                        this.setState({
                                            currentTab: val
                                        });
                                    }}
                                    renderTabBar={props => (
                                        <Tabs.DefaultTabBar
                                            {...props}
                                            page={2}
                                        />
                                    )}
                                >
                                    {this.renderContent}
                                </Tabs>
                                <WhiteSpace />
                            </div>
                        </List>
                        <List.Item>
                                <Button
                                    style={{ zIndex: 100 }}
                                    onClick={()=>this.noUseCoupon()}
                                    style={{
                                        color:'#333',
                                        fontSize:'14px'
                                    }}
                                >
                                    不使用优惠券
                                </Button>
                            </List.Item>
                    </Modal>
                    <WhiteSpace />
                </div>
            </WingBlank>
        );
    }
}

export default CouponPopup;
