/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "../styles/selectTime.scss";
import {
    Modal,
    List,
    Button,
    WhiteSpace,
    WingBlank,
    Tabs,
    Radio
} from "antd-mobile";
const RadioItem = Radio.RadioItem;
const tabs = [
    { title: "今天", key: "t1" },
    { title: "明天", key: "t2" }
];
class SelectTime extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal1: false,
            modal2: false,
            value: '',
            currentTab: tabs[0]
        };
    }
    componentDidMount() {
        this.props.onRef(this);
    }
    onOpen = key => {
        this.setState({
            [key]: true
        });
    };
    onClose = key => () => {
        this.setState({
            [key]: false
        });
    };
    modalAfterClose=()=>{
        const {currentTab,value}=this.state;
        console.log(currentTab,value,currentTab.title+value);
        this.props.setDeliveryTime(currentTab.title+value);
    }
    onChange = value => {
        console.log("checkbox");
        this.setState({
            value
        });
    };
    renderContent = tab => {
        const { value,currentTab } = this.state;
        console.log(123, tab,currentTab.key,tab.key);
        const data1 = [
            { value: "09:00-11:00", label: "09:00-11:00" },
            { value: "11:00-13:00", label: "11:00-13:00" },
            { value: "13:00-15:00", label: "13:00-15:00" },
            { value: "15:00-17:00", label: "15:00-17:00" },
            { value: "17:00-19:00", label: "17:00-19:00" },
            { value: "19:00-21:00", label: "19:00-21:00" },
            { value: "21:00-23:00", label: "21:00-23:00" }
        ];
        const data2 = [
            { value: "09:00-11:00", label: "09:00-11:00" },
            { value: "11:00-13:00", label: "11:00-13:00" },
            { value: "13:00-15:00", label: "13:00-15:00" },
            { value: "15:00-17:00", label: "15:00-17:00" },
            { value: "17:00-19:00", label: "17:00-19:00" },
            { value: "19:00-21:00", label: "19:00-21:00" },
            { value: "21:00-23:00", label: "21:00-23:00" }
        ];
        const data = tab.key === "t1" ? data1 : data2;
        return (
            <div
                style={{
                    display: "flex",
                    alignItems: "flex-start",
                    justifyContent: "center",
                    height: "100%",
                    backgroundColor: "#fff"
                }}
                className="radio-wrapper"
            >
                <List>
                    {data.map(i => (
                        <RadioItem
                            key={i.value+tab.key}
                            checked={(currentTab.key===tab.key)&&(value === i.value)}
                            onChange={() => this.onChange(i.value)}
                        >
                            {i.label}
                        </RadioItem>
                    ))}
                </List>
            </div>
        );
    };

    render() {
        return (
            <WingBlank>
                <div>
                    <Modal
                        popup
                        className="selectTime_wrapper"
                        visible={this.state.modal2}
                        onClose={this.onClose("modal2")}
                        animationType="slide-up"
                        afterClose={() => {
                            this.modalAfterClose()
                        }}
                    >
                        <List
                            renderHeader={() => <div>送达时间</div>}
                            className="popup-list"
                        >
                            <div style={{ height: 300 }}>
                                <WhiteSpace />
                                <Tabs
                                    tabs={tabs}
                                    initialPage={"t1"}
                                    tabBarPosition="left"
                                    tabDirection="vertical"
                                    animated={true}
                                    onChange={(val)=>{
                                        console.log(22, val);
                                        this.setState({
                                            currentTab:val
                                        })
                                    }}
                                    renderTabBar={props => (
                                        <Tabs.DefaultTabBar
                                            {...props}
                                            page={2}
                                        />
                                    )}
                                >
                                    {this.renderContent}
                                </Tabs>
                                <WhiteSpace />
                            </div>
                            <List.Item>
                                <Button
                                    style={{ zIndex: 100 }}
                                    type="primary"
                                    onClick={this.onClose("modal2")}
                                >
                                    确定
                                </Button>
                            </List.Item>
                        </List>
                    </Modal>
                    <WhiteSpace />
                </div>
            </WingBlank>
        );
    }
}

export default SelectTime;
