/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import SelectTime from "./components/SelectTime"; //视频没回答时间选择
import CouponPopup from "./components/CouponPopup"; //优惠券选择
import { getUserDefaultAdress } from "@/api/address";
import { createGoodsOrder } from "@/api/order";
import { formateArea, formateAmount } from "@/utils/tool";
import {
    List,
    Checkbox,
    Flex,
    Button,
    WhiteSpace,
    InputItem,
    Switch,
    Toast
} from "antd-mobile";
const CheckboxItem = Checkbox.CheckboxItem;
const AgreeItem = Checkbox.AgreeItem;
const Item = List.Item;
const Brief = Item.Brief;
import { createForm } from "rc-form";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";
import moment from "moment";

class Settlement extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            disabled: false,
            adressData: null,
            goodsListData: [],
            totalAmount: 0,
            totalCount: 0,
            freight: 0,
            integral: 800,
            deliveryTime: "",
            couponAmount: 0,
            payType: null,
            data: [
                {
                    id: 0,
                    label: "微信支付",
                    icon: "#iconweixinzhifu",
                    selected: true
                },
                {
                    id: 1,
                    label: "支付宝支付",
                    icon: "#iconzhifubaozhifu",
                    selected: false
                }
            ]
        };
    }

    componentDidMount() {
        this.onReset();
        const params = this.props.location.state;
        this.setState({
            payType: this.state.data[0]
        });
        if (params) {
            if (Object.keys(params).includes("prePageState")) {
                const { rowData, prePageState } = params;
                this.setState({
                    ...prePageState,
                    adressData: rowData
                });
            } else {
                const { cartListData, totalCount, totalAmount } = params;
                this.setState({
                    goodsListData: cartListData,
                    totalCount,
                    totalAmount
                });
                this.getUserDefaultAdressData();
            }
        }
    }
    getUserDefaultAdressData() {
        Loading.show();
        getUserDefaultAdress().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    adressData: data
                });
            } else {
                Toast.info(res.msg, 3);
            }
        });
    }
    onRef = ref => {
        this.child = ref;
    };
    onCouponRef = ref => {
        this.childCoupon = ref;
    };
    onReset = () => {
        this.props.form.resetFields();
    };
    onSubmit = () => {
        const {
            payType,
            totalAmount,
            totalCount,
            goodsListData,
            freight,
            adressData
        } = this.state;
        this.props.form.validateFields({ force: true }, error => {
            if (!error) {
                console.log("表单参数：", this.props.form.getFieldsValue());
                const { note } = this.props.form.getFieldsValue();
                const idArr = goodsListData.map(item => {
                    return item.id;
                });
                const params = {
                    count: totalCount, //总件数
                    couponId: 0, //优惠券id//
                    payType: payType ? payType.id : "", //支付方式
                    goods_id: idArr.join(","), //商品id集合
                    deliveryTime: moment(new Date()).format("YYYY-MM-DD HH:mm:ss"), //送达时间//
                    integral: 0, //积分//
                    freight: freight, //配送费
                    amount: totalAmount < 0 ? "0.00" : totalAmount, //总金额
                    adress_id: adressData.id, //地址id
                    note: note || "" //备注
                };
                console.log("params:", params);
                this.setState({
                    disabled: true
                });
                Loading.show();
                createGoodsOrder(params).then(res => {
                    this.setState({
                        disabled: false
                    });
                    Loading.hide();
                    if (!res) {
                        return;
                    }
                    if (res.code === 0) {
                        const data = res.data.records;
                        this.props.history.push({
                            pathname: "/paymentDetail",
                            state: {
                                payType,
                                totalAmount,
                                order_num: data.order_num,
                                time: data.time
                            }
                        });
                    } else if (res.code === -3) {
                        Toast.fail(res.msg, 3, () => {
                            this.props.history.push({
                                pathname: "/myOrder",
                                state: { type: 1 }
                            });
                        });
                    } else {
                        Toast.fail(res.msg, 3);
                    }
                });
            } else {
                alert("Validation failed");
            }
        });
    };
    integralChange = val => {
        const { totalAmount, integral } = this.state;
        if (val) {
            this.setState({
                totalAmount: totalAmount - integral / 100
            });
        } else {
            this.setState({
                totalAmount: totalAmount + integral / 100
            });
        }
    };
    onChange = val => {
        const { data } = this.state;
        let payType;
        data.forEach((item, index) => {
            if (item.id == val) {
                item.selected = true;
                payType = item;
            } else {
                item.selected = false;
            }
        });
        this.setState({
            data,
            payType
        });
    };
    setDeliveryTime = val => {
        this.setState({
            deliveryTime: val
        });
    };
    setCouponAmount = val => {
        const { totalAmount, couponAmount } = this.state;
        if (val > 0) {
            this.setState({
                couponAmount: val,
                totalAmount: formateAmount(
                    parseFloat(totalAmount) + couponAmount - val
                )
            });
        } else {
            this.setState({
                couponAmount: val,
                totalAmount: formateAmount(
                    parseFloat(totalAmount) + couponAmount
                )
            });
        }
    };
    goToAdress = () => {
        this.props.history.push({
            pathname: "/address",
            state: { type: "order", prePageState: this.state }
        });
    };
    goToGoodList = () => {
        const { goodsListData, totalCount } = this.state;
        this.props.history.push({
            pathname: "/goodsList",
            state: { goodsListData, totalCount, prePageState: this.state }
        });
    };
    render() {
        const { history } = this.props;
        const { getFieldProps, getFieldError } = this.props.form;
        const {
            disabled,
            adressData,
            data,
            totalAmount,
            totalCount,
            goodsListData,
            freight,
            integral,
            deliveryTime,
            couponAmount
        } = this.state;
        return (
            <div className="settlement_wrapper">
                <form>
                    <List className="address-list">
                        <Item
                            className="select-address"
                            arrow="horizontal"
                            onClick={() => {
                                this.goToAdress();
                                // this.props.history.push({pathname:"/address?type=111"});
                            }}
                        >
                            {adressData ? (
                                <div className="address-info">
                                    <div className="iconWrapper">
                                        <CustomIcon
                                            icon="#iconshouhuodizhi"
                                            width="25px"
                                            height="25px"
                                            color="#00D15B"
                                        />
                                    </div>
                                    <div className="text-info">
                                        <p>
                                            <span>姓名:</span>
                                            <span
                                                style={{ marginRight: "20px" }}
                                            >
                                                {adressData.receive_name}
                                            </span>
                                            <span>电话:</span>
                                            <span>
                                                {adressData.receive_phone}
                                            </span>
                                        </p>
                                        <p>
                                            {formateArea(
                                                adressData.receive_area
                                            ) +
                                                adressData.receive_detail_address}
                                        </p>
                                    </div>
                                </div>
                            ) : (
                                <Fragment>
                                    <span className="icon_wrapper">
                                        <CustomIcon
                                            icon="#icontianjia"
                                            width="25px"
                                            height="25px"
                                            color="#00D15B"
                                        />
                                    </span>
                                    <span>选择收货地址</span>
                                </Fragment>
                            )}
                        </Item>
                        <Item
                            className="deliveryTime"
                            multipleLine
                            arrow="horizontal"
                            extra={deliveryTime}
                            onClick={() => {
                                this.child.onOpen("modal2");
                            }}
                        >
                            <p>送达时间</p>
                            <Brief>
                                超过十分钟可获得积分补偿
                                <CustomIcon
                                    icon="#iconbangzhu"
                                    width="12px"
                                    height="12px"
                                    color="#999"
                                />
                            </Brief>
                        </Item>
                        <Item
                            extra={totalCount}
                            arrow="horizontal"
                            className="spe good_list"
                            wrap
                        >
                            <div
                                className="img_wrapper"
                                onClick={() => this.goToGoodList()}
                            >
                                <div className="list_wrapper">
                                    {goodsListData.length > 0
                                        ? goodsListData.map((item, index) => (
                                              <DefaultImage
                                                  key={item.id}
                                                  src={item.url}
                                              />
                                          ))
                                        : null}
                                </div>
                            </div>
                        </Item>
                    </List>
                    <SelectTime
                        onRef={this.onRef}
                        setDeliveryTime={val => {
                            this.setDeliveryTime(val);
                        }}
                    />
                    <List renderHeader={() => "支付方式"}>
                        {data.map(i => (
                            <CheckboxItem
                                key={i.id}
                                defaultChecked={i.selected}
                                checked={i.selected}
                                onChange={() => this.onChange(i.id)}
                            >
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon={i.icon}
                                        width="20px"
                                        height="20px"
                                        color="#00D15B"
                                    />
                                </span>
                                <span className="checkBox_text">{i.label}</span>
                            </CheckboxItem>
                        ))}
                    </List>
                    <List renderHeader={() => ""} className="coupons-list">
                        <Item
                            extra={
                                couponAmount === 0
                                    ? "2张可用"
                                    : `-${couponAmount}元`
                            }
                            arrow="horizontal"
                            onClick={() => {
                                this.childCoupon.onCouponOpen("modal2");
                            }}
                        >
                            优惠劵
                        </Item>
                        <Item
                            extra={
                                <Switch
                                    {...getFieldProps("integral", {
                                        initialValue: false,
                                        valuePropName: "checked"
                                    })}
                                    onChange={val => this.integralChange(val)}
                                    onClick={checked => {
                                        // set new value
                                        this.props.form.setFieldsValue({
                                            integral: checked
                                        });
                                    }}
                                />
                            }
                        >
                            {`使用${integral}积分,可抵扣￥${integral / 100}元`}
                        </Item>
                    </List>
                    <CouponPopup
                        onCouponRef={this.onCouponRef}
                        setCouponAmount={val => {
                            this.setCouponAmount(val);
                        }}
                    />
                    <List
                        className="note-list"
                        renderHeader={() => ""}
                        renderFooter={() =>
                            getFieldError("note") &&
                            getFieldError("note").join(",")
                        }
                    >
                        <InputItem
                            {...getFieldProps("note", {
                                // initialValue: 'little ant',
                                rules: [
                                    {
                                        required: false
                                    }
                                ]
                            })}
                            clear
                            error={!!getFieldError("note")}
                            onErrorClick={() => {
                                alert(getFieldError("note").join("、"));
                            }}
                            placeholder="选填，可以告诉我们您的特殊需求"
                        >
                            备注
                        </InputItem>
                    </List>
                    <List className="cost-list">
                        <Item extra={`￥${totalAmount}`}>商品金额</Item>
                        <Item extra={`￥${freight}`}>配送费</Item>
                    </List>
                    <div className="bottom_statistical">
                        <div className="amount_text">
                            <span>实付:</span>
                            <span className="amount_total">{`￥${
                                totalAmount < 0 ? "0.00" : totalAmount
                            }`}</span>
                        </div>
                        <div className="settlement_button">
                            <Button
                                type="warning"
                                disabled={disabled}
                                loading={disabled}
                                onClick={this.onSubmit}
                            >
                                提交订单
                            </Button>
                            <WhiteSpace />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const SettlementWrapper = createForm()(Settlement);
export default SettlementWrapper;
