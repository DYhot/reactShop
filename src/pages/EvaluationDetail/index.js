/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import { Steps } from "antd-mobile";
import { getEvaluationList } from "@/api/evaluation";
import { encryptionString } from "@/utils/tool";
import Loading from "@/components/Loading/Loading";
import url from "url";
import CustomIcon from "@/components/Icon/CustomIcon";
import StarRatings from "react-star-ratings";
import WxImageViewer from "react-wx-images-viewer";
import DefaultImage from "@/components/DefaultImage/index";
import LazyLoad from 'react-lazyload';
class EvaluationDetail extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            listData: [],
            good_id: null,
            total: 0,
            current: 1,
            size: 1000,
            imags: [],
            index: 0,
            isOpen: false
        };
    }

    componentDidMount() {
        const query = url.parse(this.props.location.search, true).query;
        this.setState(
            {
                good_id: query.good_id
            },
            () => {
                this.getEvaluationData();
            }
        );
    }
    getEvaluationData(order_num) {
        const { good_id, current, size } = this.state;
        const params = {
            good_id,
            current,
            size
        };
        Loading.show();
        getEvaluationList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const total = res.data.total;
                this.setState({
                    listData: data,
                    total
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    renderContent = () => {
        const { listData } = this.state;
        return listData.length > 0
            ? listData.map(item => (
                  <div className="list-wrapper" key={item.id}>
                      <div className="head">
                          <div className="top">
                              {item.user_head_img ? (
                                  <DefaultImage src={item.user_head_img} />
                              ) : item.user_gender == 1 ? (
                                  <CustomIcon
                                      icon="#iconicon-test"
                                      width="35px"
                                      height="35px"
                                      color="#fff"
                                  />
                              ) : (
                                  <CustomIcon
                                      icon="#iconicon-test1"
                                      width="35px"
                                      height="35px"
                                      color="#fff"
                                  />
                              )}
                              <span>{encryptionString(item.user_name)}</span>
                          </div>
                          <div className="bottom">
                              <StarRatings
                                  rating={Number(item.good_star)}
                                  starRatedColor="#FF2B00"
                                  numberOfStars={5}
                                  name="rating"
                                  starDimension="15px"
                                  starSpacing="2px"
                              />
                              <span>{item.create_time}</span>
                          </div>
                      </div>
                      <div className="text">{item.content}</div>
                      <div className="image-list">
                          {item.imageArray.length > 0
                              ? item.imageArray.map((value, key) => (
                                    <div
                                        key={value.url}
                                        className="image-wrapper"
                                        onClick={this.openViewer.bind(
                                            this,
                                            key,
                                            item.imageArray
                                        )}
                                    >
                                        <LazyLoad width={"100px"}>
                                            <DefaultImage src={value.url} />
                                        </LazyLoad>
                                    </div>
                                ))
                              : null}
                      </div>
                  </div>
              ))
            : null;
    };
    onClose = () => {
        this.setState({
            isOpen: false
        });
    };

    openViewer(index, imageArray) {
        let imags = [];
        if (imageArray && imageArray.length > 0) {
            imags = imageArray.map(item => {
                return item.url;
            });
        }
        this.setState({
            imags,
            index,
            isOpen: true
        });
    }
    render() {
        const { listData, imags, index, isOpen } = this.state;
        return (
            <div className="evaluationDetail-wrapper">
                {this.renderContent()}
                {isOpen ? (
                    <WxImageViewer
                        onClose={this.onClose}
                        urls={this.state.imags}
                        index={index}
                    />
                ) : (
                    ""
                )}
            </div>
        );
    }
}

export default EvaluationDetail;
