/**
 * Created by Daguang Li on 11/27/2017.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import { List, WhiteSpace, Toast, Button, Badge } from "antd-mobile";
const Item = List.Item;
import introduceImg from "@/assets/images/detail/introduce.jpg";
import { getGoodsDetailById } from "@/api/goods";
import CustomIcon from "@/components/Icon/CustomIcon";
import ToTop from "@/components/BackToTop/ToTop";
import Loading from "@/components/Loading/Loading";
import { addGoodToCart, getCartTotalCount } from "@/api/cart";
import DefaultImage from "@/components/DefaultImage/index";

class Detail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            totalCount: 0,
            evaluationCount: 0,
            goodId: null
        };
    }

    componentDidMount() {
        // console.log("详情页接受到的参数：", this.props.match.params);
        const goodId = this.props.match.params
            ? this.props.match.params.id
            : "";
        this.setState({
            goodId
        });
        this.getGoodsDetailData(goodId);
        this.getCartTotalCountData();
    }
    getGoodsDetailData(id) {
        Loading.show();
        getGoodsDetailById({ id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const evaluationCount = res.data.evaluationCount;
                this.setState({
                    data,
                    evaluationCount
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    getCartTotalCountData(id) {
        Loading.show();
        getCartTotalCount().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data;
                this.setState({
                    totalCount: data
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    addToCart = () => {
        Loading.show();
        addGoodToCart({ id: this.props.match.params.id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("成功加入购物车", 1);
                this.getCartTotalCountData();
            } else if(res.code === -3){
                Toast.info(res.msg, 2);
            }else{
                Toast.fail(res.msg, 3);
            }
        });
    };
    render() {
        const { data, totalCount, evaluationCount, goodId } = this.state;
        return (
            <div className="detail_wrapper">
                <ToTop />
                {data ? (
                    <Fragment>
                        <div className="header-content-Wrapper">
                            <DefaultImage src={data.url}/>
                            <div className="good-info">
                                <p className="title">{data.title}</p>
                                <p className="describe">{data.describe}</p>
                                <p className="amount-count">
                                    <span>
                                        <span>￥{data.price}</span>
                                        <span className="sold">
                                            (库存:{data.count})
                                        </span>
                                    </span>
                                    <span>
                                        {data.count < 1 ? (
                                            <span className="sold-out-text">
                                                售罄
                                            </span>
                                        ) : null}
                                        <span>已售{data.sold}</span>
                                    </span>
                                </p>
                            </div>
                            <List className="good-describe-list">
                                <Item>
                                    此商品按500g/份计价,如实收少于500g将退还差价
                                </Item>
                                <Item>最快29分钟内送达</Item>
                            </List>
                            <List
                                className="evaluation-list"
                                onClick={e => {
                                    evaluationCount &&
                                        this.props.history.push({
                                            pathname: `/evaluationDetail`,
                                            search: `?good_id=${goodId}`
                                        });
                                }}
                            >
                                <Item>
                                    <span>评价({evaluationCount})</span>
                                    <span>查看全部</span>
                                </Item>
                            </List>
                        </div>
                        <WhiteSpace />
                        <div className="detail-content-wrapper">
                            <List
                                renderHeader={() => "规格"}
                                className="specifications-list"
                            >
                                <Item extra={"冷藏"}>保存条件</Item>
                                <Item extra={"15天"}>保质期</Item>
                            </List>
                            <WhiteSpace />
                            <List className="good-img-list">
                                <DefaultImage src={data.url} />
                            </List>
                            <List className="introduce-img-list">
                                <DefaultImage src={introduceImg}/>
                            </List>
                        </div>
                    </Fragment>
                ) : null}
                <div className="bottom-wrapper">
                    <div
                        className="left"
                        onClick={() => {
                            this.props.history.push({ pathname: `/cart` });
                        }}
                    >
                        <Badge text={totalCount}>
                            <CustomIcon
                                icon="#icongouwuche"
                                width="30px"
                                height="30px"
                                color="#999"
                            />
                        </Badge>
                    </div>
                    <div className="right">
                        <Button
                            disabled={data && data.count > 0 ? false : true}
                            onClick={() => {
                                this.addToCart();
                            }}
                        >
                            加入购物车
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Detail;
