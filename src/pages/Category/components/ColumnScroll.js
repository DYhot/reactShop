import React from "react";
import BScroll from "better-scroll";
import "../styles/columnScroll.scss";
import { getCategoryMenu, getGoodsByCategoryId } from "@/api/category";

class ColumnScroll extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            total: 0,
            currentTab: "",
            goodsListArray: [],
            goodsTotal: 0,
            activeTab: null,
            tabsArray: []
    };
        this.ColumnScroll = null;
    }

    componentDidMount() {
        this.props.onRef(this);
        const wrapper = document.querySelector(".wrapper");
        //选中DOM中定义的 .wrapper 进行初始化
        this.ColumnScroll = new BScroll(wrapper, {
            scrollX: false, //开启横向滚动
            click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
            scrollY: true, //关闭竖向滚动
            // bounce: false,
            probeType:3,
        });
        // this.getCategoryData();
    }
    getCategoryData() {
        const { categoryId } = this.props;
        // this.setLoading(true);
        getCategoryMenu().then(res => {
            // Toast.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                if (res.data.records.length > 0) {
                    let tabs = [];
                    data.forEach((item, index) => {
                        const target = {
                            title: item.categoryName,
                            key: item.id
                        };
                        tabs.push(target);
                    });
                    this.setState(
                        {
                            activeTab: categoryId ? categoryId : tabs[0].key,
                            total: res.data.total,
                            tabsArray: tabs
                        },
                        () => {
                            setTimeout(()=>{
                                const el=this.refs[`columnLi${categoryId}`];
                                this.ColumnScroll.scrollToElement(el,500);
                            },0)
                            this.props.getGoodsByCategoryIdData(
                                this.state.activeTab
                            );
                        }
                    );
                }
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    tabClick = row => {
        const {activeTab}=this.state;
        if(row.key==activeTab){
            return;
        }
        this.setState({
            activeTab: row.key
        });
        this.props.getGoodsByCategoryIdData(row.key);
    };
    componentWillUnmount() {
        //处理逻辑
        console.log("组件卸载");
    }
    renderContent = () => {
        const { activeTab, tabsArray } = this.state;
        return tabsArray.length > 0
            ? tabsArray.map((item, index) => {
                  return (
                      <li
                          ref={`columnLi${item.key}`}
                          key={item.key}
                          className={`item ${
                              activeTab == item.key ? "tab-active" : ""
                          }`}
                          onClick={() => {
                              this.tabClick(item);
                          }}
                      >
                          {item.title}
                      </li>
                  );
              })
            : null;
    };

    render() {
        return (
            <div className="wrapper columnScroll-wrapper">
                <ul className="content">{this.renderContent()}</ul>
            </div>
        );
    }
}

export default ColumnScroll;
