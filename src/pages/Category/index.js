import React from "react";
import styles from "./styles/index.scss";
import { Tabs, WhiteSpace, Toast, List, SearchBar } from "antd-mobile";
import { getGoodsByCategoryId } from "@/api/category";
import { addGoodToCart } from "@/api/cart";
import ColumnScroll from "./components/ColumnScroll";
import IconCart from "@/components/Icon/IconCart";
import LoadingImg from "@/components/Loading/LoadingImg";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";
import CustomIcon from "@/components/Icon/CustomIcon";
import url from "url";

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pageNum: 0,
            pageSize: 10,
            total: 0,
            currentTab: "",
            goodsListArray: [],
            goodsTotal: 0,
            showLoading: false,
            categoryId: null,
            init: false,
            value: null
        };
    }
    componentDidMount() {
        // console.log(34,this.props.location.search)
        const query = url.parse(this.props.location.search, true).query;
        this.setState(
            {
                categoryId: query.type,
                currentTab: query.type
            },
            () => {
                this.child.getCategoryData();
            }
        );
    }
    onRef = ref => {
        this.child = ref;
    };
    getGoodsByCategoryIdData({ id = null, value = null }) {
        const { activeTab } = this.child.state;
        const { pageNum,pageSize } = this.state;
        this.setState({
            showLoading: true
        });
        let params = {
            current: pageNum + 1,
            size: pageSize
        };
        value
            ? (params.searchValue = value)
            : id
            ? (params.id = id)
            : (params.id = activeTab);
        getGoodsByCategoryId(params).then(res => {
            this.setState({
                showLoading: false
            });
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    init: true,
                    goodsTotal: res.data.total,
                    goodsListArray: data ? data : []
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    tabsChange = val => {
        this.setState({
            currentTab: val
        });
        this.getGoodsByCategoryIdData({ id: val.key });
    };
    addToCart = (item, e) => {
        e.nativeEvent.stopImmediatePropagation();
        e.stopPropagation();
        if (item.count < 1) {
            return;
        }
        Loading.show();
        addGoodToCart({ id: item.id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("成功加入购物车", 1);
            } else {
                Toast.info(res.msg, 3);
            }
        });
    };
    renderContent = tab => {
        const { value, currentTab, goodsListArray, init } = this.state;
        // console.log(123, tab,currentTab.key,tab.key);
        return (
            <div
                style={{
                    display: "flex",
                    alignItems: "flex-start",
                    justifyContent: "center",
                    backgroundColor: "#fff"
                }}
                className="good_list_wrapper"
            >
                {goodsListArray.length > 0 ? (
                    goodsListArray.map(i => (
                        <div
                            className="list_item"
                            key={i.id}
                            onClick={() => {
                                this.skipToDetail(i.id);
                            }}
                        >
                            <div className="list_item_left">
                                <DefaultImage src={i.url} />
                            </div>
                            <div className="list_item_right">
                                <div className="right_top">
                                    <p className="title">{i.title}</p>
                                    <p className="describe">{i.describe}</p>
                                </div>
                                <div className="right_bottom">
                                    <p className="amount">￥{i.price}</p>
                                    {i.price != i.original_price && (
                                        <del className="amount_del">
                                            ￥{i.original_price}
                                        </del>
                                    )}
                                    <div
                                        className="iconCartWrapper"
                                        onClick={e => this.addToCart(i, e)}
                                    >
                                        {i.count > 0 ? (
                                            <IconCart />
                                        ) : (
                                            <span className="sold-out">
                                                售罄
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                ) : init ? (
                    <div className="empty_wrapper">
                        <span className="icon_wrapper">
                            <CustomIcon
                                icon="#iconzanwushuju"
                                width="45px"
                                height="45px"
                                color="#999"
                            />
                            <p>暂无数据</p>
                        </span>
                    </div>
                ) : null}
            </div>
        );
    };
    skipToDetail(id) {
        this.props.history.push({ pathname: `/detail/${id}` });
    }
    onSubmit = value => {
        this.setState({
            value
        });
        if (value) {
            this.getGoodsByCategoryIdData({ value });
        }
    };
    onClear = () => {
        const { value } = this.state;
        if (value) {
            this.getGoodsByCategoryIdData({
                id: this.state.currentTab || this.child.state.activeTab
            });
        }
    };
    render() {
        const { showLoading, categoryId } = this.state;
        return (
            <div className="category_wrapper" style={{ fontSize: "40px" }}>
                <div className="head_search">
                    <SearchBar
                        placeholder="输入商品名称"
                        maxLength={8}
                        onSubmit={value => this.onSubmit(value)}
                        onClear={value => this.onClear()}
                    />
                </div>
                {showLoading ? <LoadingImg /> : null}
                <div className="main_content">
                    <ColumnScroll
                        onRef={this.onRef}
                        categoryId={categoryId}
                        getGoodsByCategoryIdData={id => {
                            this.getGoodsByCategoryIdData({ id });
                        }}
                    />
                    <div className="right_content">
                        {this.renderContent()}
                    </div>
                    <WhiteSpace />
                </div>
            </div>
        );
    }
}
export default Category;
