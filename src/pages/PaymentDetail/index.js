/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import {
    List,
    Button,
    WhiteSpace,
    WingBlank,
    Checkbox,
    Modal,
    Accordion,
    Toast
} from "antd-mobile";
import CustomIcon from "@/components/Icon/CustomIcon";
import Loading from "@/components/Loading/Loading";
import FacePay from "@/components/FacePay/index";
import { getOrderCutDown, payOrder } from "@/api/order";
const CheckboxItem = Checkbox.CheckboxItem;
const alert = Modal.alert;

class PaymentDetail extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            btnLoading: false,
            activeKey: "1",
            payType: null,
            totalAmount: 0,
            order_num: "",
            time: "",
            minutes: 14,
            seconds: 59,
            data: [
                {
                    id: 0,
                    label: "微信支付",
                    icon: "#iconweixinzhifu",
                    selected: true
                },
                {
                    id: 1,
                    label: "支付宝支付",
                    icon: "#iconzhifubaozhifu",
                    selected: false
                }
            ],
            showFacePay:false,
        };
    }

    componentDidMount() {
        const params = this.props.location.state;
        const { payType, totalAmount, order_num, time } = params;
        const { data } = this.state;
        data.forEach((item, index) => {
            if (item.id == payType.id) {
                item.selected = true;
            } else {
                item.selected = false;
            }
        });
        // console.log(moment(new Date()).add(15, 'm').set({'minute': 00, 'second': 00}).toJSON())
        this.setState(
            {
                data,
                payType,
                totalAmount,
                order_num
            },
            () => {
                this.getOrderCutDownData();
            }
        );
    }
    getOrderCutDownData() {
        Loading.show();
        const { order_num } = this.state;
        const params = {
            order_num
        };
        getOrderCutDown(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const { minutes, seconds } = res.data.records;
                this.setState(
                    {
                        minutes,
                        seconds,
                        time: minutes + "分" + seconds + "秒"
                    },
                    () => {
                        this.timer = setInterval(() => this.CountDown(), 1000);
                    }
                );
            } else if (res.code === -4) {
                this.setState({
                    time: 0 + "分" + 0 + "秒"
                },()=>{
                    Toast.fail(res.msg, 1);
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    CountDown = () => {
        const { minutes, seconds } = this.state;
        if (minutes == 0 && seconds == 0) {
            clearInterval(this.timer);
        } else if (minutes >= 0) {
            if (seconds > 0) {
                this.setState({
                    seconds: seconds - 1
                });
            } else if (seconds == 0) {
                this.setState({
                    minutes: minutes - 1,
                    seconds: 59
                });
            }
            this.setState({
                time: minutes + "分" + seconds + "秒"
            });
        }
    };
    payTypeOnChange = val => {
        const { data, payType } = this.state;
        let newPayType = payType;
        data.forEach((item, index) => {
            if (item.id == val) {
                item.selected = true;
                newPayType = item;
            } else {
                item.selected = false;
            }
        });
        this.setState({
            data,
            payType: newPayType,
            activeKey: "1"
        });
    };
    surePayment = () => {
        const { payType, totalAmount, order_num } = this.state;
        const alertInstance = alert("", "确定支付该订单?", [
            {
                text: "取消",
                onPress: () => {
                    this.props.history.push({
                        pathname: "/myOrder"
                    });
                },
                style: "default"
            },
            {
                text: "确定",
                onPress: () => {
                    alertInstance.close();
                    const params = {
                        payId: payType.id,
                        totalAmount,
                        order_num
                    };
                    this.setState({
                        btnLoading: true,
                        showFacePay: true,
                    });
                    Loading.show();
                    payOrder(params).then(res => {
                        this.setState({
                            btnLoading: false
                        });
                        Loading.hide();
                        if (!res) {
                            return;
                        }
                        if (res.code === 0) {
                            this.props.history.push({
                                pathname: "/result",
                                state: { payType, totalAmount }
                            });
                        } else if ([-3, -4].includes(res.code)) {
                            Toast.fail(res.msg, 1, () => {
                                this.props.history.push({
                                    pathname: "/myOrder",
                                    state: { key: 2 }
                                });
                            });
                        } else {
                            Toast.fail(res.msg, 1);
                        }
                    });
                }
            }
        ]);
    };
    accordionOnChange = () => {};
    render() {
        const {
            activeKey,
            data,
            payType,
            totalAmount,
            order_num,
            time,
            btnLoading,
            showFacePay
        } = this.state;
        return (
            <div className="paymentDetail-wrapper">
                {/* {showFacePay?<FacePay/>:null} */}
                <div className="top-content">
                    <div className="gradient_top"></div>
                    <div className="cutdown-time">支付剩余时间：{time}</div>
                    <div className="order-info">
                        <p className="order-number">订单号:{order_num}</p>
                    </div>
                    <div className="pay-type-list">
                        <List
                            renderHeader={() => {
                                return (
                                    <div>
                                        {payType ? (
                                            <Fragment>
                                                <span className="icon_wrapper">
                                                    <CustomIcon
                                                        icon={payType.icon}
                                                        width="20px"
                                                        height="20px"
                                                        color="#00D15B"
                                                    />
                                                </span>
                                                <span
                                                    style={{
                                                        marginLeft: "10px"
                                                    }}
                                                >
                                                    {payType.label}
                                                </span>
                                            </Fragment>
                                        ) : null}
                                    </div>
                                );
                            }}
                        >
                            <Accordion
                                defaultActiveKey={activeKey}
                                className="my-accordion"
                                onChange={this.accordionOnChange}
                            >
                                <Accordion.Panel header="选择其他支付方式">
                                    {data.map(i => (
                                        <CheckboxItem
                                            key={i.id}
                                            defaultChecked={i.selected}
                                            checked={i.selected}
                                            onChange={() =>
                                                this.payTypeOnChange(i.id)
                                            }
                                        >
                                            <span className="icon_wrapper">
                                                <CustomIcon
                                                    icon={i.icon}
                                                    width="20px"
                                                    height="20px"
                                                    color="#00D15B"
                                                />
                                            </span>
                                            <span className="checkBox_text">
                                                {i.label}
                                            </span>
                                        </CheckboxItem>
                                    ))}
                                </Accordion.Panel>
                            </Accordion>
                        </List>
                    </div>
                </div>
                <div className="bottom-content">
                    <p className="text-row">
                        <span>需支付</span>
                        <span>￥{totalAmount}</span>
                    </p>
                    <Button
                        type="primary"
                        disabled={btnLoading}
                        onClick={() => this.surePayment()}
                    >
                        确认付款
                    </Button>
                    <WhiteSpace />
                </div>
            </div>
        );
    }
}

export default PaymentDetail;
