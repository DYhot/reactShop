/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import { Steps } from "antd-mobile";
import { getLogisticsByOrdernum } from "@/api/order";
import Loading from "@/components/Loading/Loading";
import url from "url";
import CustomIcon from "@/components/Icon/CustomIcon";
const Step = Steps.Step;

class Logistics extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            listData: []
        };
    }

    componentDidMount() {
        const query = url.parse(this.props.location.search, true).query;
        this.getLogisticsByOrdernumData(query.order_num);
    }
    getLogisticsByOrdernumData(order_num) {
        Loading.show();
        getLogisticsByOrdernum({ order_num }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    listData: data
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    render() {
        const { listData } = this.state;
        return (
            <div className="logistics-wrapper">
                {listData.length > 0 ? (
                    <Steps current={0}>
                        {listData.map(item => (
                            <Step
                                key={item.id}
                                title={item.title}
                                icon={
                                    item.id == 0 ? (
                                        <CustomIcon
                                            icon="#iconwuliu"
                                            width="25px"
                                            height="25px"
                                            color="#666"
                                        />
                                    ) : (
                                        <CustomIcon
                                            icon="#iconyuandian"
                                            width="20px"
                                            height="20px"
                                            color="#c5c5c5"
                                        />
                                    )
                                }
                                description={
                                    <Fragment>
                                        <span>{item.description}</span>
                                        <p>
                                            {item.create_time}
                                        </p>
                                    </Fragment>
                                }
                            />
                        ))}
                    </Steps>
                ) : (
                    <div className="empty-data">暂无物流信息</div>
                )}
            </div>
        );
    }
}

export default Logistics;
