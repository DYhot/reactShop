/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import { WhiteSpace } from "antd-mobile";
import "./styles/goodlist.scss";
import DefaultImage from "@/components/DefaultImage/index";

class GoodList extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            prePageState: null,
            goodsListData: [],
            totalCount: 0
        };
    }

    componentDidMount() {
        const params = this.props.location.state;
        if (params) {
            const { goodsListData, totalCount, prePageState } = params;
            this.setState({
                prePageState,
                goodsListData,
                totalCount
            });
        }
    }
    skipToDetail(good_id) {
        this.props.history.push({ pathname: `/detail/${good_id}` });
    }
    renderContent = () => {
        const { goodsListData } = this.state;
        return (
            <Fragment>
                {goodsListData.map(item => (
                    <div className="list-item" key={item.id}>
                        <div
                            className="left-img"
                            onClick={() => {
                                this.skipToDetail(item.good_id);
                            }}
                        >
                            <DefaultImage src={item.url}/>
                        </div>
                        <div className="right-wrapper">
                            <p className="title">
                                <span>{item.title}</span>
                                <span>{item.price}</span>
                            </p>
                            <p className="describe">{item.describe}</p>
                        </div>
                    </div>
                ))}
            </Fragment>
        );
    };
    render() {
        const { totalCount } = this.state;
        return (
            <div className="goodlist-wrapper">
                <WhiteSpace />
                <div className="head-title">
                    <span>商品</span>
                    <span>{totalCount}</span>
                </div>
                {this.renderContent()}
            </div>
        );
    }
}

export default GoodList;
