import React from "react";
import { Link } from "react-router-dom";
import "./styles/index.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import { connect } from "react-redux";
import { WhiteSpace, Badge, Toast } from "antd-mobile";
import { getUserStatistical } from "@/api/order";
import { getUserInfo } from "@/api/user";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";
import { encryptionString } from "@/utils/tool";

class Mine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            unPay: 0,
            unDelivery: 0,
            unEvaluation: 0,
            refundCount: 0,
            couponCount: 0,
            userName: null,
            avatar: null,
            nikeName: null,
            gender: null
        };
    }
    componentDidMount() {
        this.getUserStatisticalData();
        this.getUserInfoData();
    }
    getUserInfoData() {
        Loading.show();
        getUserInfo().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    userName: data.user_name,
                    avatar: data.user_head_img,
                    nikeName: data.user_nikeName,
                    gender: [data.user_gender]
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    getUserStatisticalData(id) {
        Loading.show();
        getUserStatistical({ id }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    unPay: data.unPay || 0,
                    unDelivery: data.unDelivery || 0,
                    unEvaluation: data.unEvaluation || 0,
                    refundCount: data.refundCount || 0,
                    couponCount: data.couponCount || 0
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    render() {
        const { loginStatus } = this.props;
        const {
            unPay,
            unDelivery,
            unEvaluation,
            refundCount,
            couponCount,
            userName,
            avatar,
            nikeName,
            gender
        } = this.state;
        return (
            <div className="mine_wrapper">
                <div className="set_area">
                    <div className="small-point"></div>
                    <Link to="/notification">
                        <CustomIcon
                            icon="#iconyijian"
                            width="25px"
                            height="25px"
                            color="#fff"
                        />
                    </Link>
                </div>
                <div className="logo_area">
                    <div className="logo_area_img">
                        {avatar ? (
                            <DefaultImage src={avatar} />
                        ) : gender == 1 ? (
                            <CustomIcon
                                icon="#iconicon-test"
                                width="45px"
                                height="45px"
                                color="#fff"
                            />
                        ) : (
                            <CustomIcon
                                icon="#iconicon-test1"
                                width="45px"
                                height="45px"
                                color="#fff"
                            />
                        )}
                    </div>
                    <div className="logo_area_text">
                        {loginStatus.accessToken ? (
                            <span>
                                {encryptionString(loginStatus.userName)}
                            </span>
                        ) : (
                            <Link to="/user">
                                <span>立即登录</span>
                            </Link>
                        )}
                        <Link to="/userCenter/index" className="logo_icon">
                            <CustomIcon
                                icon="#iconb-1"
                                width="15px"
                                height="15px"
                                color="#00D15B"
                            />
                        </Link>
                    </div>
                </div>
                <Link to="/myOrder" className="mine_order">
                    <div className="mine_order_content">
                        <div>
                            <span className="icon_wrapper">
                                <CustomIcon
                                    icon="#icondingdan1"
                                    width="20px"
                                    height="20px"
                                    color="#00c35d"
                                />
                            </span>
                            <span>我的订单</span>
                        </div>
                        <div className="text_gray">
                            <span>查看全部订单</span>
                            <span className="arrow_icon">
                                <CustomIcon icon="#iconb-1" />
                            </span>
                        </div>
                    </div>
                </Link>
                <div className="nav_area">
                    <div className="nav_area_content">
                        <Link
                            to={{ pathname: `/myOrder`, state: { key: 1 } }}
                            className="nav_area_item"
                        >
                            <span className="nav_icon_wrapper">
                                {unPay > 0 ? (
                                    <Badge text={unPay} overflowCount={99} />
                                ) : null}
                                <CustomIcon
                                    icon="#icondaizhifu1"
                                    width="25px"
                                    height="25px"
                                    color="#000"
                                />
                            </span>
                            <span>待支付</span>
                        </Link>
                        <Link
                            to={{ pathname: `/myOrder`, state: { key: 2 } }}
                            className="nav_area_item"
                        >
                            <span className="nav_icon_wrapper">
                                {unDelivery > 0 ? (
                                    <Badge
                                        text={unDelivery}
                                        overflowCount={99}
                                    />
                                ) : null}
                                <CustomIcon
                                    icon="#icondaishouhuo"
                                    width="26px"
                                    height="26px"
                                    color="#000"
                                />
                            </span>
                            <span>待收货</span>
                        </Link>
                        <Link
                            to={{ pathname: `/myOrder`, state: { key: 3 } }}
                            className="nav_area_item"
                        >
                            <span className="nav_icon_wrapper">
                                {unEvaluation > 0 ? (
                                    <Badge
                                        text={unEvaluation}
                                        overflowCount={99}
                                    />
                                ) : null}
                                <CustomIcon
                                    icon="#icondaipingjia1"
                                    width="25px"
                                    height="25px"
                                    color="#000"
                                />
                            </span>
                            <span>待评价</span>
                        </Link>
                        <Link to="/afterSale" className="nav_area_item">
                            <span className="nav_icon_wrapper">
                                {refundCount > 0 ? (
                                    <Badge
                                        text={refundCount}
                                        overflowCount={99}
                                    />
                                ) : null}
                                <CustomIcon
                                    icon="#icontuikuanzhong"
                                    width="25px"
                                    height="25px"
                                    color="#000"
                                />
                            </span>
                            <span className="nav_area_item_title">
                                售后/退款
                            </span>
                        </Link>
                    </div>
                </div>
                <WhiteSpace />
                <div className="myInfo_area">
                    <Link to="/myCoupon" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconyouhui-1"
                                        width="22px"
                                        height="22px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>我的优惠卷</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    {couponCount > 0 ? (
                                        <Badge
                                            text={couponCount}
                                            overflowCount={99}
                                        />
                                    ) : null}
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                    <Link to="/address" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconaddress_icon"
                                        width="23px"
                                        height="23px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>我的收货地址</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                </div>
                <WhiteSpace />
                <div className="myInfo_area">
                    <Link to="/greenCard" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconvip-card"
                                        width="23px"
                                        height="23px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>我的绿卡</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                    <Link to="/paySetting" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconzhifu"
                                        width="24px"
                                        height="24px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>支付设置</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                </div>
                <WhiteSpace />
                <div className="myInfo_area">
                    <Link to="/accountSecurity" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconanquan"
                                        width="24px"
                                        height="24px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>账户与安全</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                    <Link to="/systemSet" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconset"
                                        width="20px"
                                        height="20px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>系统设置</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                </div>
                <WhiteSpace />
                <div className="myInfo_area">
                    <Link to="/luckDraw" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconchoujiang"
                                        width="24px"
                                        height="24px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>幸运抽奖</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                    <Link to="/aboutUs" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconguanyu"
                                        width="24px"
                                        height="24px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>关于我们</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                </div>
                <WhiteSpace />
                <div className="myInfo_area">
                    <Link
                        to="/"
                        onClick={e => {
                            e.preventDefault();
                            window.location.href = `tel:${10010}`
                        }}
                        className="myInfo_area_item"
                    >
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconweibiaoti-"
                                        width="25px"
                                        height="25px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>联系客服</span>
                            </div>
                            <div className="text_gray">
                                <span>客服时间 07:00-22:00 </span>
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                    <Link to="/feedBack" className="myInfo_area_item">
                        <div className="myInfo_area_item_content">
                            <div>
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#iconyijian"
                                        width="20px"
                                        height="20px"
                                        color="#00c35d"
                                    />
                                </span>
                                <span>意见反馈</span>
                            </div>
                            <div className="text_gray">
                                <span className="arrow_icon">
                                    <CustomIcon icon="#iconb-1" />
                                </span>
                            </div>
                        </div>
                    </Link>
                </div>
                <WhiteSpace />
                <div className="version_area">当前版本1.1.0</div>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    let { updateAppState } = state;
    return {
        loginStatus: updateAppState.loginStatus
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        // updateLoginStatus: (...params) => {
        //     dispatch(appAction.updateLoginStatus(...params));
        // }
    };
};
// const MineWrapper = createForm()(Mine);
const MineContainer = connect(mapStateToProps, mapDispatchToProps)(Mine);
export default MineContainer;
