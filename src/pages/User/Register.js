/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/register.scss";
import normalURL from "@/assets/images/login/normal.png";
import { getSmsCode, accountRegister } from "@/api/user";
import {
    validatePhone,
    validateCode,
    validateEmail,
    validatePassword
} from "@/utils/validate";
import Loading from "@/components/Loading/Loading";
import CustomIcon from "@/components/Icon/CustomIcon";
import { Button, List, InputItem, Toast, Radio } from "antd-mobile";
import { createForm } from "rc-form";
const Item = List.Item;
class Register extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: [],
            type: "money",
            btnLoading: false,
            sendCodeText: "获取验证码",
            email: "",
            phone: "",
            smsId: "",
            phoneCode: "",
            password: "",
            surePwd: "",
            canCode: true,
            isSendCode: false,
            agree: false,
            passwordType: "password",
            againPasswordType: "password"
        };
    }

    componentDidMount() {}
    getCode() {
        if (!validatePhone(this.state.phone)) {
            Toast.info("请输入正确的手机号", 2);
            return false;
        } else if (this.state.canCode) {
            this.state.canCode = false;
            var j = 60;
            Loading.show();
            getSmsCode({
                telephone: this.state.phone,
                v: this.state.markId,
                promoteId: 1
            }).then(res => {
                Loading.hide();
                if (res.code == 0) {
                    this.setState({
                        smsId: res.data.smsId,
                        phoneCode: res.data.smsCode,
                        isSendCode: true,
                        sendCodeText: j + "s"
                    });
                    var i = setInterval(() => {
                        j--;
                        if (j === 0) {
                            this.setState({
                                canCode: true,
                                sendCodeText: "重新获取"
                            });
                            clearInterval(i);
                        } else {
                            this.setState({
                                sendCodeText: j + "s"
                            });
                        }
                    }, 1000);
                } else {
                    Toast.info(res.msg, 2);
                    this.setState({
                        canCode: true
                    });
                }
            });
        }
    }
    sureSubmit = () => {
        if (!validateEmail(this.state.email)) {
            Toast.info("请输入正确的邮箱", 2);
            return false;
        }
        if (!validatePassword(this.state.password)) {
            Toast.info("请输入正确的密码", 2);
            return false;
        }
        if (this.state.password !== this.state.surePwd) {
            Toast.info("两次密码输入不一致", 2);
            return false;
        }
        if (!validatePhone(this.state.phone)) {
            Toast.info("请输入正确的电话号码", 2);
            return false;
        }
        if (!this.state.isSendCode) {
            Toast.info("请先获取验证码", 2);
            return false;
        }
        if (!validateCode(this.state.phoneCode)) {
            Toast.info("请输入正确的验证码", 2);
            return false;
        }
        if (!this.state.agree) {
            Toast.info("请先勾选同意用户协议", 2);
            return false;
        }
        this.setState({
            btnLoading: true
        });
        accountRegister({
            mail: this.state.email,
            password: this.state.password,
            confirm: this.state.surePwd,
            mobile: this.state.phone,
            smsId: this.state.smsId,
            captcha: this.state.phoneCode,
            prefix: "86"
        }).then(res => {
            this.setState({
                btnLoading: false
            });
            if (res) {
                if (res.code === 0) {
                    Toast.success("注册成功", 2, () => {
                        this.props.history.push("/user/login");
                    });
                } else {
                    Toast.fail(res.msg, 2);
                }
            }
        });
    };
    render() {
        const { getFieldProps } = this.props.form;
        const { type, btnLoading, sendCodeText } = this.state;
        return (
            <div className="register-wrapper">
                <div className="content-wrapper">
                    <div className="head-img">
                        <img src={normalURL} alt="" />
                    </div>
                    <List renderHeader={() => ""} className="my-list">
                        <Item
                            extra={
                                <InputItem
                                    {...getFieldProps("autofocus")}
                                    clear
                                    moneyKeyboardAlign="left"
                                    placeholder="请输入邮箱"
                                    value={this.state.email}
                                    onChange={email => {
                                        this.setState({
                                            email
                                        });
                                    }}
                                    ref={el => (this.autoFocusInst = el)}
                                />
                            }
                        >
                            <CustomIcon
                                icon="#iconyouxiang"
                                width="20px"
                                height="20px"
                                color="#108ee9"
                            />
                        </Item>
                        <Item
                            extra={
                                <div className="password-wrpper">
                                    <InputItem
                                        {...getFieldProps("focus")}
                                        type={this.state.passwordType}
                                        placeholder="请输入密码"
                                        name="password"
                                        value={this.state.password}
                                        onChange={password => {
                                            this.setState({
                                                password
                                            });
                                        }}
                                        ref={el => (this.inputRef = el)}
                                    />
                                    <span
                                        className="iconWrapper"
                                        onClick={() => {
                                            if (
                                                this.state.passwordType ===
                                                "password"
                                            ) {
                                                this.setState({
                                                    passwordType: "text"
                                                });
                                            } else {
                                                this.setState({
                                                    passwordType: "password"
                                                });
                                            }
                                        }}
                                    >
                                        {this.state.passwordType ===
                                        "password" ? (
                                            <CustomIcon
                                                icon="#iconyanjing"
                                                width="20px"
                                                height="20px"
                                                color="#108ee9"
                                            />
                                        ) : (
                                            <CustomIcon
                                                icon="#iconiconset0207"
                                                width="20px"
                                                height="20px"
                                                color="#108ee9"
                                            />
                                        )}
                                    </span>
                                </div>
                            }
                        >
                            <CustomIcon
                                icon="#iconmima"
                                width="20px"
                                height="20px"
                                color="#108ee9"
                            />
                        </Item>
                        <Item
                            extra={
                                <div className="password-wrpper">
                                    <InputItem
                                        {...getFieldProps("focus")}
                                        type={this.state.againPasswordType}
                                        placeholder="请再次输入密码"
                                        name="password"
                                        value={this.state.surePwd}
                                        onChange={surePwd => {
                                            this.setState({
                                                surePwd
                                            });
                                        }}
                                        ref={el => (this.inputRef = el)}
                                    />
                                    <span
                                        className="iconWrapper"
                                        onClick={() => {
                                            if (
                                                this.state.againPasswordType ===
                                                "password"
                                            ) {
                                                this.setState({
                                                    againPasswordType: "text"
                                                });
                                            } else {
                                                this.setState({
                                                    againPasswordType:
                                                        "password"
                                                });
                                            }
                                        }}
                                    >
                                        {this.state.againPasswordType ===
                                        "password" ? (
                                            <CustomIcon
                                                icon="#iconyanjing"
                                                width="20px"
                                                height="20px"
                                                color="#108ee9"
                                            />
                                        ) : (
                                            <CustomIcon
                                                icon="#iconiconset0207"
                                                width="20px"
                                                height="20px"
                                                color="#108ee9"
                                            />
                                        )}
                                    </span>
                                </div>
                            }
                        >
                            <CustomIcon
                                icon="#iconquerenmima"
                                width="23px"
                                height="23px"
                                color="#108ee9"
                            />
                        </Item>
                        <Item
                            extra={
                                <InputItem
                                    {...getFieldProps("autofocus")}
                                    // type={type}
                                    clear
                                    moneyKeyboardAlign="left"
                                    placeholder="请输入手机号"
                                    value={this.state.phone}
                                    onChange={phone => {
                                        this.setState({
                                            phone
                                        });
                                    }}
                                    ref={el => (this.autoFocusInst = el)}
                                />
                            }
                        >
                            <CustomIcon
                                icon="#iconweibiaoti-"
                                width="20px"
                                height="20px"
                                color="#108ee9"
                            />
                        </Item>
                        <Item
                            extra={
                                <div className="password-wrpper">
                                    <InputItem
                                        {...getFieldProps("focus")}
                                        // type={type}
                                        moneyKeyboardAlign="left"
                                        placeholder="请输入验证码"
                                        name="phoneCode"
                                        value={this.state.phoneCode}
                                        onChange={phoneCode => {
                                            this.setState({
                                                phoneCode
                                            });
                                        }}
                                        ref={el => (this.inputRef = el)}
                                    />
                                    <span
                                        className="sendCode"
                                        onClick={() => this.getCode()}
                                    >
                                        {sendCodeText}
                                    </span>
                                </div>
                            }
                        >
                            <CustomIcon
                                icon="#iconyanzhengma"
                                width="20px"
                                height="20px"
                                color="#108ee9"
                            />
                        </Item>
                        <Item
                            className="radio-wrapper"
                            extra={
                                <Radio
                                    className="my-radio"
                                    checked={this.state.agree}
                                    onClick={e =>
                                        this.setState({
                                            agree: !this.state.agree
                                        })
                                    }
                                >
                                    注册即同意<span className="agreement-text">《用户协议》</span>
                                </Radio>
                            }
                        ></Item>
                        <Item
                            extra={
                                <Button
                                    type="primary"
                                    disabled={btnLoading}
                                    loading={btnLoading}
                                    onClick={() => this.sureSubmit()}
                                >
                                    确定
                                </Button>
                            }
                        ></Item>
                    </List>
                </div>
            </div>
        );
    }
}

const RegisterWrapper = createForm()(Register);
export default RegisterWrapper;
