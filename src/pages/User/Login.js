import React, { Fragment } from "react";
import "./styles/login.scss";
import { Button, List, InputItem } from "antd-mobile";
import { createForm } from "rc-form";
import {
    validatePhone,
    validateCode,
    validateEmail,
    validatePassword
} from "@/utils/validate";
import appAction from "@/redux/actions/app";
import { getSmsCode, LoginCodeSubmit, LoginPwdSubmit } from "@/api/user";
import md5 from "js-md5";
import Loading from "@/components/Loading/Loading";
import { connect } from "react-redux";
import { Toast } from "antd-mobile";
import CustomIcon from "@/components/Icon/CustomIcon";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: "money",
            email: "576950395@qq.com",
            phone: "18628119371",
            sendCodeText: "获取验证码",
            phoneCode: "",
            password: "",
            canCode: true,
            isSendCode: false,
            markId: "",
            timer: "",
            btnLoading: false,
            loginType: "password",
            smsId: "",
            showPassword: false,
            inited:false,
        };
    }

    componentDidMount() {}
    changeType = () => {
        if(!this.state.inited){
            this.setState({
                inited: true,
                showPassword: false
            });
        }
    };
    getCode() {
        if (!validatePhone(this.state.phone)) {
            Toast.info("请输入正确的手机号", 2);
            return false;
        } else if (this.state.canCode) {
            this.state.canCode = false;
            var j = 60;
            Loading.show();
            getSmsCode({
                telephone: this.state.phone,
                v: this.state.markId,
                promoteId: 1
            }).then(res => {
                Loading.hide();
                if (res.code == 0) {
                    this.setState({
                        smsId: res.smsId,
                        phoneCode: res.smsCode,
                        isSendCode: true,
                        sendCodeText: j + "s"
                    });
                    var i = setInterval(() => {
                        j--;
                        if (j === 0) {
                            this.setState({
                                canCode: true,
                                sendCodeText: "重新获取"
                            });
                            clearInterval(i);
                        } else {
                            this.setState({
                                sendCodeText: j + "s"
                            });
                        }
                    }, 1000);
                } else {
                    Toast.info(res.msg, 2);
                    this.setState({
                        canCode: true
                    });
                }
            });
        }
    }

    loginSubmit() {
        const { updateLoginStatus } = this.props;
        const { loginType } = this.state;
        if (loginType !== "password" && !validatePhone(this.state.phone)) {
            Toast.info("请输入正确的电话号码", 2);
            return false;
        }
        if (loginType !== "password" && !this.state.isSendCode) {
            Toast.info("请先获取验证码", 2);
            return false;
        }
        if (loginType !== "password" && !validateCode(this.state.phoneCode)) {
            Toast.info("请输入正确的验证码", 2);
            return false;
        }
        if (loginType === "password" && !validateEmail(this.state.email)) {
            Toast.info("请输入正确的邮箱", 2);
            return false;
        }
        if (
            loginType === "password" &&
            !validatePassword(this.state.password)
        ) {
            Toast.info("请输入正确的密码", 2);
            return false;
        }
        if (loginType === "password") {
            this.setState({
                btnLoading: true
            });
            LoginPwdSubmit({
                userName: this.state.email,
                password: this.state.password
            }).then(res => {
                this.setState({
                    btnLoading: false
                });
                if (res) {
                    if (res.code === 0) {
                        const params = {
                            avatar: res.data.avatar,
                            gender: res.data.gender,
                            userName: res.data.userName,
                            accessToken: res.data.token,
                            currentAuthority: res.data.currentAuthority,
                            memberId: ""
                        };
                        updateLoginStatus(params);
                        // this.props.history.push("/");
                    } else {
                        Toast.fail(res.msg, 2);
                    }
                }
            });
        } else {
            if (this.state.isSendCode) {
                this.state.btnLoading = true;
                LoginCodeSubmit({
                    userName: this.state.phone,
                    code: this.state.phoneCode,
                    smsId: this.state.smsId
                }).then(res => {
                    this.state.btnLoading = false;
                    if (res.code === 0) {
                        const params = {
                            avatar: res.data.avatar,
                            gender: res.data.gender,
                            userName: res.data.userName,
                            accessToken: res.data.token,
                            currentAuthority: res.data.currentAuthority,
                            memberId: ""
                        };
                        updateLoginStatus(params);
                        // this.props.history.push("/");
                    } else {
                        Toast.fail(res.msg, 2);
                    }
                });
            } else {
                Toast.info("请先发送验证码", 2);
            }
        }
    }
    changeLoginType() {
        if (this.state.loginType !== "password") {
            this.setState({
                loginType: "password"
            });
            return;
        }
        this.setState({
            loginType: "sms"
        });
    }

    skipToRegister() {
        this.props.history.push({ pathname: `/user/register` });
    }
    skipToForgetpwd() {
        this.props.history.push({ pathname: `/user/forgetpwd` });
    }
    repairBottomWhite = () => {
        var _body = document.getElementsByTagName("body")[0];
        _body.style.height = _body.clientHeight + "px";
    };
    switchChange = () => {
        this.setState({
            inited: true,
            showPassword: !this.state.showPassword
        });
    };
    render() {
        const { loginType, btnLoading, type, showPassword } = this.state;
        const { getFieldProps } = this.props.form;
        return (
            <div className="login-wrapper">
                <div className="login-logo-wrapper">
                    <p className="login-title">
                        {loginType === "password" ? (
                            <span onClick={() => this.skipToRegister()}>
                                注册
                            </span>
                        ) : (
                            <span>登录</span>
                        )}
                    </p>
                </div>
                <div className="login-form-wrapper">
                    <List renderHeader={() => ""}>
                        <p className="login-form-label">
                            {loginType === "password" ? "账户" : "手机号"}
                        </p>
                        {loginType === "password" ? (
                            <InputItem
                                {...getFieldProps("autofocus")}
                                clear
                                moneyKeyboardAlign="left"
                                placeholder="请输入邮箱"
                                value={this.state.email}
                                id="userName"
                                onChange={email => {
                                    this.setState({
                                        email
                                    });
                                }}
                                ref={el => (this.autoFocusInst = el)}
                            />
                        ) : (
                            <InputItem
                                {...getFieldProps("autofocus")}
                                // type={type}
                                clear
                                moneyKeyboardAlign="left"
                                placeholder="请输入手机号"
                                value={this.state.phone}
                                onChange={phone => {
                                    this.setState({
                                        phone
                                    });
                                }}
                                onFocus={() => {
                                    this.repairBottomWhite();
                                }}
                                ref={el => (this.autoFocusInst = el)}
                            />
                        )}
                        <p className="login-form-label">
                            {loginType === "password" ? "密码" : "手机验证码"}
                        </p>
                        {loginType !== "password" && (
                            <span
                                className="sendCode"
                                onClick={() => this.getCode()}
                            >
                                {this.state.sendCodeText}
                            </span>
                        )}
                        {loginType === "password" ? (
                            <Fragment>
                                <InputItem
                                    {...getFieldProps("focus")}
                                    type={showPassword ? "text" : "password"}
                                    placeholder="请输入密码"
                                    name="password"
                                    value={this.state.password}
                                    id="password"
                                    onChange={password => {
                                        this.setState({
                                            password
                                        });
                                    }}
                                    onFocus={() => {
                                        this.repairBottomWhite();
                                    }}
                                    ref={el => (this.inputRef = el)}
                                    onClick={() => {
                                        this.changeType();
                                    }}
                                />
                                <span
                                    className="eye-iamge"
                                    onClick={() => {
                                        this.switchChange();
                                    }}
                                >
                                    {showPassword ? (
                                        <CustomIcon
                                            icon="#iconyanjing"
                                            width="22px"
                                            height="22px"
                                            color="#fff"
                                        />
                                    ) : (
                                        <CustomIcon
                                            icon="#iconiconset0207"
                                            width="25px"
                                            height="25px"
                                            color="#fff"
                                        />
                                    )}
                                </span>
                            </Fragment>
                        ) : (
                            <InputItem
                                {...getFieldProps("focus")}
                                moneyKeyboardAlign="left"
                                placeholder="请输入验证码"
                                name="phoneCode"
                                value={this.state.phoneCode}
                                onChange={phoneCode => {
                                    this.setState({
                                        phoneCode
                                    });
                                }}
                                ref={el => (this.inputRef = el)}
                            />
                        )}
                    </List>
                    <div className="toggle-wrapper">
                        {loginType === "password" ? (
                            <div>
                                <span onClick={() => this.changeLoginType()}>
                                    短信验证码登录
                                </span>
                                <span onClick={() => this.skipToForgetpwd()}>
                                    忘记密码?
                                </span>
                            </div>
                        ) : (
                            <span onClick={() => this.changeLoginType()}>
                                账号密码登录
                            </span>
                        )}
                    </div>
                    {/* <div>{this.state.btnLoading}</div> */}
                    <div className="login-button-row">
                        <Button
                            loading={btnLoading}
                            onClick={() => this.loginSubmit()}
                        >
                            登录
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { updateAppState } = state;
    return {
        // loginType: updateAppState.loginType
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        updateLoginStatus: (...params) => {
            dispatch(appAction.updateLoginStatus(...params));
        }
    };
};
const LoginWrapper = createForm()(Login);
const LoginContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginWrapper);
export default LoginContainer;
