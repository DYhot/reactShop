import React from "react";
import { List, InputItem, WhiteSpace, Button, Toast } from "antd-mobile";
import { createForm } from "rc-form";
import "../styles/setNikeName.scss";
import { updateNikeName } from "@/api/user";

class SetNikeName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nikeName: ""
        };
    }
    componentDidMount() {
        const params = this.props.match.params.nikeName;
        this.setState({
            nikeName: params || ""
        });
    }

    submit = () => {
        updateNikeName({ nikeName: this.state.nikeName }).then(res => {
            if (!res) {
                return;
            }
            if (res.code === 0) {
                this.props.history.push("/userCenter/index");
            }
            Toast.info(res.msg, 1);
        });
    };
    render() {
        const { getFieldProps } = this.props.form;
        const { nikeName } = this.state;
        return (
            <div className="setNikeName_wrapper">
                <List renderHeader={() => "请输入新昵称"}>
                    <InputItem
                        {...getFieldProps("input3", {
                            initialValue: `${nikeName}`
                        })}
                        clear
                        placeholder=""
                        onChange={value => {
                            this.setState({
                                nikeName: value
                            });
                        }}
                    />
                </List>
                <div className="button_wrapper">
                    <Button type="primary" onClick={() => this.submit()}>
                        <span>保存</span>
                    </Button>
                </div>
            </div>
        );
    }
}
const SetNikeNameContainer = createForm()(SetNikeName);
export default SetNikeNameContainer;
