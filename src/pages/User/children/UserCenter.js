import React from "react";
import { connect } from "react-redux";
import appAction from "@/redux/actions/app";
import {
    Button,
    WhiteSpace,
    WingBlank,
    List,
    InputItem,
    Toast,
    Switch,
    Picker,
    DatePicker,
    Modal,
    Icon
} from "antd-mobile";
import "../styles/userCenter.scss";
import { createForm } from "rc-form";
import { genderData } from "@/utils/dataDictionary";
import {
    getUserInfo,
    updateGender,
    updateBirthday,
    upLoadAvatar
} from "@/api/user";
import { encryptionString } from "@/utils/tool";
import { dataURLtoFile } from "@/utils/file";
import CustomIcon from "@/components/Icon/CustomIcon";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";
import UpLoadAvatar from "@/components/UpLoadAvatar/index";
const alert = Modal.alert;
class UserCenter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            headImg: "",
            nikeName: "邓杨",
            phone: "18628119371",
            gender: "",
            birthday: null,
            modal1: false
        };
    }
    componentDidMount() {
        this.getUserInfoData();
    }
    onRef = ref => {
        this.child = ref;
    };
    goPage = url => {
        const { history } = this.props;
        history.push("/userCenter/setNikeName/" + `${this.state.nikeName}`);
    };
    getUserInfoData() {
        Loading.show();
        getUserInfo().then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    userName: data.user_name,
                    headImg: data.user_head_img,
                    nikeName: data.user_nikeName,
                    phone: data.user_mobile,
                    gender: [data.user_gender],
                    birthday: new Date(data.user_birthday)
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    }
    birthdayChange = birthday => {
        Loading.show();
        updateBirthday({ birthday: birthday }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                this.setState({
                    birthday
                });
            }
            Toast.info(res.msg, 1);
        });
    };
    genderChange = gender => {
        Loading.show();
        updateGender({ gender: gender[0] }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                this.setState({
                    gender
                });
            }
            Toast.info(res.msg, 1);
        });
    };
    handleLayOut() {
        const { layOut } = this.props;
        layOut();
    }
    showModal = key => e => {
        console.log(111, e);
        e.preventDefault(); // 修复 Android 上点击穿透
        this.setState({
            [key]: true
        });
    };
    avatarChange = avatar => {
        if (!avatar) {
            return;
        }
        Loading.show();
        const file = dataURLtoFile(avatar, "avatar.png");
        var formData = new FormData();
        formData.append(`file`, file);
        formData.append(`type`, "avatar");
        upLoadAvatar(formData).then(res => {
            // Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info(res.msg, 1);
                this.getUserInfoData();
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    };
    render() {
        const { getFieldProps } = this.props.form;
        const { nikeName, headImg, gender, phone } = this.state;
        const { loginStatus } = this.props;
        return (
            <div className="userCenter-wrapper">
                <UpLoadAvatar
                    onRef={this.onRef}
                    upload={this.avatarChange}
                />
                <div className="userCenter-content">
                    <div className="form_wrapper">
                        <div className="avatar-wrapper">
                            <span
                                className="icon-wrapper"
                                onClick={() => this.child.selectImage()}
                            >
                                <CustomIcon
                                    icon="#iconxiangji"
                                    width="30px"
                                    height="30px"
                                    color="#fff"
                                />
                            </span>
                            <div className="avatar">
                                {headImg ? (
                                    <DefaultImage src={headImg} />
                                ) : gender == 1 ? (
                                    <CustomIcon
                                        icon="#iconicon-test"
                                        width="70px"
                                        height="70px"
                                        color="#fff"
                                    />
                                ) : (
                                    <CustomIcon
                                        icon="#iconicon-test1"
                                        width="70px"
                                        height="70px"
                                        color="#fff"
                                    />
                                )}
                            </div>
                            <p className="descibe">点击更换头像</p>
                        </div>
                        <List>
                            <List.Item
                                extra={encryptionString(this.state.userName)}
                                onClick={() => {}}
                            >
                                用户名
                            </List.Item>
                            <List.Item
                                extra={nikeName}
                                arrow="horizontal"
                                onClick={() => {
                                    this.goPage("/userCenter/setNikeName");
                                }}
                            >
                                昵称
                            </List.Item>
                            <Picker
                                extra="未设置"
                                data={genderData}
                                cols={1}
                                {...getFieldProps("district3")}
                                className="forss"
                                value={this.state.gender}
                                onChange={this.genderChange}
                            >
                                <List.Item arrow="horizontal">性别</List.Item>
                            </Picker>
                            <DatePicker
                                mode="date"
                                title=""
                                extra="未设置"
                                minDate={new Date(1900, 1, 1)}
                                maxDate={new Date(Date.now())}
                                value={this.state.birthday || ""}
                                onChange={birthday => {
                                    this.birthdayChange(birthday);
                                }}
                            >
                                <List.Item arrow="horizontal">生日</List.Item>
                            </DatePicker>
                            <List.Item
                                extra={encryptionString(phone)}
                                onClick={() => {}}
                            >
                                手机号
                            </List.Item>
                        </List>
                    </div>

                    <div className="button_wrapper">
                        <Button
                            type="warning"
                            onClick={() =>
                                alert("", "确定退出登录吗?", [
                                    {
                                        text: "取消",
                                        onPress: () => console.log("cancel")
                                    },
                                    {
                                        text: "确定",
                                        onPress: () => this.handleLayOut()
                                    }
                                ])
                            }
                        >
                            退出登录
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    let { updateAppState } = state;
    return {
        loginStatus: updateAppState.loginStatus
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        layOut: () => {
            dispatch(appAction.layOut());
        }
    };
};
const UserCenterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(createForm()(UserCenter));
export default UserCenterContainer;
