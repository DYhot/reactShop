/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import { getGoodsList } from "@/api/goods";
import RenderItem from "./components/RenderItem";
import PullRefresh from "@/components/PullRefresh/index";
import ToTop from "@/components/BackToTop/ToTop";

class DemoPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            noMore: false,
            empty: true,
            freshTime: "",
            total: 0,
            pageNum: 0,
            pageSize: 10,
            data: [],
            goodsListArray: [],
            BScroll: null
        };
    }

    componentDidMount() {
        this.refreshData();
    }
    componentDidUpdate() {}
    refreshData = () => {
        const { pageNum, pageSize } = this.state;
        const params = {
            current: 1,
            size: pageSize
        };
        return new Promise((resolve, reject) => {
            getGoodsList(params).then(res => {
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    const data = res.data.records;
                    const total = res.data.total;
                    const freshTime = res.data.time;
                    this.setState(
                        {
                            empty: data.length > 0 ? false : true,
                            noMore:
                                total > (1 - 1) * pageSize + pageSize
                                    ? false
                                    : true,
                            pageNum: 1,
                            total,
                            freshTime,
                            goodsListArray: data
                        },
                        () => {
                            resolve();
                        }
                    );
                } else {
                    Toast.fail(res.msg, 1);
                }
            });
        });
    };
    getMoreData = () => {
        const { pageNum, pageSize } = this.state;
        const params = {
            current: pageNum + 1,
            size: pageSize
        };
        return new Promise((resolve, reject) => {
            getGoodsList(params).then(res => {
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    const data = res.data.records;
                    const total = res.data.total;
                    const freshTime = res.data.time;
                    this.setState(
                        {
                            noMore:
                                total > (pageNum - 1) * pageSize + pageSize
                                    ? false
                                    : true,
                            pageNum:
                                total > (pageNum - 1) * pageSize + pageSize
                                    ? pageNum + 1
                                    : pageNum,
                            total,
                            freshTime,
                            goodsListArray: this.state.goodsListArray.concat(
                                data
                            )
                        },
                        () => {
                            resolve();
                        }
                    );
                } else {
                    Toast.fail(res.msg, 1);
                }
            });
        });
    };
    render() {
        const { goodsListArray, noMore, empty, freshTime } = this.state;
        return (
            <div className="demoPage-wrapper">
                <ToTop />
                <PullRefresh
                    data={goodsListArray}
                    empty={empty}
                    noMore={noMore}
                    freshTime={freshTime}
                    pullDown={this.refreshData}
                    pullUp={this.getMoreData}
                >
                    {RenderItem(goodsListArray)}
                </PullRefresh>
            </div>
        );
    }
}

export default DemoPage;
