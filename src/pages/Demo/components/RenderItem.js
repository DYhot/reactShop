import React from "react";
import IconCart from "@/components/Icon/IconCart";
import "../styles/renderItem.scss";
import { addGoodToCart } from "@/api/cart";
import DefaultImage from "@/components/DefaultImage";

const RenderItem = (goodsListArray) => {
    // addToCart = (item, e) => {
    //     e.nativeEvent.stopImmediatePropagation();
    //     e.stopPropagation();
    //     Loading.show();
    //     console.log(33, item);
    //     addGoodToCart({ id: item.id }).then(res => {
    //         Loading.hide();
    //         if (!res) {
    //             return;
    //         }
    //         if (res.code === 0) {
    //             Toast.info("成功加入购物车", 1);
    //         } else {
    //             Toast.info(res.msg, 3);
    //         }
    //     });
    // };
    // skipToDetail=(id) =>{
    //     this.props.history.push({ pathname: `/detail/${id}` });
    // };
    return (
        <div className="demo-content-wrapper">
            {goodsListArray.length > 0 ? (
                goodsListArray.map(item => (
                    <div className="list-item" key={item.id}>
                        <div
                            className="top-row"
                            onClick={() => {
                                // this.skipToDetail(item.id);
                            }}
                        >
                            <DefaultImage src={item.url}/>
                        </div>
                        <div className="middle-row">
                            <p className="title">{item.title}</p>
                            <p className="describe">{item.describe}</p>
                        </div>
                        <div className="bottom-row">
                            <span>￥5.00</span>
                            <div
                                className="iconCartWrapper"
                                onClick={e => {
                                    // this.addToCart(item, e)
                                }
                            }
                            >
                                <IconCart />
                            </div>
                        </div>
                    </div>
                ))
            ) : null}
        </div>
    );
};
export default RenderItem;