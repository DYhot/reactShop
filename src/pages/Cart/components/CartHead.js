/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "../styles/cartHead.scss";
import { NavBar, Icon } from "antd-mobile";

class CartHead extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.onRef(this);
    }
    render() {
        const {isEditing}=this.props;
        return (
            <div className="cartHead-wrapper">
                <NavBar
                    mode="light"
                    rightContent={!isEditing?[
                        <span
                            key="0"
                            style={{ marginRight: "16px" }}
                            onClick={()=>{
                                this.props.setIsEditing()
                            }}
                        >
                            编辑
                        </span>,
                        <Icon key="1" type="ellipsis" />
                    ]:[
                        <span
                            key="0"
                            style={{ marginRight: "16px" }}
                            onClick={()=>{
                                this.props.setIsEditing()
                            }}
                        >
                            完成
                        </span>,
                    ]}
                >
                    购物车
                </NavBar>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {}

export default CartHead;
