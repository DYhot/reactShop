import React from "react";
import "./styles/index.scss";
import {
    Button,
    WhiteSpace,
    WingBlank,
    Checkbox,
    List,
    Stepper,
    Modal,
    Toast
} from "antd-mobile";
import {
    getCartList,
    deleteCartGoodsBatchById,
    updateCartGoodsParams
} from "@/api/cart";
import { getGoodsList } from "@/api/goods";
import { formateAmount } from "@/utils/tool";
import CartHead from "./components/CartHead";
import CustomIcon from "@/components/Icon/CustomIcon";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";
import IconCart from "@/components/Icon/IconCart";
const CheckboxItem = Checkbox.CheckboxItem;
const alert = Modal.alert;
class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inited: false,
            isEditing: false,
            defaultChecked: false,
            refreshing: false, // 是否显示刷新状态
            showLoading: false, // 是否显示loading
            current: 1,
            size: 10,
            total: 0,
            totalCount: 0,
            totalAmount: 0,
            cartListData: [],
            goodsListArray: []
        };
    }
    componentDidMount() {
        this.getCartListData();
    }
    onRef = ref => {
        this.child = ref;
    };
    getGoodsListData() {
        Loading.show();
        const { current, size } = this.state;
        const params = {
            current,
            size: 1000
        };
        getGoodsList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const total = res.data.total;
                this.setState({
                    total,
                    current: this.state.current + 1,
                    goodsListArray: this.state.goodsListArray.concat(data)
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    skipToDetail(id) {
        this.props.history.push({ pathname: `/detail/${id}` });
    }
    renderContent = () => {
        const { goodsListArray } = this.state;
        return (
            <div className="content-wrapper">
                {goodsListArray.length > 0 ? (
                    goodsListArray.map(item => (
                        <div className="list-item" key={item.id}>
                            <div
                                className="top-row"
                                onClick={() => {
                                    this.skipToDetail(item.id);
                                }}
                            >
                                <DefaultImage src={item.url} />
                            </div>
                            <div className="middle-row">
                                <p className="title">{item.title}</p>
                                <p className="describe">{item.describe}</p>
                            </div>
                            <div className="bottom-row">
                                <div className="text-wrapper">
                                    <span>￥{item.price}</span>
                                    {item.price != item.original_price && (
                                        <del className="amount_del">
                                            ￥{item.original_price}
                                        </del>
                                    )}
                                </div>
                            </div>
                        </div>
                    ))
                ) : (
                    <div className="nodata-wrapper">
                        {/* <img src={noDataImg} alt=""/> */}
                        <p>暂无数据</p>
                    </div>
                )}
                {/* </InfiniteScroll> */}
            </div>
        );
    };
    getCartListData() {
        const {
            current,
            size,
            refreshing,
            cartListData
            // dataSource
        } = this.state;
        const params = { current, size };
        Loading.show();
        getCartList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                if (res.data.records) {
                    if (res.data.records.length < 1) {
                        this.getGoodsListData();
                    }
                    // const ListArray = [...cartListData, ...res.data.records];
                    const ListArray = [...res.data.records];
                    let totalCount = 0;
                    let totalAmount = 0;
                    let isDefaultChecked = true;
                    ListArray.forEach(item => {
                        if (item.isSelected == 1) {
                            totalCount += item.count;
                            totalAmount += item.count * item.price;
                        } else {
                            isDefaultChecked = false;
                        }
                    });
                    this.setState({
                        inited: true,
                        defaultChecked:
                            ListArray.length > 0 ? isDefaultChecked : false,
                        total: res.data.total,
                        showLoading: false,
                        // current: current + 1,
                        // isShowContent: true,
                        refreshing: false,
                        // isLoading: false,
                        cartListData: ListArray,
                        totalCount,
                        totalAmount: formateAmount(totalAmount)
                        // dataSource: dataSource.cloneWithRows(ListArray) // 数据源dataSource
                    });
                    // this.setLoading(false);
                } else {
                    // this.setState({
                    //     isShowContent: false
                    // });
                }
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    changeCartGoodsParams = ({ ids, isSelected = null, count = null }) => {
        const params = {
            ids
        };
        count && (params.count = count);
        isSelected !== null && (params.isSelected = isSelected);
        return new Promise((resolve, reject) => {
            updateCartGoodsParams(params).then(res => {
                // Toast.hide();
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    // Toast.info('修改成功', 1);
                } else {
                    Toast.info(res.msg, 1);
                }
                resolve(res);
            });
        });
    };
    onChangeAll() {
        const { cartListData, defaultChecked } = this.state;
        let totalCount = 0;
        let totalAmount = 0;
        const idArr = cartListData.map(item => {
            return item.id;
        });
        if (idArr.length === 0) {
            return;
        }
        if (!defaultChecked) {
            this.changeCartGoodsParams({ ids: idArr, isSelected: 1 });
            cartListData.forEach(item => {
                item.isSelected = 1;
                totalCount += item.count;
                totalAmount += item.count * item.price;
            });
        } else {
            this.changeCartGoodsParams({ ids: idArr, isSelected: 0 });
            cartListData.forEach(item => {
                item.isSelected = 0;
            });
        }
        this.setState({
            defaultChecked: !defaultChecked,
            cartListData,
            totalAmount: formateAmount(totalAmount),
            totalCount
        });
    }
    onChange = id => {
        // console.log(id);
        const { cartListData, totalCount, totalAmount } = this.state;
        let calculateCount = 0;
        let calculateAmount = 0;
        cartListData.forEach((item, index) => {
            if (item.id === id) {
                if (item.isSelected == 1) {
                    this.changeCartGoodsParams({ ids: [id], isSelected: 0 });
                    item.isSelected = 0;
                    calculateCount = totalCount - item.count;
                    calculateAmount = formateAmount(
                        totalAmount - item.count * item.price
                    );
                } else {
                    this.changeCartGoodsParams({ ids: [id], isSelected: 1 });
                    item.isSelected = 1;
                    calculateCount = totalCount + item.count;
                    calculateAmount = formateAmount(
                        Number(totalAmount) + item.count * item.price
                    );
                }
            }
        });
        this.setState({
            cartListData,
            totalCount: calculateCount,
            totalAmount: calculateAmount
        });
    };

    stepperOnChange = (val, row) => {
        if (isNaN(Number(val))) {
            return;
        }
        const { cartListData, totalCount, totalAmount } = this.state;
        let calculateCount = 0;
        let calculateAmount = 0;
        this.changeCartGoodsParams({ ids: [row.id], count: val }).then(data => {
            if (data.code == 0) {
                cartListData.forEach((item, index) => {
                    if (item.id === row.id) {
                        if (item.isSelected == 1) {
                            calculateCount = totalCount + val - item.count;
                            calculateAmount = formateAmount(
                                Number(totalAmount) +
                                    (val - item.count) * item.price
                            );
                        }
                        item.count = val;
                    }
                });
                this.setState({
                    cartListData,
                    totalCount: calculateCount,
                    totalAmount: calculateAmount
                });
            }
        });
    };
    setIsEditing = () => {
        const { isEditing } = this.state;
        this.setState({
            isEditing: !isEditing
        });
    };
    deleteCart = () => {
        const { cartListData } = this.state;
        const itemArr = cartListData.filter(item => {
            return item.isSelected == 1;
        });
        const idArr = itemArr.map(item => {
            return item.id;
        });
        deleteCartGoodsBatchById({ ids: idArr }).then(res => {
            // Toast.hide();
            console.log("res", res);
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("删除成功", 1);
                this.getCartListData();
                this.setState({
                    isEditing: false
                });
            } else {
                Toast.info(res.msg, 3);
            }
        });
    };
    goToSettlement = () => {
        const { cartListData, totalCount, totalAmount } = this.state;
        if (cartListData.length < 1) {
            Toast.info("请先添加商品到购物车", 3);
            return;
        }
        let isSelectedArr = [];
        cartListData.forEach(item => {
            console.log(item);
            item.isSelected == 1 && isSelectedArr.push(item);
        });

        if (isSelectedArr.length < 1) {
            Toast.info("请先勾选需要结算的商品", 3);
            return;
        }
        this.props.history.push({
            pathname: "/settlement",
            state: { cartListData: isSelectedArr, totalCount, totalAmount }
        });
    };
    render() {
        const {
            defaultChecked,
            cartListData,
            totalCount,
            totalAmount,
            isEditing,
            inited
        } = this.state;
        const { history } = this.props;
        return (
            <div className="cart_wrapper">
                <CartHead
                    isEditing={isEditing}
                    onRef={this.onRef}
                    setIsEditing={() => {
                        this.setIsEditing();
                    }}
                />
                <div
                    className={`list_content ${
                        cartListData.length === 0 ? "empty_list_content" : ""
                    }`}
                >
                    <List>
                        {cartListData.length > 0 ? (
                            cartListData.map((i, index) => (
                                <CheckboxItem
                                    key={i.id}
                                    checked={i.isSelected == 0 ? false : true}
                                    onChange={() => this.onChange(i.id)}
                                >
                                    <div className="list_item">
                                        <div
                                            className="list_item_img"
                                            onClick={() => {
                                                this.props.history.push({
                                                    pathname: `/detail/${i.good_id}`
                                                });
                                            }}
                                        >
                                            <DefaultImage src={i.url} />
                                        </div>
                                        <div className="list_item_content">
                                            <div className="item_title_group">
                                                <p className="item_title">
                                                    {i.title}
                                                </p>
                                                <p className="item_describe">
                                                    {i.describe}
                                                </p>
                                            </div>
                                            <div className="item_price_count">
                                                <p className="item_price">
                                                    <span>￥</span>
                                                    <span>{i.price}</span>
                                                </p>
                                                <Stepper
                                                    style={{
                                                        width: "120px"
                                                        // minWidth: "100px"
                                                    }}
                                                    showNumber
                                                    max={10000}
                                                    min={1}
                                                    step={1}
                                                    value={i.count}
                                                    onChange={e => {
                                                        this.stepperOnChange(
                                                            e,
                                                            i
                                                        );
                                                    }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </CheckboxItem>
                            ))
                        ) : inited ? (
                            <div className="empty_wrapper">
                                <span className="icon_wrapper">
                                    <CustomIcon
                                        icon="#icongouwuche2"
                                        width="100px"
                                        height="100px"
                                        color="#00D15B"
                                    />
                                </span>
                                <p>购物车都是空的，快来填满它吧</p>
                            </div>
                        ) : null}
                        {cartListData.length == 0 && inited && (
                            <div className="recommended-wrapper">
                                <div className="head-wrapper">
                                    <p className="head-title">
                                        <span className="icon_wrapper">
                                            <CustomIcon
                                                icon="#iconicon_fuben"
                                                width="20px"
                                                height="20px"
                                                color="#F60001"
                                            />
                                        </span>
                                        <span>为你推荐</span>
                                    </p>
                                </div>
                                <div className="goods-list-wrapper">
                                    {this.renderContent()}
                                </div>
                            </div>
                        )}
                    </List>
                </div>
                <div className="bottom_statistical shadow-only-bottom">
                    <div className="select-all-wrapper">
                        <CheckboxItem
                            style={{ fontSize: "14px" }}
                            key="disabled"
                            data-seed="logId"
                            checked={defaultChecked}
                            multipleLine
                            onChange={() => this.onChangeAll()}
                        >
                            全选
                        </CheckboxItem>
                    </div>
                    {!isEditing ? (
                        <div className="right_box">
                            <div className="amount_text">
                                <span>合计:</span>
                                <span className="amount_total">
                                    <span>￥</span>
                                    <span>{totalAmount}</span>
                                </span>
                            </div>
                            <div className="settlement_button">
                                <Button
                                    type="warning"
                                    onClick={() => this.goToSettlement()}
                                >
                                    去结算({totalCount})
                                </Button>
                                <WhiteSpace />
                            </div>
                        </div>
                    ) : (
                        <div>
                            <Button
                                size="small"
                                inline
                                onClick={() =>
                                    alert(
                                        "",
                                        "确定将已选商品从购物车中删除吗?",
                                        [
                                            {
                                                text: "取消",
                                                onPress: () =>
                                                    this.setState({
                                                        isEditing: false
                                                    })
                                            },
                                            {
                                                text: "确定",
                                                onPress: () => this.deleteCart()
                                            }
                                        ]
                                    )
                                }
                            >
                                删除
                            </Button>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
export default Cart;
