/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import {
    List,
    TextareaItem,
    Radio,
    Button,
    ImagePicker,
    InputItem,
    WhiteSpace,
    Toast
} from "antd-mobile";
import Loading from "@/components/Loading/Loading";
import { validatePhone } from "@/utils/validate";
import { zipImage } from "@/utils/file";
import { createFeedBack } from "@/api/feedBack";
import { uploadFeedBackFile} from "@/api/file";
import { createForm } from "rc-form";
const RadioItem = Radio.RadioItem;

class FeedBack extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: [
                {
                    value: 0,
                    label: "功能异常",
                    placeholder:
                        "请详细描述出现的异常问题，上传相关页面截图能方便我快速解决问题哦"
                },
                {
                    value: 1,
                    label: "优化建议",
                    placeholder: "还想要什么功能？快来反馈吧"
                },
                {
                    value: 2,
                    label: "其他反馈",
                    placeholder: "说你想说，畅所欲言的反馈吧"
                }
            ],
            value: null,
            files: [],
            initialValue: "选择问题类型，并填写3字以上的反馈，我们将尽快解决",
            disabled: false,
            multiple: true,
            disabled:false,
        };
    }

    componentDidMount() {}
    onChange = key => {
        const { data, value } = this.state;
        let initialValue;
        data.forEach((item, index) => {
            if (item.value == key) {
                initialValue = item.placeholder;
            }
        });
        this.setState({
            value: key,
            initialValue
        });
    };
    textareaOnFocus = () => {
        this.setState({
            initialValue: ""
        });
    };
    textareaOnBlur = () => {
        const { getFieldsValue } = this.props.form;
        const { data, value } = this.state;
        const formValue = getFieldsValue(["content"]);
        const { content } = formValue;
        if (content == "") {
            if (value != null) {
                let initialValue;
                data.forEach((item, index) => {
                    if (item.value == value) {
                        initialValue = item.placeholder;
                    }
                    this.setState({
                        initialValue
                    });
                });
            } else {
                this.setState({
                    initialValue:
                        "选择问题类型，并填写3字以上的反馈，我们将尽快解决"
                });
            }
        }
        if (content.length < 4) {
            Toast.info("反馈内容应在3字以上", 1);
            return;
        }
    };

    fileOnChange = (files, type, index) => {
        console.log(111, files, type, index);
        this.setState({
            files
        });
    };
    onSubmit = () => {
        const { getFieldsValue } = this.props.form;
        const { files, value } = this.state;
        const formValue = getFieldsValue(["content", "phone"]);
        const { content, phone } = formValue;
        if (value === null) {
            Toast.info("请选择问题反馈类型", 1);
            return;
        }
        if (content.length < 4) {
            Toast.info("反馈内容应在3字以上", 1);
            return;
        }
        if (!validatePhone(phone.replace(/\s+/g, ""))) {
            Toast.info("请输入11位手机号", 1);
            return;
        }
        const query = {
            type:value,
            content,
            phone: phone.replace(/\s+/g, ""),
        };
        let fileList = [...files];
        let fileArray = [];
        fileList.forEach((item, index) => {
            fileArray.push(item.file);
        });
        var formData = new FormData();
        this.setState({
            disabled:true
        })
        Loading.show();
        zipImage(fileArray).then(data => {
            data.forEach((item, index) => {
                formData.append(`file`, item);
            });
            formData.append(`type`, "feedBack");
            uploadFeedBackFile(formData).then(res => {
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    query.file_ids=res.data.records.ids;
                    createFeedBack(query).then(res => {
                        Loading.hide();
                        this.setState({
                            disabled:false
                        })
                        if (!res) {
                            return;
                        }
                        if (res.code === 0) {
                            Toast.info(res.msg, 1, () => {
                                this.props.history.go("-1");
                            });
                        } else {
                            Toast.info(res.msg, 1);
                        }
                    });
                } else {
                    Loading.hide();
                    this.setState({
                        disabled:false
                    })
                    Toast.info(res.msg, 1);
                }
            });
        })
    };
    render() {
        const { getFieldProps } = this.props.form;
        const { data, value, files, initialValue, disabled } = this.state;
        return (
            <div className="feedBack-wrapper">
                <List
                    renderHeader={() => (
                        <div className="head-wrapper">
                            <p className="title">问题反馈类型</p>
                            <div className="radio-wrapper">
                                {data.map(i => (
                                    <RadioItem
                                        key={i.value}
                                        checked={value === i.value}
                                        onChange={() => this.onChange(i.value)}
                                    >
                                        {i.label}
                                    </RadioItem>
                                ))}
                            </div>
                        </div>
                    )}
                >
                    <TextareaItem
                        {...getFieldProps("content", {
                            initialValue: initialValue
                        })}
                        rows={5}
                        count={500}
                        onFocus={this.textareaOnFocus}
                        onBlur={this.textareaOnBlur}
                    />
                    <div>
                        <ImagePicker
                            length="6"
                            files={files}
                            onChange={this.fileOnChange}
                            onImageClick={(index, fs) => console.log(index, fs)}
                            selectable={files.length < 3}
                            multiple={this.state.multiple}
                        />
                    </div>
                    <WhiteSpace />
                    <InputItem
                        {...getFieldProps("phone", {
                            initialValue: ""
                        })}
                        type="phone"
                        placeholder="请输入手机号"
                    >
                        手机号码
                    </InputItem>
                    <WhiteSpace />
                    <div className="button_wrapper">
                        <Button
                            disabled={disabled}
                            type="primary"
                            onClick={() => this.onSubmit()}
                        >
                            <span>提交</span>
                        </Button>
                    </div>
                </List>
            </div>
        );
    }
}

const FeedBackWrapper = createForm()(FeedBack);
export default FeedBackWrapper;
