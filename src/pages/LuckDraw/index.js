/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./index.scss";

class LuckDraw extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            list: [
                {
                    id: 1,
                    type: 1,
                    name: "十元代金卷",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 2,
                    type: 2,
                    name: "谢谢参与",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 3,
                    type: 1,
                    name: "笔记本电脑",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 4,
                    type: 1,
                    name: "20元优惠券",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 5,
                    type: 1,
                    name: "500积分",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 6,
                    type: 1,
                    name: "100元现金",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 7,
                    type: 1,
                    name: "健身卡一张",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                },
                {
                    id: 8,
                    type: 2,
                    name: "谢谢参与",
                    image_url: require("../../assets/images/luckDraw/djj.jpg")
                }
            ],
            circleList: [],
            colorCircleFirst: "#F12416",
            colorCircleSecond: "#FFFFFF",
            colorAwardDefault: "#F5F0FC",
            colorAwardSelect: "#ffe400",
            btntop: 0, //按钮的样式
            time: "", //定时器
            indexSelect: 0, //奖品下标
            lottert: 0, //中奖下标
            prize: 0, //是否中奖
            prize_name: "", //奖品名字
            men: false
        };
    }

    componentDidMount() {
        this.init();
    }
    init = () => {
        const { btntop } = this.state;
        var list = this.state.list;
        var left = 9;
        var top = 9;

        for (var i = 0; i < 8; i++) {
            if (i == 0) {
                left = 9;
                top = 9;
            } else if (i < 3 && i != 0) {
                top = top;
                left = left + 98 + 4;
            } else if (i >= 3 && i < 5) {
                left = left;
                top = top + 79 + 4;
            } else if (i >= 5 && i < 7) {
                left = left - 98 - 4;
                top = top;
            } else if (i >= 7 && i < 8) {
                left = left;
                top = top - 79 - 4;
            }
            list[i].top = top;
            list[i].left = left;
        }
        this.setState(
            {
                list: list
            },
            () => {
                var cleft = 4; //e
                var ctop = 4; //a
                var dian = [];
                for (var j = 0; j < 24; j++) {
                    if (j == 0) {
                        cleft = 4;
                        ctop = 4;
                    } else if (j < 6) {
                        ctop = 2;
                        cleft += 55;
                    } else if (j == 6) {
                        ctop = 2;
                        cleft = 330;
                    } else if (j < 12) {
                        ctop += 46;
                        cleft = 331.5;
                    } else if (j == 12) {
                        ctop = 272.5;
                        cleft = 330;
                    } else if (j < 18) {
                        ctop = 275;
                        cleft -= 55;
                    } else if (j == 18) {
                        ctop = 273;
                        cleft = 5;
                    } else {
                        if (!(j < 24)) return;
                        (ctop -= 46), (cleft = 2.5);
                    }
                    dian.push({
                        topCircle: ctop,
                        leftCircle: cleft
                    });
                }

                this.setState({
                    circleList: dian
                });
                setInterval(() => {
                    const {colorCircleFirst}=this.state;
                    if (colorCircleFirst == "#FFFFFF") {
                        this.setState({
                            colorCircleFirst: "#F12416",
                            colorCircleSecond: "#FFFFFF"
                        });
                    } else {
                        this.setState({
                            colorCircleFirst: "#FFFFFF",
                            colorCircleSecond: "#F12416",
                            btntop: 0
                        });
                    }
                }, 900);
                this.time = setInterval(() => {
                    if (btntop == 0) {
                        this.setState({
                            btntop: -3
                        });
                    } else {
                        this.setState({
                            btntop: 0
                        });
                    }
                }, 900);
            }
        );
    };
    startBtn = e => {
        clearInterval(this.time);
        this.setState(
            {
                men: true,
                btntop: 0,
                lottert: 0
            },
            () => {
                var i = this.state.indexSelect;
                var list = this.state.list;
                var time = null;
                var s = 0;

                time = setInterval(() => {
                    i++;
                    i %= 8;
                    s += 30;
                    this.state.indexSelect = i;
                    if (this.state.lottert > 0 && i + 1 == this.state.lottert) {
                        clearInterval(time);
                        this.time = setInterval(() => {
                            const { btntop } = this.state;
                            if (btntop == 0) {
                                this.setState({
                                    btntop : -3
                                })
                            } else {
                                this.setState({
                                    btntop : 0
                                })
                            }
                        }, 900);
                        if (list[i].type == 2) {
                            this.setState({
                                prize : 2
                            })
                        } else {
                            this.state.prize_name = list[i].name;
                            this.setState({
                                prize : 1
                            })
                        }
                    }
                }, 200 + s);

                setTimeout(() => {
                    this.setState({
                        lottert : this.randomNum(1, 8),
                    })
                }, 2e3);
            }
        );
    };
    randomNum = (minNum, maxNum) => {
        const length = minNum && maxNum ? 2 : 1;
        switch (length) {
            case 1:
                return parseInt(Math.random() * minNum + 1, 10);
                break;
            case 2:
                return parseInt(
                    Math.random() * (maxNum - minNum + 1) + minNum,
                    10
                );
                break;
            default:
                return 0;
                break;
        }
    };
    conTinue = () => {
        this.setState({
            men : false,
            prize : 0,
        })
    };
    render() {
        const {
            list,
            indexSelect,
            colorAwardSelect,
            colorAwardDefault,
            btntop,
            circleList,
            prize,
            colorCircleFirst,
            colorCircleSecond
        } = this.state;
        return (
            <div className="luckDraw">
                <div
                    className="pond-head"
                    onClick={() => {
                        this.init();
                    }}
                >
                    <img
                        src={require("../../assets/images/luckDraw/pond-head.png")}
                        alt=""
                    />
                </div>
                <div className="container-out">
                    <div className="container-in">
                        {list.map((box, index) => (
                            <div
                                className="content-out"
                                key={index}
                                style={{
                                    left: box.left + "px",
                                    top: box.top + "px",
                                    backgroundColor:
                                        index == indexSelect
                                            ? colorAwardSelect
                                            : colorAwardDefault
                                }}
                            >
                                <img
                                    className="pond-name-img"
                                    src={box.image_url}
                                    alt=""
                                />
                                <div className="pond-name">{box.name}</div>
                            </div>
                        ))}
                    </div>
                    <div
                        className="start-btn"
                        onClick={e => {
                            this.startBtn(e);
                        }}
                    >
                        <img
                            src={require("../../assets/images/luckDraw/pond-button.png")}
                            style={{ height: "80px", width: "100%" }}
                            alt=""
                        />
                        <img
                            src={require("../../assets/images/luckDraw/pond-cj.png")}
                            style={{ top: btntop + "px" }}
                            className="center-style"
                            alt=""
                        />
                    </div>
                    {circleList.map((item, index) => (
                        <div
                            className="circle"
                            key={index}
                            style={{
                                top: item.topCircle + "px",
                                left: item.leftCircle + "px",
                                backgroundColor:
                                    index % 2 == 0
                                        ? colorCircleFirst
                                        : colorCircleSecond
                            }}
                        ></div>
                    ))}
                </div>
                {prize == 1 ? (
                    <div className="prize">
                        <div className="prize-box">
                            <img
                                className="prize-img"
                                src={require("../../assets/images/luckDraw/pond-success.png")}
                                alt=""
                            />
                            <div className="prize-msg">
                                <div>恭喜</div>
                                <div>抽中{prize_name}</div>
                                <div
                                    className="continue"
                                    onClick={() => {
                                        this.conTinue();
                                    }}
                                >
                                    继续抽奖
                                </div>
                            </div>
                        </div>
                    </div>
                ) : null}
                {prize == 2 ? (
                    <div className="prize">
                        <div className="prize-box">
                            <img
                                className="prize-img2"
                                src={require("../../assets/images/luckDraw/pond-empty.png")}
                                alt=""
                            />
                            <div className="prize-msg">
                                <div>谢谢参与</div>
                                <div>再换个姿势抽奖</div>
                                <div
                                    className="continue"
                                    onClick={() => {
                                        this.conTinue();
                                    }}
                                >
                                    继续抽奖
                                </div>
                            </div>
                        </div>
                    </div>
                ) : null}
                <div className="men" v-show="men"></div>
            </div>
        );
    }
}

export default LuckDraw;
