/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import { Tabs, WhiteSpace, Toast, Modal, Button } from "antd-mobile";
import noDataImg from "@/assets/images/common/noData.jpeg";
import {
    getUserOrderList,
    deleteOrderBatchById,
    confirmGoods
} from "@/api/order";
import { addGoodToCart } from "@/api/cart";
import CustomIcon from "@/components/Icon/CustomIcon";
import Loading from "@/components/Loading/Loading";
import DefaultImage from "@/components/DefaultImage/index";
const alert = Modal.alert;

class MyOrder extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            current: 1,
            size: 1000,
            total: 0,
            currentTabKey: null,
            orderListArray: [],
            tabs: [
                { title: "全部", key: 0 },
                { title: "待支付", key: 1 },
                { title: "待收货", key: 2 },
                { title: "待评价", key: 3 }
            ],
            data: [
                {
                    id: 0,
                    label: "微信支付",
                    icon: "#iconweixinzhifu",
                    selected: true
                },
                {
                    id: 1,
                    label: "支付宝支付",
                    icon: "#iconzhifubaozhifu",
                    selected: false
                }
            ]
        };
    }

    componentDidMount() {
        const params = this.props.location.state;
        if (params) {
            this.setState(
                {
                    currentTabKey: params.key
                },
                () => {
                    this.getOrderListData();
                }
            );
        } else {
            this.getOrderListData();
        }
    }
    getOrderListData() {
        Loading.show();
        const { current, size, currentTabKey, tabs } = this.state;
        const params = {
            current,
            size,
            type: currentTabKey ? currentTabKey : tabs[0].key
        };
        getUserOrderList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    total: res.data.total,
                    orderListArray: data
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    }
    deleteOrder = item => {
        const alertInstance = alert("", "确定删除该订单?", [
            {
                text: "取消",
                onPress: () => {},
                style: "default"
            },
            {
                text: "确定",
                onPress: () => {
                    alertInstance.close();
                    Loading.show();
                    const params = {
                        ids: [item.id]
                    };
                    deleteOrderBatchById(params).then(res => {
                        Loading.hide();
                        if (!res) {
                            return;
                        }
                        if (res.code === 0) {
                            Toast.success(res.msg, 1);
                            this.getOrderListData();
                        } else {
                            Toast.fail(res.msg, 1);
                        }
                    });
                }
            }
        ]);
    };
    addToCart = (item, e) => {
        e.nativeEvent.stopImmediatePropagation();
        e.stopPropagation();
        Loading.show();
        const idArr = item.goodArray.map(item => {
            return item.good_id;
        });
        addGoodToCart({ id: idArr.join(",") }).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                Toast.info("成功加入购物车", 1, () => {
                    this.props.history.push({
                        pathname: "/cart"
                    });
                });
            } else {
                Toast.info(res.msg, 1);
            }
        });
    };
    tabsChange = val => {
        const { currentTabKey } = this.state;
        if (val.key == currentTabKey) {
            return;
        }
        this.setState(
            {
                currentTabKey: val.key
            },
            () => {
                this.getOrderListData(val.key);
            }
        );
    };
    goToPaymentDetail = item => {
        const { payType, amount, order_num, create_time } = item;
        const { data } = this.state;
        let payTypeData;
        data.forEach((item, index) => {
            if (item.id == payType) {
                payTypeData = item;
            }
        });
        this.props.history.push({
            pathname: "/paymentDetail",
            state: {
                payType: payTypeData,
                totalAmount: amount,
                order_num,
                time: create_time
            }
        });
    };
    ConfirmGoods = (item, e) => {
        e.nativeEvent.stopImmediatePropagation();
        e.stopPropagation();
        const alertInstance = alert("", "确定已收到该订单的商品?", [
            {
                text: "取消",
                onPress: () => {},
                style: "default"
            },
            {
                text: "确定",
                onPress: () => {
                    alertInstance.close();
                    Loading.show();
                    confirmGoods({ id: item.id }).then(res => {
                        Loading.hide();
                        if (!res) {
                            return;
                        }
                        if (res.code === 0) {
                            Toast.info("确认收货成功", 1);
                            this.getOrderListData();
                        } else {
                            Toast.info(res.msg, 1);
                        }
                    });
                }
            }
        ]);
    };
    goToGoodList = item => {
        this.props.history.push({
            pathname: "/goodsList",
            state: { goodsListData: item.goodArray, totalCount: item.count }
        });
    };
    renderContent = tab => {
        const { orderListArray } = this.state;
        return (
            <div className="content-wrapper">
                {orderListArray.length > 0 ? (
                    orderListArray.map(item => (
                        <div className="list-item" key={item.id}>
                            <div className="head-row">
                                <div className="left">
                                    <span>订单号:{item.order_num}</span>
                                </div>
                                <div className="right">
                                    <span></span>
                                    <span
                                        className="iconWrapper"
                                        onClick={() => {
                                            this.deleteOrder(item);
                                        }}
                                    >
                                        <CustomIcon
                                            icon="#iconshanchu"
                                            width="15px"
                                            height="15px"
                                            color="#333"
                                        />
                                    </span>
                                </div>
                            </div>
                            <div className="moddile-row">
                                {item.goodArray.length > 1 ? (
                                    <Fragment>
                                        <div
                                            className="multiple-img-wrapper"
                                            onClick={() =>
                                                this.goToGoodList(item)
                                            }
                                        >
                                            <div>
                                                {item.goodArray.map(value => (
                                                    <DefaultImage
                                                        src={value.url}
                                                        key={value.url}
                                                    />
                                                ))}
                                            </div>
                                        </div>
                                        <div className="right-text">
                                            <p>{item.amount}</p>
                                            <p>共{item.count}件</p>
                                        </div>
                                    </Fragment>
                                ) : item.goodArray.length === 1 ? (
                                    <div
                                        className="single-good-wrapper"
                                        onClick={() => this.goToGoodList(item)}
                                    >
                                        <div className="left">
                                            <DefaultImage
                                                src={item.goodArray[0].url}
                                            />
                                        </div>
                                        <div className="right">
                                            <span>
                                                <span>
                                                    {item.goodArray[0].title}
                                                </span>
                                                <span
                                                    style={{
                                                        color: "#666"
                                                    }}
                                                >
                                                    {item.goodArray[0].describe}
                                                </span>
                                            </span>
                                            <span>
                                                ￥{item.goodArray[0].price}
                                            </span>
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                            <div className="bottom-row">
                                {/* order_status 0:订单可以继续支付 1:订单已关闭 2:订单未签收 3:订单未评价 4:订单已评价 */}
                                {item.order_status == 0 ? (
                                    <Button
                                        type="ghost"
                                        size="small"
                                        inline
                                        style={{ marginRight: "4px" }}
                                        className="am-button-borderfix"
                                        onClick={e => {
                                            this.goToPaymentDetail(item, e);
                                        }}
                                    >
                                        去支付
                                    </Button>
                                ) : item.order_status == 1 ? (
                                    <span className="order-close">
                                        订单已取消
                                    </span>
                                ) : item.order_status == 2 ? (
                                    <Fragment>
                                        <Button
                                            type="ghost"
                                            size="small"
                                            inline
                                            style={{ marginRight: "4px" }}
                                            className="am-button-borderfix"
                                            onClick={e => {
                                                this.props.history.push({
                                                    pathname: `/logistics`,
                                                    search: `?order_num=${item.order_num}`
                                                });
                                            }}
                                        >
                                            查看物流
                                        </Button>
                                        <Button
                                            type="ghost"
                                            size="small"
                                            inline
                                            style={{ marginRight: "4px" }}
                                            className="am-button-borderfix"
                                            onClick={e => {
                                                this.ConfirmGoods(item, e);
                                            }}
                                        >
                                            确认收货
                                        </Button>
                                    </Fragment>
                                ) : item.order_status == 3 ? (
                                    <Fragment>
                                        <Button
                                            type="ghost"
                                            size="small"
                                            inline
                                            style={{ marginRight: "4px" }}
                                            className="am-button-borderfix"
                                            onClick={e => {
                                                this.props.history.push({
                                                    pathname: `/evaluation`,
                                                    search: `?order_num=${item.order_num}`
                                                });
                                            }}
                                        >
                                            去评价
                                        </Button>
                                        <Button
                                            type="ghost"
                                            size="small"
                                            inline
                                            style={{ marginRight: "4px" }}
                                            className="am-button-borderfix border-red"
                                            onClick={e => {
                                                this.addToCart(item, e);
                                            }}
                                        >
                                            再次购买
                                        </Button>
                                    </Fragment>
                                ) : item.order_status == 4 ? (
                                    <Fragment>
                                        <Button
                                            type="ghost"
                                            size="small"
                                            inline
                                            style={{ marginRight: "4px" }}
                                            className="am-button-borderfix"
                                            onClick={e => {
                                                // this.addToCart(item, e);
                                            }}
                                        >
                                            查看评价
                                        </Button>
                                        <Button
                                            type="ghost"
                                            size="small"
                                            inline
                                            style={{ marginRight: "4px" }}
                                            className="am-button-borderfix border-red"
                                            onClick={e => {
                                                this.addToCart(item, e);
                                            }}
                                        >
                                            再次购买
                                        </Button>
                                    </Fragment>
                                ) : null}
                            </div>
                            <div className="time-list">
                                <span>下单时间:{item.create_time}</span>
                            </div>
                        </div>
                    ))
                ) : (
                    <div className="nodata-wrapper">
                        <DefaultImage src={noDataImg} />
                        <p>暂时还没有相关订单</p>
                    </div>
                )}
            </div>
        );
    };
    render() {
        const { tabs, currentTabKey } = this.state;
        return (
            <div className="myOrder-wrapper">
                <WhiteSpace />
                <Tabs
                    tabs={tabs}
                    swipeable={false}
                    // initialPage={currentTabKey}
                    distanceToChangeTab={1}
                    page={currentTabKey}
                    tabBarBackgroundColor="##F4F4F4"
                    tabBarActiveTextColor="#00c35d"
                    tabBarInactiveTextColor="#6B6B6B"
                    tabBarUnderlineStyle={{
                        border: "1px #00c35d solid",
                        width: "15%",
                        marginLeft: "5%"
                    }}
                    tabBarTextStyle={{ fontSize: "14px", fontWeight: "bold" }}
                    renderTabBar={props => (
                        <Tabs.DefaultTabBar {...props} page={4} />
                    )}
                    onChange={val => {
                        this.tabsChange(val);
                    }}
                >
                    {this.renderContent}
                </Tabs>
            </div>
        );
    }
}

export default MyOrder;
