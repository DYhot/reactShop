/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import { getUserCouponList } from "@/api/coupon";
import Loading from "@/components/Loading/Loading";
import { List, Toast, Button } from "antd-mobile";
const Item = List.Item;
class MyCoupon extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            couponListArray: [],
            total: 0,
            current:1,
            size:100,
        };
    }

    componentDidMount() {
        this.getUserCouponListData();
    }
    getUserCouponListData() {
        Loading.show();
        const {current,size}=this.state;
        const params={
            current,
            size,
        }
        getUserCouponList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const total = res.data.total;
                this.setState({
                    total,
                    couponListArray: data ? data : []
                });
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    render() {
        const { couponListArray } = this.state;
        return (
            <div className="myCoupon-wrapper">
                <List className="list-content">
                    {couponListArray.length > 0
                        ? couponListArray.map(item => (
                              <div className="coupon-item" key={item.id}>
                                  <div className="coupon-item-left">
                                      <p className="right-amount">{item.discountAmount}元</p>
                                      <p>无使用门槛</p>
                                      <p>最多优惠{item.discountAmount}元</p>
                                  </div>
                                  <div className="coupon-item-right">
                                      <p className="right-title">通用优惠券</p>
                                      <p>
                                          <span>有效期:</span>
                                          <span>{item.endTime}</span>
                                      </p>
                                  </div>
                                  <div className="coupon-item-bottom">
                                    <p>使用说明:仅适用于{item.categoryName}类商品</p>
                                    <p>劵编号:{item.serialNumber}</p>
                                </div>
                              </div>
                          ))
                        : null}
                </List>
                <div className="bottom-wrapper">
                    <Button
                        style={{ zIndex: 100 }}
                        onClick={() => this.props.history.push("/myCoupon/couponRecords")}
                    >
                        优惠券使用记录
                    </Button>
                    <Button
                        style={{ zIndex: 100 }}
                        onClick={() => this.props.history.push("/greenCard")}
                    >
                        领取更多优惠券
                    </Button>
                </div>
            </div>
        );
    }
}

export default MyCoupon;
