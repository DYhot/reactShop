/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "../styles/couponRecords.scss";
import { List, Button, WhiteSpace, Tabs, Badge } from "antd-mobile";
import noCoupon from "@/assets/images/order/coupon-empty.png";
import usedImg from "@/assets/images/coupon/used.png";
import overTimeImg from "@/assets/images/coupon/overTime.png";
import { getUserCouponUsedList } from "@/api/coupon";
import Loading from "@/components/Loading/Loading";
import { Item } from "antd-mobile/lib/tab-bar";
import DefaultImage from "@/components/DefaultImage/index";

const tabs = [
    { title: <Badge text={"3"}>已使用</Badge>, key: "t1" },
    { title: <Badge text={"0"}>已过期</Badge>, key: "t2" }
];
class CouponRecords extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            total: 0,
            current: 1,
            size: 100,
            currentTab: tabs[0],
            canUseData: [
                {
                    id: 0,
                    condition: "无使用门槛",
                    type: "通用优惠券",
                    amount: 1.5,
                    period_validity: "2021.02.20",
                    selected: false
                },
                {
                    id: 1,
                    condition: "无使用门槛",
                    type: "通用优惠券",
                    amount: 2.5,
                    period_validity: "2021.03.01",
                    selected: false
                }
            ]
        };
    }

    componentDidMount() {
        this.getCouponUsedListData();
    }
    getCouponUsedListData() {
        Loading.show();
        const { current, size } = this.state;
        const params = {
            current,
            size
        };
        getUserCouponUsedList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                const total = res.data.total;
                this.setState({
                    total,
                    canUseData: data ? data : []
                });
            } else {
                Toast.fail(res.msg, 3);
            }
        });
    }
    renderContent = tab => {
        const { canUseData, currentTab } = this.state;
        return (
            <div
                style={{
                    height: "100%"
                }}
            >
                <List className="coupon-wrapper">
                    {tab.key === "t1" ? (
                        canUseData.length > 0 ? (
                            canUseData.map(item => (
                                <div className="coupon-item">
                                    <div className="coupon-item-left">
                                        <p className="right-amount">
                                            {item.discountAmount}元
                                        </p>
                                        <p>无使用门槛</p>
                                        <p>最多优惠{item.discountAmount}元</p>
                                    </div>
                                    <div className="coupon-item-right">
                                        {item.categoryName ? (
                                            <p className="right-title">
                                                指定商品可用
                                            </p>
                                        ) : null}
                                        <p>
                                            <span>有效期:</span>
                                            <span>{item.endTime}</span>
                                        </p>
                                    </div>
                                    <DefaultImage src={usedImg} />
                                    <div className="coupon-item-bottom">
                                        <p>
                                            使用说明:仅适用于{item.categoryName}
                                            类商品
                                        </p>
                                        <p>劵编号:{item.serialNumber}</p>
                                    </div>
                                </div>
                            ))
                        ) : (
                            <div className="noCoupon">
                                <DefaultImage src={noCoupon}/>
                                <p>暂无已使用优惠券</p>
                            </div>
                        )
                    ) : (
                        <div className="noCoupon">
                            <DefaultImage src={noCoupon}/>
                            <p>暂无已过期优惠券</p>
                        </div>
                    )}
                </List>
            </div>
        );
    };
    render() {
        return (
            <div className="couponRecords-wrapper">
                <List className="popup-list">
                    <div style={{ height: "100%" }}>
                        <WhiteSpace />
                        <Tabs
                            tabs={tabs}
                            swipeable={false}
                            initialPage={"t1"}
                            onChange={val => {
                                this.setState({
                                    currentTab: val
                                });
                            }}
                            renderTabBar={props => (
                                <Tabs.DefaultTabBar {...props} page={2} />
                            )}
                        >
                            {this.renderContent}
                        </Tabs>
                    </div>
                </List>
            </div>
        );
    }
}

export default CouponRecords;
