/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import { Result } from "antd-mobile";
import CustomIcon from "@/components/Icon/CustomIcon";

const payIcon = (payType) => (
    <CustomIcon
        icon={payType?payType.icon:''}
        width="60px"
        height="60px"
        color="#00D15B"
    />
);

const ResultList = props => {
    const params = props.location.state;
    if(params){
        var { payType, totalAmount } = params;
    }else{
        props.history.go(-1);
    }

    return (
        <div className="result-example">
            <div className="sub-title"></div>
            <Result
                img={payIcon(payType)}
                title="支付成功"
                message={
                    <div>
                        {totalAmount} <del>1098元</del>
                    </div>
                }
            />
        </div>
    );
};

export default ResultList;
