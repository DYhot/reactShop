/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./index.scss";

class ExamplePage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: []
        };
    }

    componentDidMount() {}

    render() {
        return <div className="demo-wrapper"></div>;
    }
}

function mapDispatchToProps(dispatch) {
    // return dispatch(setTitle("Home"));
}

// export default connect(null, mapDispatchToProps)(Home);
export default ExamplePage;
