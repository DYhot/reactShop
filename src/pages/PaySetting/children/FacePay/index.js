/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "../../styles/facePay.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import { List, Button, Modal ,Toast} from "antd-mobile";
import Loading from "@/components/Loading/Loading";

const Item = List.Item;
const Brief = Item.Brief;
const alert = Modal.alert;

class FacePay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
    }

    componentDidMount() {}
    openSubmit = () => {
        const { isOpen } = this.state;
        Loading.show();
        setTimeout(()=>{
            this.setState(
                {
                    isOpen: !isOpen
                },
                () => {
                    Loading.hide();
                    Toast.success('开通成功', 1);
                }
            );
        },500)
    };
    submit = () => {
        const { isOpen } = this.state;
        const alertInstance = alert(
            "确定要关闭面容支付吗?",
            "关闭后每次支付都需要输入密码",
            [
                {
                    text: "取消",
                    onPress: () => {},
                    style: "default"
                },
                {
                    text: "确定",
                    onPress: () => {
                        alertInstance.close();
                        Loading.show();
                        setTimeout(()=>{
                            this.setState(
                                {
                                    isOpen: !isOpen
                                },
                                () => {
                                    Loading.hide();
                                    Toast.success('关闭成功', 1);
                                }
                            );
                        },500)
                        // const params = {
                        //     payId: payType.id,
                        //     totalAmount,
                        //     order_num
                        // };
                        // this.setState({
                        //     btnLoading: true,
                        //     showFacePay: true,
                        // });
                        // Loading.show();
                        // payOrder(params).then(res => {
                        //     this.setState({
                        //         btnLoading: false
                        //     });
                        //     Loading.hide();
                        //     if (!res) {
                        //         return;
                        //     }
                        //     if (res.code === 0) {
                        //         this.props.history.push({
                        //             pathname: "/result",
                        //             state: { payType, totalAmount }
                        //         });
                        //     } else if ([-3, -4].includes(res.code)) {
                        //         Toast.fail(res.msg, 1, () => {
                        //             this.props.history.push({
                        //                 pathname: "/myOrder",
                        //                 state: { key: 2 }
                        //             });
                        //         });
                        //     } else {
                        //         Toast.fail(res.msg, 1);
                        //     }
                        // });
                    }
                }
            ]
        );
    };
    render() {
        const { isOpen } = this.state;
        return (
            <div className="facePay-wrapper">
                {!isOpen ? (
                    <div className="unOpen-list">
                        <div className="icon-wrapper">
                            <CustomIcon
                                icon="#iconmianrongda"
                                width="80px"
                                height="80px"
                                color="#0398FF"
                            />
                        </div>
                        <div className="title">
                            开通面容支付，让付款更安全便捷
                        </div>
                        <div className="describe">
                            开启后，可通过验证系统面容快速完成付款
                        </div>
                        <div className="button_wrapper">
                            <Button
                                type="primary"
                                onClick={() => this.openSubmit()}
                            >
                                <span>立即开通</span>
                            </Button>
                        </div>
                    </div>
                ) : (
                    <div className="opened-list">
                        <div className="icon-wrapper">
                            <CustomIcon
                                icon="#iconmianrongda"
                                width="80px"
                                height="80px"
                                color="#0398FF"
                            />
                        </div>
                        <div className="describe">
                            可通过验证系统面容快速完成付款
                        </div>
                        <div className="button_wrapper">
                            <Button onClick={() => this.submit()}>
                                <span>关闭面容支付</span>
                            </Button>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default FacePay;
