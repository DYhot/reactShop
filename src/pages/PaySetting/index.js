/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import CustomIcon from "@/components/Icon/CustomIcon";
import { List } from "antd-mobile";

const Item = List.Item;
const Brief = Item.Brief;
class PaySetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {}

    render() {
        return (
            <div className="paySetting-wrapper">
                <List className="paySetting-list">
                    <Item extra="面容" arrow="horizontal" onClick={() => {
                        this.props.history.push("/paySetting/facePay")
                    }}>
                        生物支付
                    </Item>
                </List>
            </div>
        );
    }
}

export default PaySetting;
