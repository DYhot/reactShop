/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./index.scss";

class AboutUs extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {};
    }

    componentDidMount() {}

    render() {
        return (
            <div className="aboutUs-wrapper">
                <div className="head">
                    <img src={process.env.PUBLIC_URL + "logo192.png"} alt="" />
                    <p>叮咚买菜</p>
                    <p>1.0.0</p>
                </div>
                <div className="content">
                    <p className="title">1.0.0版本更新说明</p>
                    <p className="subtitle">
                        【新增】账户与安全，支付设置，幸运抽奖
                    </p>
                    <p className="subtitle">
                        【优化】部分bug解决及整体交互体验升级
                    </p>
                    <p className="describe">
                        我们正在学习的过程中，希望通过这个项目对自己有所提高
                    </p>
                </div>
            </div>
        );
    }
}

export default AboutUs;
