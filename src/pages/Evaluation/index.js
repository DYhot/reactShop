/**
 * Created by DengYang Li on 04/05/2019.
 */
import React, { Fragment } from "react";
import "./styles/index.scss";
import {
    List,
    TextareaItem,
    Radio,
    Button,
    ImagePicker,
    InputItem,
    WhiteSpace,
    Toast
} from "antd-mobile";
import StarRatings from "react-star-ratings";
import Loading from "@/components/Loading/Loading";
import { validatePhone } from "@/utils/validate";
import { createEvaluation } from "@/api/evaluation";
import { uploadEvaluationFile } from "@/api/file";
import { getUserOrderList } from "@/api/order";
import { zipImage } from "@/utils/file";
import url from "url";
import { createForm } from "rc-form";
const RadioItem = Radio.RadioItem;
const Item = List.Item;

class Evaluation extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: [
                {
                    id: 0,
                    rating: 0,
                    label: "商品符合度"
                },
                {
                    id: 1,
                    rating: 0,
                    label: "店家服务态度"
                },
                {
                    id: 2,
                    rating: 0,
                    label: "快递配送速度"
                },
                {
                    id: 3,
                    rating: 0,
                    label: "快递员服务"
                },
                {
                    id: 4,
                    rating: 0,
                    label: "快递包装"
                }
            ],
            files: [],
            initialValue: "写出您的感受,可以帮助更多小伙伴哦",
            disabled: false,
            multiple: true,
            order_num: null,
            orderListArray: null,
            goodRating: 0
        };
    }

    componentDidMount() {
        const query = url.parse(this.props.location.search, true).query;
        this.setState(
            {
                order_num: query.order_num
            },
            () => {
                this.getOrderData();
            }
        );
    }
    getOrderData = () => {
        const { order_num } = this.state;
        Loading.show();
        const params = {
            current: 1,
            size: 1,
            type: 3,
            order_num
        };
        getUserOrderList(params).then(res => {
            Loading.hide();
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const data = res.data.records;
                this.setState({
                    total: res.data.total,
                    orderListArray: data.length > 0 ? data[0] : null
                });
            } else {
                Toast.fail(res.msg, 1);
            }
        });
    };
    textareaOnFocus = () => {
        this.setState({
            initialValue: ""
        });
    };
    textareaOnBlur = () => {
        const { getFieldsValue } = this.props.form;
        const { data, value } = this.state;
        const formValue = getFieldsValue(["content"]);
        const { content } = formValue;
        if (content == "") {
            this.setState({
                initialValue:
                    "选择问题类型，并填写3字以上的反馈，我们将尽快解决"
            });
        }
        if (content.length < 4) {
            Toast.info("评价内容应在3字以上", 1);
            return;
        }
    };

    fileOnChange = (files, type, index) => {
        // console.log(111, files, type, index);
        this.setState({
            files
        });
    };
    onSubmit = () => {
        const { getFieldsValue } = this.props.form;
        const {
            files,
            data,
            goodRating,
            initialValue,
            orderListArray,
            order_num
        } = this.state;
        const formValue = getFieldsValue(["content"]);
        const { content } = formValue;
        if (goodRating == 0) {
            Toast.info("请对商品进行评价", 1);
            return;
        }
        if (content.length < 4 || content == initialValue) {
            Toast.info("反馈内容应在3字以上", 1);
            return;
        }
        for (var i = 0; i < data.length; i++) {
            let item = data[i];
            if (item.rating == 0) {
                switch (item.id) {
                    case 1:
                        Toast.info("请对商品符合度进行评价", 1);
                        return;
                        break;
                    case 2:
                        Toast.info("请对店家服务态度进行评价", 1);
                        return;
                        break;
                    case 3:
                        Toast.info("请对快递配送速度进行评价", 1);
                        return;
                        break;
                    case 4:
                        Toast.info("请对快递员服务进行评价", 1);
                        return;
                        break;
                    case 5:
                        Toast.info("请对快递包装进行评价", 1);
                        return;
                        break;
                    default:
                }
            }
        }
        const query = {
            content,
            good_star: goodRating,
            good_conform: data[0].rating,
            service_attitude: data[1].rating,
            courier_speed: data[2].rating,
            courier_service: data[3].rating,
            courier_packaging: data[4].rating,
            cart_ids: orderListArray.goods_id,
            order_num
        };
        let fileList = [...files];
        let fileArray = [];
        fileList.forEach((item, index) => {
            fileArray.push(item.file);
        });
        var formData = new FormData();
        this.setState({
            disabled: true
        });
        Loading.show();
        zipImage(fileArray).then(data => {
            data.forEach((item, index) => {
                formData.append(`file`, item);
            });
            formData.append(`type`, "evaluation");
            
            uploadEvaluationFile(formData).then(res => {
                if (!res) {
                    return;
                }
                if (res.code === 0) {
                    query.file_ids = res.data.records.ids;
                    createEvaluation(query).then(res => {
                        Loading.hide();
                        this.setState({
                            disabled: false
                        });
                        if (!res) {
                            return;
                        }
                        if (res.code === 0) {
                            Toast.info(res.msg, 1, () => {
                                this.props.history.go("-1");
                            });
                        } else {
                            Toast.info(res.msg, 1);
                        }
                    });
                } else {
                    Toast.info(res.msg, 1);
                }
            });
        });
        console.log(222, formData);
    };
    changeRating = (newRating, name, item) => {
        const { data } = this.state;
        data.forEach((value, key) => {
            if (value.id == item.id) {
                data[key].rating = newRating;
                this.setState({
                    data
                });
            }
        });
    };
    goodChangeRating = (newRating, name) => {
        this.setState({
            goodRating: newRating
        });
    };
    createStarDescibe = rating => {
        switch (rating) {
            case 1:
                return <span className="star-descibe">非常差</span>;
                break;
            case 2:
                return <span className="star-descibe">差</span>;
                break;
            case 3:
                return <span className="star-descibe">一般</span>;
                break;
            case 4:
                return <span className="star-descibe">好</span>;
                break;
            case 5:
                return <span className="star-descibe">非常好</span>;
                break;
            default:
                return null;
        }
    };
    goToGoodList = () => {
        const { orderListArray } = this.state;
        this.props.history.push({
            pathname: "/goodsList",
            state: {
                goodsListData: orderListArray.goodArray,
                totalCount: orderListArray.count
            }
        });
    };
    render() {
        const { getFieldProps } = this.props.form;
        const {
            data,
            value,
            files,
            initialValue,
            disabled,
            orderListArray,
            goodRating
        } = this.state;
        return (
            <div className="evaluation-wrapper">
                <List
                    renderHeader={() => {
                        return orderListArray &&
                            orderListArray.goodArray.length == 1 ? (
                            <div className="head-wrapper">
                                <div
                                    className="left"
                                    onClick={() => this.goToGoodList()}
                                >
                                    <img
                                        src={orderListArray.goodArray[0].url}
                                        alt=""
                                    />
                                </div>
                                <div className="right">
                                    <div>商品评价</div>
                                    <div>
                                        <StarRatings
                                            rating={goodRating}
                                            starRatedColor="#FF2B00"
                                            changeRating={this.goodChangeRating}
                                            numberOfStars={5}
                                            name="rating"
                                            starDimension="22px"
                                            starSpacing="5px"
                                        />
                                        {this.createStarDescibe(goodRating)}
                                    </div>
                                </div>
                            </div>
                        ) : orderListArray &&
                          orderListArray.goodArray.length > 1 ? (
                            <div className="head-goods-wrapper">
                                <div
                                    className="top"
                                    onClick={() => this.goToGoodList()}
                                >
                                    <div className="image-list-wrapper">
                                        {orderListArray.goodArray.map(item => (
                                            <img src={item.url} alt="" />
                                        ))}
                                    </div>
                                </div>
                                <div className="bottom">
                                    <div className="label">商品评价</div>
                                    <div>
                                        <StarRatings
                                            rating={goodRating}
                                            starRatedColor="#FF2B00"
                                            changeRating={this.goodChangeRating}
                                            numberOfStars={5}
                                            name="rating"
                                            starDimension="22px"
                                            starSpacing="5px"
                                        />
                                        {this.createStarDescibe(goodRating)}
                                    </div>
                                </div>
                            </div>
                        ) : null;
                    }}
                >
                    <TextareaItem
                        {...getFieldProps("content", {
                            initialValue: initialValue
                        })}
                        rows={5}
                        count={500}
                        onFocus={this.textareaOnFocus}
                        onBlur={this.textareaOnBlur}
                    />
                    <div>
                        <ImagePicker
                            length="6"
                            files={files}
                            onChange={this.fileOnChange}
                            onImageClick={(index, fs) => console.log(index, fs)}
                            selectable={files.length < 6}
                            multiple={this.state.multiple}
                        />
                    </div>
                    <WhiteSpace />
                    <div className="star-wrapper">
                        <List
                            renderHeader={() => (
                                <div>
                                    <span>物流服务评价</span>
                                    <span>满意请给5星哦</span>
                                </div>
                            )}
                            className="my-list"
                        >
                            {data.map(value => (
                                <Item
                                    key={value.id}
                                    extra={
                                        <div>
                                            <StarRatings
                                                rating={value.rating}
                                                starRatedColor="#FF2B00"
                                                changeRating={(
                                                    newRating,
                                                    name
                                                ) =>
                                                    this.changeRating(
                                                        newRating,
                                                        name,
                                                        value
                                                    )
                                                }
                                                numberOfStars={5}
                                                name="rating"
                                                starDimension="22px"
                                                starSpacing="5px"
                                            />
                                            {this.createStarDescibe(
                                                value.rating
                                            )}
                                        </div>
                                    }
                                >
                                    {value.label}
                                </Item>
                            ))}
                        </List>
                    </div>
                    <WhiteSpace />
                    <div className="button_wrapper">
                        <Button
                            disabled={disabled}
                            type="primary"
                            onClick={() => this.onSubmit()}
                        >
                            <span>提交</span>
                        </Button>
                    </div>
                </List>
            </div>
        );
    }
}

const EvaluationWrapper = createForm()(Evaluation);
export default EvaluationWrapper;
