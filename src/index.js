import React, { Fragment } from "react";
import "./index.css";
import "@/assets/css/normalize.css";
// import "@/assets/styles/transition.scss";
import * as serviceWorker from "./serviceWorker";
import "lib-flexible";

import { render } from "react-dom"; // 引入render方法
import { Provider } from "react-redux"; // 利用Provider可以使我们的 store 能为下面的组件所用
// import { browserHistory } from "react-router"; // Browser history 是由 React Router 创建浏览器应用推荐的 history
import { ConnectedRouter, syncHistoryWithStore } from "react-router-redux"; // 利用react-router-redux提供的syncHistoryWithStore我们可以结合store同步导航事件
import { BrowserRouter, Route } from "react-router-dom";
import finalCreateStore from "@/redux/store/configureStore"; //引入store配置
import DevTools from "./pages/DevTools/DevTools"; // 引入Redux调试工具DevTools
import reducer from "./redux/reducers"; // 引入reducers集合
import AppContainer from "@/router/index";
// import AppContainer from "@/router/transitionRouter";
// import AppContainer from "@/router/generalRouter";
// const createHistory = require('history').createBrowserHistory;
import { store, persistor } from "./redux/store/persistStore";
import { PersistGate } from "redux-persist/lib/integration/react";
// 创建一个增强版的history来结合store同步导航事件
// const history = syncHistoryWithStore(createHistory, store)

render(
    <Provider store={store}>
        {/* <ConnectedRouter history={history}> */}
        {/* <Fragment> */}
        {/* <BrowserRouter basename='/'> */}
        {/* <BrowserRouter>
                <Route path={`/`} component={AppContainer}></Route>
            </BrowserRouter> */}
        <PersistGate loading={null} persistor={persistor}>
            <AppContainer/>
            {/* <DevTools/> */}
        </PersistGate>
        {/* </ConnectedRouter> */}
        {/* </Fragment> */}
    </Provider>,
    document.getElementById("root")
);
// ReactDOM.render(<Routers />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
