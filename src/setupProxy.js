const proxy = require("http-proxy-middleware");

module.exports = function(app) {
    app.use(
        proxy("/base", {
            // target: "https://api.videorungo.com",
            // target: "http://api.douban.com", //豆瓣api测试环境
            target: "http://127.0.0.1:7001/", //egg本地环境
            // target: "http://120.79.235.171:7001/", //egg生产环境
            changeOrigin: true,
            pathRewrite: {
                "^/base": "/"
            }
        })
    );
};
