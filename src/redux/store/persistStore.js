// redux持久化存储配置
import reducers from "@/redux/reducers"; 
import {persistStore, persistCombineReducers} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import finalCreateStore from "@/redux/store/configureStore"; //引入store配置

const persistConfig = {
    key: 'root',
    storage: storage,
    stateReconciler: autoMergeLevel2 // 查看 'Merge Process' 部分的具体情况
};

const myPersistReducer = persistCombineReducers(persistConfig, reducers);
export const store = finalCreateStore(myPersistReducer);
window.g_store=store;
export const persistor = persistStore(store);