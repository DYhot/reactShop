/**
 * Created by Daguang Li on 11/27/2017.
 */

export const constants = {
    INVALID_TIME: 60 * 60 * 1000,
    localKey: { // 本地存储Key
        userToken: 'USER_AUTHORIZATION'
    }
};
// action常量
export const INCREASE = 'INCREASE'
export const DECREASE = 'DECREASE'
export const LOADDATA = 'LOADDATA'
export const GETSUCCESS = 'GETSUCCESS'
export const REFRESHDATA = 'REFRESHDATA'
export const CHECKLOGIN = 'CHECKLOGIN'
export const CHANGETABBARSTATE = 'CHANGETABBARSTATE'
export const CHANGEHEADERTABBARSTATE = 'CHANGEHEADERTABBARSTATE'

// 全局app的action常量
export const SHOWTOAST = 'SHOWTOAST'
export const UPDATELOGINSTATUS = 'UPDATELOGINSTATUS'
export const UPDATETABBAR = 'UPDATETABBAR'
export const UPDATENAVBAR = 'UPDATENAVBAR'
export const CLEARLOGINSTATUS = 'CLEARLOGINSTATUS'