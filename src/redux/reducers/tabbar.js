// reducers/tabbar.js
import { CHANGETABBARSTATE, CHANGEHEADERTABBARSTATE } from "@/redux/constants"; // 引入action类型常量名

// 初始化state数据
const initialState = {
    tabbarState: {
        barStyle: [true, false, false, false]
    },
    headerTabBarState: true
};

// 通过dispatch action进入
export default function updateTabState(state = initialState, action) {
    // 根据不同的action type进行state的更新
    switch (action.type) {
        case CHANGETABBARSTATE:
            return Object.assign({}, state, {
                tabbarState: { barStyle: action.barStyle }
            });
            break;
        case CHANGEHEADERTABBARSTATE:
            return Object.assign({}, state, {
                headerTabBarState: action.currentState
            });
            break;
        default:
            return state;
    }
}
