// reducers/count.js
import {
    INCREASE,
    DECREASE,
    GETSUCCESS,
    REFRESHDATA,
    CHECKLOGIN,
    CHANGETABBARSTATE
} from "@/redux/constants"; // 引入action类型常量名

// 初始化state数据
const initialState = {
    number: 1,
    loginState: {
        username: "",
        islogin: false
    },
    tabbarState: {
        barStyle: [true, false, false, false]
    }
};

// 通过dispatch action进入
export default function updateLoginState(state = initialState, action) {
    // 根据不同的action type进行state的更新
    switch (action.type) {
        case CHECKLOGIN:
            return Object.assign({}, state, {
                loginState: {
                    username: action.username,
                    islogin: action.islogin
                }
            });
            break;
        case CHANGETABBARSTATE:
            return Object.assign({}, state, { tabbarState: action.barStyle });
            break;
        case DECREASE:
            return Object.assign({}, state, {
                number: state.number - action.amount
            });
            break;
        case GETSUCCESS:
            return Object.assign({}, state, { data: action.json });
        case REFRESHDATA:
            return Object.assign({}, state, { data: [] });
        default:
            return state;
    }
}
