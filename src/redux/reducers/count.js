// reducers/count.js
import {
    INCREASE,
    DECREASE,
    GETSUCCESS,
    REFRESHDATA
} from "@/redux/constants"; // 引入action类型常量名

// 初始化state数据
const initialState = {
    tabbarState: [true, false, false, false]
};

// 通过dispatch action进入
export default function update(state = initialState, action) {
    // 根据不同的action type进行state的更新
    switch (action.type) {
        case INCREASE:
            return Object.assign({}, state, {
                number: state.number + action.amount
            });
            break;
        case DECREASE:
            return Object.assign({}, state, {
                number: state.number - action.amount
            });
            break;
        case GETSUCCESS:
            return Object.assign({}, state, { data: action.json });
        case REFRESHDATA:
            return Object.assign({}, state, { data: [] });
        default:
            return state;
    }
}
