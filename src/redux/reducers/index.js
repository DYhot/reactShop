// reducers/index.js
import { combineReducers } from 'redux' // 利用combineReducers 合并reducers
import { routerReducer } from 'react-router-redux' // 将routerReducer一起合并管理
import updateAppState from './app'
import updateLoginState from './login'
import updateTabState from './tabbar'

// export default combineReducers({
//     updateAppState,
//     updateLoginState,
//     updateTabState,
//     routing: routerReducer
// })
export default {
    updateAppState,
    updateLoginState,
    updateTabState,
    routing: routerReducer
}