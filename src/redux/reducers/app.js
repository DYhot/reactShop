// reducers/app.js
import {
    SHOWTOAST,
    UPDATELOGINSTATUS,
    CLEARLOGINSTATUS,
    UPDATETABBAR,
    UPDATENAVBAR
} from "@/redux/constants"; // 引入全局action类型常量名
import { getStorage, setStorage } from "@/utils/storage";

// 初始化state数据
const initialState = {
    // loginType: "password",
    toastStatus: {
        isShow: false,
        text: ""
    },
    loginStatus: {
        avatar:null,
        gender:null,
        userName: "",
        accessToken: "",
        currentAuthority: "",
        memberId: ""
    },
    tabBarStatus: {
        activeIndex: "0"
    },
    navBarStatus: {
        isShow: true
    }
};

// 通过dispatch action进入
export default function updateAppState(state = initialState, action) {
    // 根据不同的action type进行state的更新
    switch (action.type) {
        case SHOWTOAST:
            return Object.assign({}, state, { toastStatus: action.value });
            break;
        case UPDATELOGINSTATUS:
            setStorage("accessToken", action.value.accessToken);
            return Object.assign({}, state, { loginStatus: action.value });
            break;
        case CLEARLOGINSTATUS:
            return Object.assign({}, state, { loginStatus: action.value });
            break;
        case UPDATETABBAR:
            return Object.assign({}, state, { tabBarStatus: action.value });
            break;
        case UPDATENAVBAR:
            return Object.assign({}, state, { navBarStatus: action.value });
            break;
        default:
            return state;
    }
}
