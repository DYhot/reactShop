import { CHANGETABBARSTATE, CHANGEHEADERTABBARSTATE } from "@/redux/constants"; // 引入action类型名常量

// 这里的方法返回一个action对象
export const changetabbarstate = m => {
    console.log("bbbbb", m);
    return {
        type: CHANGETABBARSTATE,
        barStyle: m
    };
};
// 这里的方法返回一个action对象
export const changeheadertabbarstate = m => {
    console.log("bbbbb", m);
    return {
        type: CHANGEHEADERTABBARSTATE,
        currentState: m
    };
};
// 底部导航栏切换
export const changeTabbar = (params, barCurrentState) => {
    return dispatch => {
        console.log("xxxxxxxxxx", params, barCurrentState);
        // let barStyle=[true, false, false, false]
        let newState = barCurrentState.map((item, index) => {
            return index === params ? true : false;
        });
        dispatch(changetabbarstate(newState));
    };
};
// 头部导航栏切换
export const changeHeaderTabBar = barCurrentState => {
    return dispatch => {
        console.log("xxxxxxxxxx", barCurrentState);
        dispatch(changeheadertabbarstate(barCurrentState));
    };
};
