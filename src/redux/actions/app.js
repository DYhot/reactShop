import {
    SHOWTOAST,
    UPDATELOGINSTATUS,
    UPDATETABBAR,
    UPDATENAVBAR,
    CLEARLOGINSTATUS
} from "@/redux/constants"; // 引入action类型名常量
import { layOutLogin } from "@/api/user";
import { Toast } from "antd-mobile";
import { parse, stringify } from "qs";
import { replace } from "react-router-redux";
export function getPageQuery() {
    return parse(window.location.href.split("?")[1]);
}
// 这里的方法返回一个action对象
const showToast = params => {
    return {
        type: SHOWTOAST,
        value: params
    };
};

// 更新登录状态
// const updateLoginStatus = value => async (dispatch) =>{
const updateLoginStatus = value => {
    const urlParams = new URL(window.location.href);
    const params = getPageQuery();
    let { redirect } = params;
    if (redirect) {
        const redirectUrlParams = new URL(redirect);
        if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
                redirect = redirect.substr(redirect.indexOf("#") + 1);
            }
        } else {
            window.location.href = redirect;
            return;
        }
    }
    // console.log(111,replace,value,dispatch)
    // console.log(222,replace,value)
    window.g_router.replace(redirect || "/");
    // replace(redirect || "/");
    // dispatch(replace(redirect || "/"));
    return {
        type: UPDATELOGINSTATUS,
        value: value
    };
};

const updateTabBar = params => {
    return {
        type: UPDATETABBAR,
        value: params
    };
};
const updateNavBar = params => {
    return {
        type: UPDATENAVBAR,
        value: params
    };
};
// 清除登录状态
const clearLoginStatus = params => {
    console.log("clearLoginStatus被调用");
    return {
        type: CLEARLOGINSTATUS,
        value: params
    };
};
// 退出登录
const layOut = () => {
    return (dispatch, getState) => {
        const preState = getState();
        const redirectLogin = () => {
            const { redirect } = getPageQuery(); // redirect
            if (window.location.pathname !== "/user/login" && !redirect) {
                window.g_router.replace({
                    pathname: "/user/login",
                    search: stringify({
                        redirect: window.location.href
                    })
                });
            }
        };
        if (!preState.updateAppState.loginStatus.accessToken) {
            redirectLogin();
        }
        layOutLogin().then(res => {
            if (!res) {
                return;
            }
            if (res.code === 0) {
                const params = {
                    userName: "",
                    accessToken: "",
                    currentAuthority: "",
                    memberId: ""
                };
                dispatch(clearLoginStatus(params));
                redirectLogin();
            } else {
                Toast.info(res.msg, 1);
            }
        });
    };
};

const appAction = {
    showToast: (...params) => showToast(...params),
    updateLoginStatus: (...params) => updateLoginStatus(...params),
    layOut: () => layOut(),
    updateTabBar: (...params) => updateTabBar(...params),
    updateNavBar: (...params) => updateNavBar(...params)
};

export default appAction;
