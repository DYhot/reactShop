import { CHECKLOGIN, CHANGETABBARSTATE } from "@/redux/constants"; // 引入action类型名常量
import "whatwg-fetch"; // 可以引入fetch来进行Ajax
import request from "@/services/request";

// 这里的方法返回一个action对象
export const checklogin = (m, n) => {
    return {
        type: CHECKLOGIN,
        username: m,
        islogin: n
    };
};
export const changetabbarstate = m => {
    return {
        type: CHANGETABBARSTATE,
        barStyle: m
    };
};
// dispatch (changetabbarstate)
export const loginSubmit = (m, n) => {
    return dispatch => {
        console.log("xxxxxxxxxx", m, n);
        let url = "/data/user.json";
        request.get(url).then(response => {
            if (response[0].username === m && response[0].password === n) {
                console.log("登录成功");
                localStorage.setItem(
                    "loginTime",
                    JSON.stringify({ timestamp: Date.parse(new Date()) })
                );
                dispatch(checklogin(m, true));
            } else {
                console.log("用户名或密码错误");
            }
        });
    };
};
// function fetchPosts() {
//     return dispatch => {
//         return fetch('data.json')
//             .then((res) => { console.log(res.status); return res.json() })
//             .then((data) => {
//                 dispatch(getSuccess(data))
//             })
//             .catch((e) => { console.log(e.message) })
//         }
// }

// // 这里的方法返回一个函数进行异步操作
// export function fetchPostsIfNeeded() {
//
//     // 注意这个函数也接收了 getState() 方法
//     // 它让你选择接下来 dispatch 什么
//     return (dispatch, getState) => {
//         return dispatch(fetchPosts())
//     }
// }
