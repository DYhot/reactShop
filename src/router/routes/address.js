 const addressRoutes = {
    path: "/address",
    name: "DefaultLayOut",
    exact: false,
    component: "components/LayOuts/DefaultLayOut",
    routes: [
        {
            path: "/address",
            name: "address",
            exact: true,
            component: "pages/Address/index",
            title:"我的地址",
        },
        {
            path: "/address/addaddress",
            name: "addaddress",
            exact: true,
            component: "pages/Address/children/Addaddress",
            title:"添加地址",
        },
        {
            path: "/address/addaddress/:id",
            name: "addaddress",
            exact: true,
            component: "pages/Address/children/Addaddress",
            title:"编辑地址",
        },
        {
            path: "*",
            component: "components/Exception/Page404"
        }
    ]
}
export default addressRoutes;