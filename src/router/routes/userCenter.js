 const userCenterRoutes = {
    path: "/userCenter",
    name: "DefaultLayOut",
    exact: false,
    component: "components/LayOuts/DefaultLayOut",
    routes: [
        {
            path: "/userCenter/index",
            name: "userCenter",
            exact: true,
            component: "pages/User/children/UserCenter",
            authorization: true,
            title:"个人资料"
        },
        {
            path: "/userCenter/setNikeName/:nikeName",
            name: "SetNikeName",
            exact: true,
            component: "pages/User/children/SetNikeName",
            authorization: true,
            title:"修改用户昵称"
        },
        {
            path: "*",
            component: "components/Exception/Page404"
        }
    ]
}
export default userCenterRoutes;