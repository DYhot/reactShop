 const paySetRoutes = {
    path: "/paySetting",
    name: "DefaultLayOut",
    exact: false,
    component: "components/LayOuts/DefaultLayOut",
    routes: [
        {
            path: "/paySetting",
            name: "paySetting",
            exact: true,
            component: "pages/PaySetting/index",
            title:"支付设置",
        },
        {
            path: "/paySetting/facePay",
            name: "facePay",
            exact: true,
            component: "pages/PaySetting/children/FacePay",
            title:"面容支付",
        },
        {
            path: "*",
            component: "components/Exception/Page404"
        }
    ]
}
export default paySetRoutes;