 const accountSecurityRoutes = {
    path: "/accountSecurity",
    name: "DefaultLayOut",
    exact: false,
    component: "components/LayOuts/DefaultLayOut",
    routes: [
        {
            path: "/accountSecurity",
            name: "accountSecurity",
            exact: true,
            component: "pages/AccountSecurity/index",
            title:"账户与安全",
        },
        {
            path: "/accountSecurity/gesturesPassword",
            name: "gesturesPassword",
            exact: true,
            component: "pages/AccountSecurity/children/GesturesPassword",
            title:"手势密码",
        },  
        {
            path: "/accountSecurity/setGestures",
            name: "setGestures",
            exact: true,
            component: "pages/AccountSecurity/children/SetGestures",
            title:"设置手势密码",
        },  
        {
            path: "/accountSecurity/passwordManage",
            name: "passwordManage",
            exact: true,
            component: "pages/AccountSecurity/children/PasswordManage",
            title:"密码管理",
        },  
        {
            path: "/accountSecurity/faceSet",
            name: "faceSet",
            exact: true,
            component: "pages/AccountSecurity/children/FaceSet",
            title:"面容",
        },
        {
            path: "/accountSecurity/faceScan",
            name: "faceScan",
            exact: true,
            component: "pages/AccountSecurity/children/FaceScan",
            title:"刷脸",
        },
        {
            path: "/accountSecurity/equipmentManage",
            name: "equipmentManage",
            exact: true,
            component: "pages/AccountSecurity/children/EquipmentManage",
            title:"设备管理",
        },   
        {
            path: "*",
            component: "components/Exception/Page404"
        }
    ]
}
export default accountSecurityRoutes;