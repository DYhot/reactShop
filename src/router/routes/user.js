 const userRoutes = {
    path: "/user",
    name: "UserLayOut",
    exact: false,
    component: "components/LayOuts/UserLayOut",
    routes: [
        {
            path: "/user",
            name: "/user",
            redirect: "/user/login",
            exact: true,
            component: "pages/User/Login",
        },
        {
            path: "/user/login",
            name: "login",
            exact: true,
            component: "pages/User/Login",
        },  
        {
            path: "*",
            component: "components/Exception/Page404"
        }
    ]
};
export default userRoutes;