const greenCardRoutes = {
    path: "/greenCard",
    name: "DefaultLayOut",
    exact: false,
    component: "components/LayOuts/DefaultLayOut",
    routes: [
        {
            path: "/greenCard",
            name: "greenCard",
            redirect: "/greenCard/index",
            exact: true,
            title: "绿卡",
            component: "pages/GreenCard/index"
        },
        {
            path: "/greenCard/index",
            name: "greenCard",
            exact: true,
            title: "绿卡",
            component: "pages/GreenCard/index"
        },
        {
            path: "/greenCard/openMember",
            name: "openMember",
            exact: true,
            component: "pages/GreenCard/children/OpenMember",
            authorization: true,
            title: "开通绿卡"
        },
        {
            path: "*",
            component: "components/Exception/Page404"
        }
    ]
};
export default greenCardRoutes;
