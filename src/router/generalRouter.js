import React from "react";
import { BrowserRouter as Router ,Switch,Redirect,Route,withRouter} from 'react-router-dom';

import Page500 from '@/components/Exception/Page500';
import Page404 from '@/components/Exception/Page404';
import BasicLayOut from '@/components/LayOuts/BasicLayOut2';
import Detail from "@/pages/Detail/index";
// const router = () => (
//     <Router>
//         <Switch>
//             <Route path="/" exact component={SelectMovies} />
//             <Route path="/test" exact component={BasicLayOut} />
//             <Route path="/home" component={BasicLayOut}/>
//             <Route path="/500" exact component={Page500} />
//             <Route path="/404" exact component={Page404} />
//             <Redirect from="*" to="/404" />
//         </Switch>
//   </Router>          
// )
import { TransitionGroup, CSSTransition } from "react-transition-group";
const ANIMATION_MAP = {
    PUSH: 'forward',
    POP: 'back'
  }
const Routes = withRouter(({location}) => (
    <TransitionGroup className={'router-wrapper'}
    childFactory={child => React.cloneElement(
        child,
        {classNames: ANIMATION_MAP[history.action]}
      )}>
      <CSSTransition
        key={location.key}
        timeout={{enter: 500, exit: 300}}
        classNames="forward-enter-active"
      >
        <Switch location={location}>
            <Route path="/" exact component={SelectMovies} />
            <Route path="/test" exact component={Test} />
            {/* <Route path="/home" component={Home}/> */}
            <Route path="/500" exact component={Page500} />
            <Route path="/404" exact component={Page404} />
            <Redirect from="*" to="/404" />
        </Switch>
      </CSSTransition>
    </TransitionGroup>
  ));
  
  
export default Routes
