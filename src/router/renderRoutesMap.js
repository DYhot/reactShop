import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import RouterGuard from "./routerGuard";

const renderRoutesMap = (routes,extraProps) =>
    routes.map((route, index) => {
        return (
            <Route
                key={index}
                path={route.path}
                exact={route.exact}
                render={props => {
                    // console.log("xxxx",props)
                  return  <RouterGuard {...route} {...props} />
                }}
            />
        );
    });

export default renderRoutesMap;
