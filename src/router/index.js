
import renderRoutes from './renderRoutes'
import routerConfig from './routerConfig'

const router = ({location}) => {
    return (
     renderRoutes({
        routes: routerConfig
    })
)}
export default router