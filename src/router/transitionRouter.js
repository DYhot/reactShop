import React from "react";
import { withRouter } from "react-router-dom";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import routerConfig from "./routerConfig";
import generateRouter from "./index";

const DEFAULT_SCENE_CONFIG = {
    enter: "from-right",
    exit: "to-exit"
};

const getSceneConfig = location => {
    const matchedRoute = routerConfig.find(config => 
         new RegExp(`^${config.path}$`).test(location.pathname)
    );
    // const matchedRoute = routerConfig.find(config => {
    //     const target = new RegExp(`^${config.path}$`).test(location.pathname);
    //     if (target && target) {
    //     }
    //     return target;
    // });
    console.log(999, matchedRoute);
    return (matchedRoute && matchedRoute.sceneConfig) || DEFAULT_SCENE_CONFIG;
};

let oldLocation = null;
const TransitionRouter = withRouter(({ location, history }) => {
    // 转场动画应该都是采用当前页面的sceneConfig，所以：
    // push操作时，用新location匹配的路由sceneConfig
    // pop操作时，用旧location匹配的路由sceneConfig
    console.log(888, history.action, location, history, oldLocation);
    let classNames = "";
    if (history.action === "PUSH") {
        classNames = "forward-" + getSceneConfig(location).enter;
    } else if (history.action === "POP" && oldLocation) {
        classNames = "back-" + getSceneConfig(oldLocation).exit;
    }

    // 更新旧location
    oldLocation = location;

    return (
        <TransitionGroup
            className={"router-wrapper"}
            childFactory={child => React.cloneElement(child, { classNames })}
        >
            <CSSTransition timeout={500} key={location.pathname}>
                {generateRouter(location)}
            </CSSTransition>
        </TransitionGroup>
    );
});
export default TransitionRouter;
