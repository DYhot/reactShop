import addressRoutes from './routes/address';
import userCenterRoutes from './routes/userCenter';
import userRoutes from './routes/user';
import greenCardRoutes from './routes/greenCard';
import paySetRoutes from './routes/paySet';
import accountSecurityRoutes from './routes/accountSecurity';
const routes = [
    {
        path: "/",
        name: "BasicLayOut",
        exact: true,
        redirect: "/home",
        component: "components/LayOuts/BasicLayOut"
    },
    {
        path: "/category",
        name: "BasicLayOut",
        exact: false,
        tabKey: "1",
        component: "components/LayOuts/BasicLayOut",
        routes: [
            {
                path: "/category",
                name: "category",
                exact: true,
                component: "pages/Category/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/cart",
        name: "BasicLayOut",
        exact: false,
        tabKey: "3",
        component: "components/LayOuts/BasicLayOut",
        routes: [
            {
                path: "/cart",
                name: "cart",
                exact: true,
                component: "pages/Cart/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/logistics",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/logistics",
                name: "logistics",
                exact: true,
                title:"物流信息",
                component: "pages/Logistics/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/evaluationDetail",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/evaluationDetail",
                name: "evaluationDetail",
                exact: true,
                title:"评价详情",
                component: "pages/EvaluationDetail/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/afterSale",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/afterSale",
                name: "afterSale",
                exact: true,
                title:"退款/售后",
                component: "pages/AfterSale/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/user/register",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/user/register",
                name: "register",
                exact: true,
                title:"注册",
                component: "pages/User/Register",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/user/forgetpwd",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/user/forgetpwd",
                name: "forgetpwd",
                exact: true,
                title:"修改密码",
                component: "pages/User/Forgetpwd",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/settlement",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/settlement",
                name: "settlement",
                exact: true,
                title:"填写订单",
                component: "pages/Settlement/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/result",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/result",
                name: "result",
                exact: true,
                title:"支付状态",
                returnUrl:"/myOrder",
                component: "pages/Result/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/mine",
        name: "BasicLayOut",
        exact: false,
        tabKey: "4",
        component: "components/LayOuts/BasicLayOut",
        routes: [
            {
                path: "/mine",
                name: "mine",
                exact: true,
                component: "pages/Mine/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/systemSet",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/systemSet",
                name: "systemSet",
                exact: true,
                title:"系统设置",
                returnUrl:"/mine",
                component: "pages/SystemSet/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/luckDraw",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/luckDraw",
                name: "luckDraw",
                exact: true,
                title:"幸运抽奖",
                component: "pages/LuckDraw/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/aboutUs",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/aboutUs",
                name: "aboutUs",
                exact: true,
                title:"关于我们",
                component: "pages/AboutUs/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    accountSecurityRoutes,
    paySetRoutes,
    addressRoutes,
    userCenterRoutes,
    userRoutes,
    greenCardRoutes,
    {
        path: "/detail",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/detail/:id",
                name: "detail",
                exact: true,
                component: "pages/Detail/index",
                title:"商品详情",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/goodsList",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/goodsList",
                name: "goodsList",
                exact: true,
                component: "pages/GoodsList/index",
                title:"商品清单",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/paymentDetail",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/paymentDetail",
                name: "paymentDetail",
                exact: true,
                component: "pages/PaymentDetail/index",
                title:"付款详情",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/myOrder",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/myOrder",
                name: "myOrder",
                exact: true,
                title:"我的订单",
                returnUrl:"/mine",
                component: "pages/MyOrder/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/myCoupon",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/myCoupon",
                name: "myCoupon",
                exact: true,
                redirect: "/myCoupon/index",
                component: "pages/MyCoupon/index",
                title:"我的优惠券",
            },
            {
                path: "/myCoupon/index",
                name: "myCoupon",
                exact: true,
                component: "pages/MyCoupon/index",
                title:"我的优惠券",
            },
            {
                path: "/myCoupon/couponRecords",
                name: "myCoupon",
                exact: true,
                component: "pages/MyCoupon/children/CouponRecords",
                title:"优惠券使用记录",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/home",
        name: "BasicLayOut",
        exact: false,
        tabKey: "0",
        component: "components/LayOuts/BasicLayOut",
        routes: [
            {
                path: "/home",
                name: "home",
                exact: true,
                component: "pages/Home/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/discover",
        name: "BasicLayOut",
        exact: false,
        tabKey: "2",
        component: "components/LayOuts/BasicLayOut",
        routes: [
            {
                path: "/discover",
                name: "discover",
                exact: true,
                component: "pages/Discover/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/refundDetail",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/refundDetail",
                name: "refundDetail",
                exact: true,
                title:"退款详情",
                component: "pages/RefundDetail/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/evaluation",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/evaluation",
                name: "evaluation",
                exact: true,
                title:"评价",
                component: "pages/Evaluation/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/feedBack",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/feedBack",
                name: "feedBack",
                exact: true,
                title:"反馈",
                component: "pages/FeedBack/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/notification",
        name: "DefaultLayOut",
        exact: false,
        component: "components/LayOuts/DefaultLayOut",
        routes: [
            {
                path: "/notification",
                name: "notification",
                exact: true,
                title:"消息中心",
                component: "pages/Notification/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/demo",
        name: "BasicLayOut",
        exact: false,
        component: "components/LayOuts/BasicLayOut",
        routes: [
            {
                path: "/demo",
                name: "demo",
                exact: true,
                component: "pages/Demo/index",
            },
            {
                path: "*",
                component: "components/Exception/Page404"
            }
        ]
    },
    {
        path: "/404",
        name: "BasicLayOut",
        exact: true,
        component: "components/Exception/Page404",
    },
    {
        path: "/500",
        name: "BasicLayOut",
        exact: true,
        component: "components/Exception/Page500",
    },
    {
        path: "/error",
        name: "BasicLayOut",
        exact: true,
        component: "components/Exception/Error",
    },
    {
        path: "*",
        component: "components/Exception/Page404"
    }
];

export default routes;
