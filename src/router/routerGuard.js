import React, { Component } from "react";
import {
    BrowserRouter as Router,
    Route,
    withRouter,
    Redirect,
    Switch,
} from "react-router-dom";
import { connect } from "react-redux";
import renderRoutesMap from "./renderRoutesMap";
import { selector } from "postcss-selector-parser";
import Loadable from "./Loadable.js";
import appAction from "@/redux/actions/app";
import HeaderNavBar from "@/components/LayOuts/HeaderNavBar";

// const mapStateToProps = state => state;
const mapStateToProps = (state, ownProps) => {
    let { updateAppState } = state;
    return {
        accessToken: updateAppState.loginStatus.accessToken
    };
};
const mapDispatchToProps = dispatch => ({ ...dispatch });
class RouterGuard extends React.Component {
    constructor(props) {
        super();
    }
    componentWillMount() {
        const {
            history: { replace },
            redirect,
            authorization,
            location,
            history,
            match,
            accessToken
        } = this.props;
        console.log("重定向地址：", `${redirect}`, match);
        window.g_router=history;
        if (redirect) {
            replace(`${redirect}`);
        } else if (authorization && !accessToken) {
            console.log("需要认证");
            replace("./user");
        }
        // if (location.pathname == "/") replace("./selectMovies");
        // console.log("路由跳转前的拦截", this.props,redirect);
    }
    componentDidMount() {}

    render() {
        let { component, routes = [] ,title,tabKey} = this.props;
        console.log("准备渲染compoent前", title,this.props, `${component}`);
        const LoadableComponent = Loadable({
            loader: () => import(`../${component}`)
        });
        return (
            <React.Fragment>
                <LoadableComponent {...this.props}>
                    <Switch>
                        {renderRoutesMap(routes)}
                    </Switch>
                </LoadableComponent>
            </React.Fragment>
        );
    }
}

// export default withRouter(
//     connect(mapStateToProps, mapDispatchToProps)(RouterGuard)
// );
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(RouterGuard));