import React from 'react' ;
import L from 'react-loadable';
import LoadingCircle from '@/components/Loading/LoadingCircle';// 加载等待时的提示模块
import Error from '@/components/Exception/Error';// 加载错误时的提示模块

const LoadingComponent=({ error, pastDelay })=> {
    console.log("Loadable参数：",error, pastDelay )
  if (error) {
    return <Error errorInfo={error}/>;  
  } else if (pastDelay) {
    return (LoadingCircle(true)); 
  } else {
    return null; // 加载时间短于pastDelay（默认200ms）则不显示Loading动画
  }
}

const Loadable = opts =>{
	return L({
		loading:LoadingComponent,
		delay: 200, // 闪屏时间
        ...opts,
	})
}

export default Loadable