/**
 * Created by DengYang on 4/28/2019.
 */
import 'whatwg-fetch'
import {setSign, createSign} from '@/utils/sign'
import qs from 'qs'

let fetchService = {
    get: (url) => {
        return fetch(url).then((response) => {
            console.log("请求回来的数据xx：",response)
            return response.json();
        });
    },
    post: (url, body) => {
        localStorage.memberId && (body.memberId = localStorage.memberId)
        body = Object.assign({random: Math.random(), sign: createSign(setSign(body))}, body)
        var searchParams = new URLSearchParams()
        console.log(body,localStorage.memberId)
        Object.keys(body).forEach(item=>{
            searchParams.set(item,body[item])
        })
        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': url.indexOf('app/')?"application/x-www-form-urlencoded; charset=UTF-8":'application/json'
            },
            body:url.indexOf('app/')?searchParams:JSON.stringify(body)
        }).then((response) => {
            console.log("request响应的数据：",response)
            return response.json();
        });
    }
};
export default fetchService;