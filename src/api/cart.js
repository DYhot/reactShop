/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 获取用户收货地址列表
 * @param {string} {current} 当前页
 * @param {string} {size} 每页显示条数
 * @returns {Promise}
 */
export function getCartList(query) {
    return request(
        `/base/mobile/cart/list?${qs.stringify(query)}`
    );
}
/**
 *
 * 获取购物车商品总数
 * @returns {Promise}
 */

export function getCartTotalCount(query) {
    return request(
        `/base/mobile/cart/getCartTotalCount`
    );
}
/**
 *
 * 添加商品到购物车
 * @param {string} {id} 商品id
 * @returns {Promise}
 */
export function addGoodToCart(query) {
    return request(
        `/base/mobile/cart/addGoodToCart`,
        {
            method: "POST",
            body: query
        }
    );
}

// 根据id批量删除购物车商品
export function deleteCartGoodsBatchById(query) {
    return request(`/base/mobile/cart/deleteCartGoodsBatchById`,{
      method: 'DELETE',
      body: query,
    });
  }
/**
 *
 * 更新购物车商品数量/是否被选中
 * @param {string} {count}商品数量
 * @param {string} {isSelected}是否被选中
 * @returns {Promise}
 */
export function updateCartGoodsParams(params) {
    return request("/base/mobile/cart/updateCartGoodsParams", {
        method: "PUT",
        body: params
    })
}
