/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
/**
 *
 * 获取短信验证码
 * @param {string} {phone} 电话号码
 * @returns {Promise}
 */
export function getSmsCode(params) {
    return request("/base/account/user/getSms", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 用户注册
 * @param {string} {mail} 邮箱
 * @param {string} {password} 密码
 * @param {string} {confirm} 确认密码
 * @param {string} {prefix} 电话号码前缀
 * @param {string} {mobile} 电话号码
 * @param {string} {captcha} 短信验证码
 * @param {string} {smsId} 短信验id
 * @returns {Promise}
 */
export async function accountRegister(params) {
    return request('/base/account/user/register', {
      method: 'POST',
      body: params,
    });
  }
/**
 *
 * 修改用户密码
 * @param {string} {mail} 邮箱
 * @param {string} {password} 密码
 * @param {string} {confirm} 确认密码
 * @param {string} {prefix} 电话号码前缀
 * @param {string} {mobile} 电话号码
 * @param {string} {captcha} 短信验证码
 * @param {string} {smsId} 短信验id
 * @returns {Promise}
 */
export async function accountModifyPwd(params) {
    return request('/base/account/user/modifyPwd', {
      method: 'PUT',
      body: params,
    });
  }
/**
 *
 * 用户登录
 * @param {string} {phone} 电话号码
 * @param {string} {code} 短信验证码
 * @returns {Promise}
 */
export function LoginCodeSubmit(params) {
    return request("/base/account/user/loginByPhone", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 用户登录
 * @param {string} {phone} 电话号码
 * @param {string} {password} 密码
 * @returns {Promise}
 */
export function LoginPwdSubmit(params) {
    const data = params;
    return request("/base/account/user/login", { method: "POST", body: data })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 用户退出登录
 * @param {string} {null}
 * @returns {Promise}
 */
export function layOutLogin() {
    return request(`/base/account/user/layOut`);
}
/**
 *
 * 获取用户信息
 * @param {string} {null}
 * @returns {Promise}
 */
export function getUserInfo() {
    return request(`/base/account/user/getUserInfo`);
}
/**
 *
 * 修改用户性别
 * @param {string} {name} 姓名
 * @returns {Promise}
 */
export function updateGender(params) {
    return request("/base/account/user/updateGender", {
        method: "PUT",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}

/**
 *
 * 修改用户性别
 * @param {string} {birthday} 生日
 * @returns {Promise}
 */
export function updateBirthday(params) {
    return request("/base/account/user/updateBirthday", {
        method: "PUT",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 修改用户昵称
 * @param {string} {nikeName} 昵称
 * @returns {Promise}
 */
export function updateNikeName(params) {
    return request("/base/account/user/updateNikeName", {
        method: "PUT",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}

/**
 *
 * 上传用户头像
 * @param {string} {avatar} 用户头像
 * @returns {Promise}
 */
export function upLoadAvatar(params) {
    return request("/base/system/common/file/upLoadAvatar", {
        method: "POST",
        body: params
    })
}

/**
 *
 * 用户开通绿卡
 * @param {string} {payId} 支付方式
 * @param {string} {greenCardId} 绿卡类型
 * @param {string} {totalAmount} 总金额
 * @returns {Promise}
 */
export async function openGreenCard(params) {
    return request('/base/account/user/openGreenCard', {
      method: 'POST',
      body: params,
    });
  }
