/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 上传反馈文件图片
 * @param {string} {files} 文件对象
 * @returns {Promise}
 */

export function uploadFeedBackFile(params) {
    return request("/base/system/common/file/uploadFeedBackFile", {
        method: "POST",
        body: params
    })
}
/**
 *
 * 上传商品评价文件
 * @param {string} {files} 文件对象
 * @returns {Promise}
 */

export function uploadEvaluationFile(params) {
    return request("/base/system/common/file/uploadEvaluationFile", {
        method: "POST",
        body: params
    })
}
