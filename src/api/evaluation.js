/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 新增用户评价
 * @param {string} {file_ids} 评价文件id集合
 * @param {string} {content} 评价内容
 * @param {string} {good_star} 商品评价
 * @param {string} {good_conform} 商品符合度
 * @param {string} {service_attitude} 店家服务态度
 * @param {string} {courier_speed} 快递配送速度
 * @param {string} {courier_service} 快递员服务
 * @param {string} {courier_packaging} 快递包装
 * @param {string} {cart_ids} 关联的购物车ids
 * @param {string} {order_num} 订单号
 * @returns {Promise}
 */

export function createEvaluation(params) {
    return request("/base/mobile/evaluation/create", {
        method: "POST",
        body: params
    })
}

/**
 *
 * 获取评价详情列表
 * @returns {Promise}
 */
export function getEvaluationList(query) {
    return request(
        `/base/mobile/evaluation/list?${qs.stringify(query)}`
    );
}