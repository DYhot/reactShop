/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 新增用户收货地址
 * @param {string} {name} 姓名
 * @param {string} {phone} 电话
 * @param {string} {area} 省市区
 * @param {string} {areaDeatil} 详细地址
 * @param {string} {zipCode} 邮编
 * @param {string} {isDefault} 是否是默认地址
 * @returns {Promise}
 */
export function addUserAdress(params) {
    return request("/base/mobile/receiveAddress/addAddress", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 编辑用户收货地址
 * @param {string} {name} 姓名
 * @param {string} {phone} 电话
 * @param {string} {area} 省市区
 * @param {string} {areaDeatil} 详细地址
 * @param {string} {zipCode} 邮编
 * @param {string} {isDefault} 是否是默认地址
 * @returns {Promise}
 */
export function updateUserAdress(params) {
    return request("/base/mobile/receiveAddress/updateAddress", {
        method: "PUT",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 获取用户收货地址列表
 * @param {string} {current} 当前页
 * @param {string} {size} 每页显示条数
 * @returns {Promise}
 */
export function getUserAdress(query) {
    return request(
        `/base/mobile/receiveAddress/getAddress?${qs.stringify(query)}`
    );
}
/**
 *
 * 删除用户收货地址
 * @param {string} {id} 用户收货地址id
 * @returns {Promise}
 */
export function delUserAdress(query) {
    return request("/base/mobile/receiveAddress/delAddress", {
        method: "DELETE",
        body: query
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 根据地址id获取用户收货地址
 * @param {string} {id} 地址id
 * @returns {Promise}
 */
export function getUserAdressById(query) {
    return request(
        `/base/mobile/receiveAddress/getUserAdressById?${qs.stringify(query)}`
    );
}

/**
 *
 * 获取用户默认收货地址
 * @param {string} {id} 地址id
 * @returns {Promise}
 */
export function getUserDefaultAdress(query) {
    return request(
        `/base/mobile/receiveAddress/getUserDefaultAdress`
    );
}
/**
 *
 * 新增用户收货地址
 * @param {string} {tagName} 标签名字
 * @param {string} {addressId} 地址id
 * @returns {Promise}
 */
export function addAdressTag(params) {
    return request("/base/mobile/receiveAddress/addAdressTag", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 更新用户地址标签状态
 * @param {string} {addressTagId} 地址标签id
 * @param {string} {tagStatus} 地址标签状态
 * @returns {Promise}
 */
export function updateUserAdressTagStatus(params) {
    return request("/base/mobile/receiveAddress/updateUserAdressTagStatus", {
        method: "PUT",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}

/**
 *
 * 更新用户地址标签状态
 * @param {string} {id} 地址id
 * @param {string} {isdefault} 地址是否为默认地址 0：否 1：是
 * @returns {Promise}
 */
export function updateUserAdressStatus(params) {
    return request("/base/mobile/receiveAddress/updateUserAdressStatus", {
        method: "PUT",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
