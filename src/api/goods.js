/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 获取商品详情
 * @param {string} {id} 商品id
 * @returns {Promise}
 */
export function getGoodsDetailById(query) {
    return request(
        `/base/mobile/goods/getGoodsDetailById?${qs.stringify(query)}`
    );
}
/**
 *
 * 获取商品列表
 * @param {string} {current} 当前页
 * @param {string} {size} 每页显示条数
 * @returns {Promise}
 */
export function getGoodsList(query) {
    return request(
        `/base/mobile/goods/getGoodsList?${qs.stringify(query)}`
    );
}

