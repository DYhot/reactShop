/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 新增用户反馈
 * @param {string} {type} 反馈问题类型
 * @param {string} {content} 反馈内容
 * @param {string} {phone} 反馈人联系电话
 * @param {string} {file_ids} 反馈图片id集合
 * @returns {Promise}
 */

export function createFeedBack(params) {
    return request("/base/mobile/feedBack/create", {
        method: "POST",
        body: params
    })
}
