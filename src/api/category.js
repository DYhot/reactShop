/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 获取分类导航菜单列表
 * @returns {Promise}
 */
export function getCategoryMenu(query) {
    return request(
        `/base/mobile/category/getCategoryMenu`
    );
}
/**
 *
 * 根据分类id获取分类商品列表
 * @returns {Promise}
 */
export function getGoodsByCategoryId(query) {
    console.log(123,query)
    return request(
        `/base/mobile/category/getGoodsByCategoryId?${qs.stringify(query)}`
    );
}

