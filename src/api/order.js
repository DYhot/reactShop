/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";
/**
 *
 * 获取用户订单列表
 * @param {string} {current} 当前页
 * @param {string} {size} 每页显示条数
 * @param {string} {type} 订单类型0:全部1:待支付2:待收货3:待评价
 * @param {string} {order_num} 订单编号
 * @returns {Promise}
 */

export function getUserOrderList(query) {
    return request(`/base/mobile/order/list?${qs.stringify(query)}`);
}
/**
 *
 * 生成商品订单
 * @param {string} {note} 备注
 * @param {string} {adress_id} 收货人地址id
 * @param {string} {count} 总件数
 * @param {string} {amount} 订单金额
 * @param {string} {freight} 配送费
 * @param {string} {integral} 积分
 * @param {string} {deliveryTime} 送达时间
 * @param {string} {goods_id} 商品id集合
 * @param {string} {payType} 支付方式
 * @param {string} {couponId} 优惠券id
 * @returns {Promise}
 */
export function createGoodsOrder(params) {
    return request("/base/mobile/order/createOrder", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}

/**
 *
 * 支付订单
 * @param {string} {order_num} 订单号
 * @param {string} {payId} 支付方式id
 * @param {string} {totalAmount} 总金额
 * @returns {Promise}
 */
export function payOrder(params) {
    return request("/base/mobile/order/payOrder", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 获取支付订单倒计时
 * @param {string} {order_num} 订单编号
 * @returns {Promise}
 */
export function getOrderCutDown(query) {
    return request(`/base/mobile/order/cutDown?${qs.stringify(query)}`);
}
/**
 *
 * 根据ids批量删除订单
 * @param {string} {order_num} 订单编号
 * @returns {Promise}
 */
export function deleteOrderBatchById(query) {
    return request(`/base/mobile/order/deleteOrderBatchById`, {
        method: "DELETE",
        body: query
    });
}

/**
 *
 * 用户订单确认收货
 * @param {string} {id} 订单id
 * @returns {Promise}
 */
export function confirmGoods(params) {
    return request("/base/mobile/order/confirmGoods", {
        method: "POST",
        body: params
    })
        .then(response => {
            return response;
        })
        .catch(err => {
            console.log("请求失败", err);
        });
}
/**
 *
 * 根据order_num获取订单物流信息
 * @returns {Promise}
 */
export function getLogisticsByOrdernum(query) {
    return request(
        `/base/mobile/order/getLogisticsByOrdernum?${qs.stringify(query)}`
    );
}
/**
 *
 * 获取用户相关统计信息
 * @param {string} {null}
 * @returns {Promise}
 */
export function getUserStatistical() {
    return request(`/base/mobile/order/getUserStatistical`);
}