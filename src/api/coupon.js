import request from "@/services/request";
import qs from "qs";
/**
 *
 * 获取优惠券列表
 * @returns {Promise}
 */
export function getCouponList(query) {
    return request(
        `/base/mobile/coupon/list?${qs.stringify(query)}`
    );
}

/**
 *
 * 用户领取优惠券
 * @param {string} {couponId} 优惠券id
 * @returns {Promise}
 */
export function getCoupon(params) {
    return request("/base/mobile/coupon/getCoupon", {
        method: "POST",
        body: params
    })
}

/**
 *
 * 获取用户优惠券列表
 * @returns {Promise}
 */
export function getUserCouponList(query) {
    return request(
        `/base/mobile/coupon/getUserCouponList?${qs.stringify(query)}`
    );
}
/**
 *
 * 获取用户已使用优惠券列表
 * @returns {Promise}
 */
export function getUserCouponUsedList(query) {
    return request(
        `/base/mobile/coupon/getUserCouponUsedList?${qs.stringify(query)}`
    );
}