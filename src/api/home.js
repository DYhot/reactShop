/**
 * Created by DengYang on 4/28/2019.
 */
import request from "@/services/request";
import qs from "qs";

/**
 *
 * 获取首页导航菜单列表
 * @returns {Promise}
 */
export function getMenu(query) {
    return request(
        `/base/mobile/home/getMenu`
    );
}
/**
 *
 * 获取首页banner列表
 * @returns {Promise}
 */
export function getBanner(query) {
    return request(
        `/base/mobile/home/getBanner`
    );
}

