/**
 * Created by DY on 2020/3/2.
 * 会显示默认图片的image
 */
import React ,{Fragment}from "react";

class DefaultImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            done: false
        };
    }
    componentWillMount() {
        // 创建一个虚拟图片
        const img = new Image();
        // 发出请求，请求图片
        img.src = this.props.src;
        // 当图片加载完毕
        img.onload = () => {
            this.setState({
                done: true
            });
        };
    }
    render() {
        let props = this.props;
        let { src } = this.state;
        return (
            <Fragment>
                {this.state.done ? (
                    <img
                        style={{
                            width: this.props.width + "px",
                            height: this.props.height + "px"
                        }}
                        src={this.props.src}
                    />
                ) : (
                    <img
                        style={{
                            width: this.props.width + "px",
                            height: this.props.height + "px"
                        }}
                        src={require("../../assets/images/common/loading/img-load.png")}
                    />
                )}
            </Fragment>
        );
    }
}

export default DefaultImage;
