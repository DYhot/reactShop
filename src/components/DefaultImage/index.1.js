/**
 * Created by DY on 2020/3/2.
 * 会显示默认图片的image
 */
import React from "react";

class DefaultImage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: this.props.src||require("../../assets/images/common/loading/img-load.png")
        };
    }

    handleImageLoaded() {
        //加载完毕
    }

    handleImageErrored() {
        //加载失败
        this.setState({
            src: require("../../assets/images/common/loading/img-load.png")
        });
    }

    render() {
        let props = this.props;
        let { src } = this.state;
        return (
            <img
                {...props}
                src={src}
                onLoad={this.handleImageLoaded.bind(this)}
                onError={this.handleImageErrored.bind(this)}
                alt={require("../../assets/images/common/loading/img-load.png")}
            />
        );
    }
}

export default DefaultImage;
