// pullDown 组件配置
pullDownConfig={
threshold: PULLDOWN_THRESHOLD, // 配置顶部下拉的距离来决定刷新时机
stop: STOP // 回弹停留的距离
};
// pullUp 组件配置
pullUpConfig={
threshold: PULLUP_THRESHOLD // 配置顶部下拉的距离来决定刷新时机
};
pullDownTips=PULLDOWN_TIPS;
pullUpTips=PULLUP_TIPS;
pullDown= true; // 是否支持下拉刷新
pullUp= true; // 是否支持上拉加载
zIndex= 100; // z-index 配置
// 是否无更多数据 - 只有开启 Pullup 的情况下传
noMore= false;
noMoreTips= "无更多数据";
// 下拉、上拉操作结束后的停留时间
stayTime= TIME_STOP;
empty= true;
beforePullDown= true; // 状态 下拉之前
isPullingDown= false; // 状态 下拉过程中
pullDownY= 0;
isPullUpLoad= false; // 状态 上拉之前
pullUpLoading= false ;// 状态 上拉过程中
