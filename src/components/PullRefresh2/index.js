/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import BScroll from "better-scroll";
import loadingGif from "@/assets/images/common/loading/loading.gif";
import DefaultImage from "@/components/DefaultImage";
import CustomIcon from "@/components/Icon/CustomIcon";
import PropTypes from "prop-types";

const TIME_BOUNCE = 500;
const TIME_STOP = 500;
const PULLDOWN_THRESHOLD = 50;
const PULLUP_THRESHOLD = 10;
const STOP = 60;
const PULLDOWN_TIPS = {
    before: "下拉可以刷新",
    pulling: "释放刷新数据",
    loading: "正在刷新数据中...",
    finished: "加载完成"
};
const PULLUP_TIPS = {
    before: "上拉加载更多",
    loading: "加载中..."
};
class PullRefresh extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state={
            rotateArrow: false,//是否旋转箭头
        };
        // pullDown 组件配置
        this.pullDownConfig = {
            threshold: PULLDOWN_THRESHOLD, // 配置顶部下拉的距离来决定刷新时机
            stop: STOP // 回弹停留的距离
        };
        // pullUp 组件配置
        this.pullUpConfig = {
            threshold: PULLUP_THRESHOLD // 配置顶部下拉的距离来决定刷新时机
        };
        this.pullDownTips = PULLDOWN_TIPS;
        this.pullUpTips = PULLUP_TIPS;
        this.pullDown = props.isPullDown || true; // 是否支持下拉刷新
        this.pullUp = props.isPullUp || true; // 是否支持上拉加载
        this.zIndex = 100; // z-index 配置
        // 是否无更多数据 - 只有开启Pullup的情况下传
        this.noMore = props.noMore || false;
        this.noMoreTips = "无更多数据";
        // 下拉、上拉操作结束后的停留时间
        this.stayTime = TIME_STOP;
        this.empty = props.empty || false;
        this.beforePullDown = true; // 状态 下拉之前
        this.isPullingDown = false; // 状态 下拉过程中
        this.pullDownY = 0;
        this.isPullUpLoad = false; // 状态 上拉之前
        this.pullUpLoading = false; // 状态 上拉过程中
        this.downTipsText = ""; //下拉显示的提示文字
        this.upTipsText = ""; //下拉显示的提示文字
        // this.rotateArrow = false; //是否旋转箭头
        this.bscroll=null;
    }

    componentDidMount() {}
    componentDidUpdate() {
        this.initBscroll();
    }
    tipsStyle = () => {
        return {
            zIndex: this.zIndex
        };
    };
    contentStyle = () => {
        return {
            zIndex: this.zIndex + 1,
            paddingBottom: this.noMore || this.pullUp ? "50px" : 0
        };
    };
    showNoMoreTips = () => {
        return this.noMore && this.pullUp && !this.empty;
    };
    pullingDownOut = () => {
        // console.log(333);
        return this.pullDownY > this.pullDownConfig.threshold || false;
    };
    /**
     * 初始化
     */
    initBscroll = () => {
        if(this.bscroll){
            return;
        }
        const config = {
            scrollY: true,
            bounce: {
                top: this.pullDown ? true : false,
                bottom: this.pullUp ? true : false
            },
            bounceTime: TIME_BOUNCE,
            pullDownRefresh: this.pullDown ? this.pullDownConfig : false,
            pullUpLoad: this.pullUp ? this.pullUpConfig : false
        };
        this.bscroll = new BScroll(this.refs.PullRefreshWrapper, config);
        if (this.pullDown) {
            this.bscroll.on("pullingDown", this.pullingDownHandler);
        }
        if (this.pullUp) {
            this.bscroll.on("pullingUp", this.pullingUpHandler);
        }
        // this.bscroll.on("scroll", this.scrollHandler);
        this.bscroll.on("scrollStart", this.scrollStartHandler);
    };
    /**
     * 更新better scroll的dom
     */
    refreshDom = () => {
        return new Promise((resolve, reject) => {
            this.bscroll.refresh();
            resolve();
        });
    };
    /**
     * 滚动开始触发
     */
    scrollStartHandler = () => {
        if(this.bscroll.movingDirectionY===-1){
            // this.rotateArrow=true;
            this.setState({
                rotateArrow:true
            })
        }else{
            this.setState({
                rotateArrow:false
            })
            // this.rotateArrow=false;
        }
    };
    /**
     * 下拉滚动持续触发
     * @param pos 滚动过程中的坐标
     */
    scrollHandler = pos => {
        // console.log("滚动过程中的坐标:",pos,this.bscroll.movingDirectionY);
        this.pullDownY = pos.y;
        // this.scrollDirectionY()
        if (pos.y > 50) {
            // this.rotateArrow = true;
            this.setState({
                rotateArrow:true
            })
        } else {
            // this.rotateArrow = false;
            this.setState({
                rotateArrow:false
            })
        }
    };
    //判断滚动方向
    scrollDirectionY = async () => {
        const downTipsConfig = this.pullDownTips;
        let tips = "";
        // -1 表示从上往下滑，1 表示从下往上滑，0 表示没有滑动
        if (this.bscroll.movingDirectionY === -1) {
            if (this.pullDownY > 0 && this.pullDownY < 30) {
                tips = downTipsConfig.before;
            }
            if (this.pullDownY > 50) {
                tips = downTipsConfig.pulling;
            }
        }
        if (this.bscroll.movingDirectionY === 1) {
        }
        this.downTipsText = (
            <p className="PullRefresh_downTips" style={this.tipsStyle()}>
                {tips}
                {this.freshTime ? (
                    <p>
                        <span>最后更新:今天 {this.freshTime}</span>
                    </p>
                ) : null}
            </p>
        );
        // console.log(this.downTipsText);
    };

    /**
     * 下拉滚动结束触发
     */
    pullingDownHandler = async () => {
        this.beforePullDown = false;
        this.isPullingDown = true;
        // 触发刷新操作
        await this.refeashData();
        // 下拉过程结束，进入回弹状态
        this.isPullingDown = false;
        this.finishPullDown();
    };
    /**
     * 下拉触发pullDown事件
     */
    refeashData = async () => {
        return new Promise((resolve, reject) => {
            // this.$emit("pullDown", resolve);
            this.props.pullDown().then(res => {
                resolve();
            });
        });
    };
    /**
     * 上拉触发pullUp事件
     */
    getMore = () => {
        return new Promise((resolve, reject) => {
            // this.$emit("pullUp", resolve);
            // this.props.pullUp()
            this.props.pullUp().then(res => {
                resolve();
            });
        });
    };
    /**
     * 下拉结束后调用的方法，用于处理停留时间等
     */
    finishPullDown = async () => {
        const stopTime = this.stayTime;
        await new Promise(resolve => {
            setTimeout(() => {
                this.bscroll.finishPullDown();
                resolve();
            }, stopTime);
        });
        setTimeout(() => {
            // console.log(777, this.isPullingDown);
            this.beforePullDown = true;
            this.bscroll.refresh();
        }, TIME_BOUNCE);
    };
    /**
     * 上拉触发的事件
     */
    pullingUpHandler = async () => {
        // console.log(789, this.noMore, this.empty);
        if (this.noMore || this.empty) return false;
        this.isPullUpLoad = true;
        this.pullUpLoading = true;
        await this.getMore();
        // console.log(444, this.noMore, this.empty);
        this.pullUpLoading = false;
        this.bscroll.finishPullUp();
        this.bscroll.refresh();
        this.isPullUpLoad = false;
    };
    /**
     * 上拉提示文字
     */
    upTips = () => {
        if (!this.pullUp || this.empty) return null;
        const upTips = this.pullUpTips;
        let tips = "";
        if (this.noMore) {
            tips = this.noMoreTips;
        } else {
            tips =
                !this.isPullUpLoad && !this.pullUpLoading
                    ? upTips.before
                    : upTips.loading;
        }
        return <p className="PullRefresh_upTips">{tips}</p>;
    };
    /**
     * 下拉提示文字
     */
    downTips = () => {
        // console.log(
        //     "调用下拉提示文字函数",
        //     this.beforePullDown,
        //     this.isPullingDown,
        //     this.pullDown
        // );
        const {freshTime}=this.props;
        if (!this.pullDown) return null;
        const downTipsConfig = this.pullDownTips;
        let tips = "";
        let showLoading=false;
        // console.log(555, this.beforePullDown, this.isPullingDown);
        if (this.beforePullDown && !this.isPullingDown) {
            tips = this.pullingDownOut()
                ? downTipsConfig.pulling
                : downTipsConfig.before;
            // console.log(5551,this.pullingDownOut(),tips)
        } else {
            showLoading=this.isPullingDown?false:true;
            tips = this.isPullingDown
                ? downTipsConfig.loading
                : downTipsConfig.finished;
            // console.log(5552, this.isPullingDown, tips);
        }
        return (
            <div className="PullRefresh_downTips" style={this.tipsStyle()}>
                <div className="left_symbol">
                    {showLoading?<DefaultImage src={loadingGif} />:
                    <span
                        className={
                            this.state.rotateArrow ? "rotate_arrow" : "reset_arrow"
                        }
                    >
                        <CustomIcon
                            icon="#iconxiajiantou"
                            width="25px"
                            height="25px"
                            color="#fff"
                        />
                    </span>}
                </div>
                <div className="right_text">
                    {tips}
                    {freshTime ? (
                        <p>
                            <span>
                                最后更新:今天 {freshTime}
                                {this.state.rotateArrow}
                            </span>
                        </p>
                    ) : null}
                </div>
            </div>
        );
    };

    render() {
        return (
            <div className="PullRefreshComponent">
                <div className="PullRefresh">
                    <div
                        className="PullRefresh_wrapper"
                        ref="PullRefreshWrapper"
                    >
                        <div
                            className="PullRefresh_content"
                            style={this.contentStyle()}
                        >
                            {this.props.children}
                            {this.showNoMoreTips() && (
                                <div className="PullRefresh_noMore">
                                    {this.noMoreTips}
                                </div>
                            )}
                            {this.upTips()}
                        </div>
                        {this.downTips()}
                        {/* {this.downTipsText} */}
                    </div>
                </div>
            </div>
        );
    }
}
PullRefresh.defaultprops = {
    freshTime: "",// 刷新时间配置
};
PullRefresh.propTypes = {
    freshTime: PropTypes.string,// 刷新时间配置
};
export default PullRefresh;
