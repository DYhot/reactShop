import React from "react";
import "./styles/userLayOut.scss";

class UserLayOut extends React.Component {
    render() {
        const { children } = this.props;
        return <div className="userLayOut-wrapper">{children}</div>;
    }
}
export default UserLayOut;
