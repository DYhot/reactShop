/**
 * Created by Dengyang on 12/1/2019.
 */
import React from "react";
import { Link, withRouter } from "react-router-dom";
import "./styles/tabBar.scss";
// 状态管理
import { connect } from "react-redux";
import appAction from "../../redux/actions/app";
import CustomIcon from "@/components/Icon/CustomIcon";

class TabBar extends React.Component {
    constructor() {
        super();
    }
    componentWillMount() {}
    componentDidMount() {
        let { activeIndex, tabKey = "" } = this.props;
        // console.log("tabKey的值：", Boolean(!tabKey),tabKey, activeIndex, this.props);
        if (tabKey != activeIndex) {
            this.toggleBar(tabKey,this.props);
        }
    }
    toggleBar(index,props=null) {
        const { updateTabBar } = this.props;
        let { activeIndex} = this.props;
        if(activeIndex==index){
            return;
        }
        updateTabBar({ activeIndex: index });
        switch (index) {
            case "0":
                this.props.history.push("/home");
                break;
            case "1":
                !props&&this.props.history.push("/category");
                props&&Object.assign(this.props.history.location,props.location) ;
                break;
            case "2":
                this.props.history.push("/discover");
                break;
            case "3":
                this.props.history.push("/cart");
                break;
            case "4":
                this.props.history.push("/mine");
                break;
            default:
        }
    }

    render() {
        const { activeIndex } = this.props;
        return (
            <div className="tabBar-Wrapper">
                {/* <img className="tabBar-bg" src={require('../../assets/images/common/tabbar-bg.png')} alt=""/> */}
                <div className="tabBar-content">
                    <ul>
                        <li onClick={() => this.toggleBar("0")}>
                            <span className="tabBar-item">
                                <span className="tabbar-img">
                                    <CustomIcon
                                        icon="#iconshouye"
                                        width="22px"
                                        height="22px"
                                        color={
                                            activeIndex === "0"
                                                ? "#00c35d"
                                                : "#333"
                                        }
                                        style={{
                                            fontWeight:'bold'
                                        }}
                                    />
                                </span>
                                <span
                                    className={
                                        activeIndex === "0" ? "active" : ""
                                    }
                                >
                                    首页
                                </span>
                            </span>
                        </li>
                        <li onClick={() => this.toggleBar("1")}>
                            <span className="tabBar-item">
                                <span className="tabbar-img">
                                    <CustomIcon
                                        icon="#iconfenlei1"
                                        width="24px"
                                        height="24px"
                                        color={
                                            activeIndex === "1"
                                                ? "#00c35d"
                                                : "#333"
                                        }
                                    />
                                </span>
                                <span
                                    className={
                                        activeIndex === "1" ? "active" : ""
                                    }
                                >
                                    分类
                                </span>
                            </span>
                        </li>
                        <li onClick={() => this.toggleBar("2")}>
                            <span className="tabBar-item">
                                <span className="tabbar-img">
                                    <img
                                        key="amache" className={['',activeIndex === "2"?'scaleDrawImage':'narrowDrawImage'].join(' ')}
                                        src={require("../../assets/images/common/tabBar/give.png")}
                                        alt=""
                                    />
                                </span>
                            </span>
                        </li>
                        <li onClick={() => this.toggleBar("3")}>
                            <span className="tabBar-item">
                                <span className="tabbar-img">
                                    <CustomIcon
                                        icon="#icongouwuche"
                                        width="22px"
                                        height="22px"
                                        color={
                                            activeIndex === "3"
                                                ? "#00c35d"
                                                : "#333"
                                        }
                                    />
                                </span>
                                <span
                                    className={
                                        activeIndex === "3" ? "active" : ""
                                    }
                                >
                                    购物车
                                </span>
                            </span>
                        </li>
                        <li onClick={() => this.toggleBar("4")}>
                            <span className="tabBar-item">
                                <span className="tabbar-img">
                                    <CustomIcon
                                        icon="#iconmy"
                                        width="22px"
                                        height="22px"
                                        color={
                                            activeIndex === "4"
                                                ? "#00c35d"
                                                : "#333"
                                        }
                                    />
                                </span>
                                <span
                                    className={
                                        activeIndex === "4" ? "active" : ""
                                    }
                                >
                                    我的
                                </span>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { updateAppState } = state;
    return {
        activeIndex: updateAppState.tabBarStatus.activeIndex
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    console.log("ownProps:", ownProps);
    return {
        updateTabBar: (...params) => {
            dispatch(appAction.updateTabBar(...params));
        }
    };
};
const TabBarContainer = connect(mapStateToProps, mapDispatchToProps)(TabBar);
// const TabBarContainer = withRouter(connect(undefined, mapStateToProps, undefined, {pure: false})(TabBar));
export default TabBarContainer;
