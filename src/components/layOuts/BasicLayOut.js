import React from "react";
import "./styles/basicLayOut.scss";
import TabBar from "@/components/LayOuts/TabBar";

class BasicLayOut extends React.Component {
    render() {
        const { children } = this.props;
        console.log("children:", children, this.props);
        return (
            <div className="basicLayOut-wrapper">
                <div className="basicLayOut-content">{children}</div>
                <TabBar {...this.props} />
            </div>
        );
    }
}
export default BasicLayOut;
