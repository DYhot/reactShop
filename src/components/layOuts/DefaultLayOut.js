import React from "react";
import "./styles/defaultLayOut.scss";
import HeaderNavBar from "@/components/LayOuts/HeaderNavBar";

class DefaultLayOut extends React.Component {
    render() {
        const { children ,title,tabKey} = this.props;
        console.log("DefaultLayOut的children:", children, this.props,title);
        return (
            <div className="defaultLayOut-wrapper">
                <div className="defaultLayOut-content">{children}</div>
                {tabKey||<HeaderNavBar {...this.props} title={title}/>}
            </div>
        );
    }
}
export default DefaultLayOut;
