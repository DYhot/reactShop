/**
 * Created by DengYang on 20/01/2019.
 */
import React from "react";
import "./styles/headerNavBar.scss";
// 状态管理
import { connect } from "react-redux";
import appAction from "@/redux/actions/app";
import { NavBar, Icon, Popover, ActionSheet, Toast } from "antd-mobile";
import PropTypes from "prop-types";
import arrayTreeFilter from "array-tree-filter";
const Item = Popover.Item;
const myImg = src => (
    <img
        src={`https://gw.alipayobjects.com/zos/rmsportal/${src}.svg`}
        className="am-icon am-icon-xs"
        alt=""
    />
);

class HeaderNavBar extends React.Component {
    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };
    constructor(pops) {
        super(pops);
        this.state = {
            visible: false,
            selected: "",
            title: "",
            returnUrl: null,
            clicked: "none"
        };
    }
    dataList = [
        { url: "OpHiXAcYzmPQHcdlLFrc", title: "发送给朋友" },
        { url: "wvEzCMiDZjthhAOcwTOu", title: "新浪微博" },
        { url: "cTTayShKtEIdQVEMuiWt", title: "生活圈" },
        { url: "umnHwvEgSyQtXlZjNJTt", title: "微信好友" },
        { url: "SxpunpETIwdxNjcJamwB", title: "QQ" }
    ].map(obj => ({
        icon: (
            <img
                src={`https://gw.alipayobjects.com/zos/rmsportal/${obj.url}.png`}
                alt={obj.title}
                style={{ width: 36 }}
            />
        ),
        title: obj.title
    }));
    componentDidMount() {
        const { location, routes = [] } = this.props;
        const route = this.findMatchedRoute(location.pathname, routes);
        console.log(1234, route, this.props);
        this.setState({
            title: route.length > 0 ? route[0].title : "",
            returnUrl: route.length > 0 ? route[0].returnUrl : null
        });
    }
    goBack() {
        const { returnUrl } = this.state;
        if (returnUrl) {
            this.props.history.push({ pathname: returnUrl });
        } else {
            this.props.history.go(-1);
        }
    }
    onSelect = opt => {
        // console.log(opt.props.value);
        this.setState({
            visible: false,
            selected: opt.props.value
        });
    };
    handleVisibleChange = visible => {
        this.setState({
            visible
        });
    };
    findMatchedRoute(pathname, routes) {
        const value = [pathname];
        const treeChildren = arrayTreeFilter(routes, (c, level) => {
            const index = c.path.indexOf(":");
            if (index > 0 && pathname.startsWith(c.path.substr(0, index))) {
                return c;
            }
            // console.log(222,c.path,value[level],Boolean(c.path === value[level]))
            return c.path === value[level];
        });
        // console.log(111,pathname,treeChildren)
        // returnUrl
        // return treeChildren.length > 0 && treeChildren[0].title;
        return treeChildren;
    }
    showShareActionSheet = () => {
        ActionSheet.showShareActionSheetWithOptions(
            {
                options: this.dataList,
                //   title: '分享给朋友',
                message: "分享给朋友"
            },
            buttonIndex => {
                console.log(111,buttonIndex)
                this.setState({
                    clicked:
                        buttonIndex > -1
                            ? this.dataList[buttonIndex].title
                            : "cancel"
                });
                // also support Promise
                if(buttonIndex > -1){
                    return new Promise(resolve => {
                        resolve();
                        Toast.info("分享成功", 1);
                    });
                }
            }
        );
    };
    render() {
        const { isShow } = this.props;
        const { title } = this.state;
        // const { isShow, location, routes = [] } = this.props;
        // const title = this.getTitle(location.pathname, routes);
        return (
            <div className="header-navBar-Wrapper">
                {isShow && (
                    <NavBar
                        mode="light"
                        icon={<Icon type="left" />}
                        onLeftClick={() => this.goBack()}
                        rightContent={
                            <Icon
                                key="0"
                                type="ellipsis"
                                onClick={() => {
                                    this.showShareActionSheet();
                                }}
                            />
                        }
                    >
                        {title}
                    </NavBar>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let { updateAppState } = state;
    return {
        isShow: updateAppState.navBarStatus.isShow
    };
};
const select = (state, props) => {
    // console.log("变化了", state, props)
    return {
        logBox: state.logBox
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        // toggleHeaderBar: (m) => {
        //     dispatch(changeHeaderTabBar(m));
        // }
    };
};
const HeaderNavBarContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(HeaderNavBar);
export default HeaderNavBarContainer;
