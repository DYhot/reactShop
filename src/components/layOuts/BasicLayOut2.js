import React from "react";
import "./styles/basicLayOut.scss";
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
    Route
} from "react-router-dom";
import Detail from "@/pages/Detail/index";

class BasicLayOut extends React.Component {
    render() {
        const { children } = this.props;
        console.log("children:", children, this.props);
        // return <div className="basicLayOut-wrapper">{children}</div>;
        return (
            <div className="basicLayOut-wrapper">
                <Switch>
                    <Route
                        path={this.props.match.url}
                        component={Test}
                        exact
                    />
                    <Route
                        path={this.props.match.url + "/detail"}
                        component={Detail}
                        exact
                    />
                    <Redirect from="*" to={{ pathname: "/500" }} />
                </Switch>
            </div>
        );
    }
}
export default BasicLayOut;
