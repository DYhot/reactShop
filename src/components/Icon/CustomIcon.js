/**
 * Created by Dengyang on 12/1/2019.
 */
import React, { Fragment } from "react";

class CustomIcon extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { icon, width, height, color } = this.props;
        let iconStyle = {
            width,
            height,
            color,
        };

        return (
            <Fragment>
                <svg className="icon" aria-hidden="true" style={iconStyle}>
                    <use xlinkHref={icon}></use>
                </svg>
            </Fragment>
        );
    }
}
export default CustomIcon;
