/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import BScroll from "better-scroll";
import PropTypes from "prop-types";
import loadingGif from "@/assets/images/common/loading/loading.gif";
import DefaultImage from "@/components/DefaultImage/index";
import CustomIcon from "@/components/Icon/CustomIcon";

class PullRefresh extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            loadingConnecting: false,
            pulldownTip: {
                text: "下拉刷新", // 松开立即刷新
                rotate: "" // icon-rotate
            }
        };
    }

    componentDidMount() {
        // 保证在DOM渲染完毕后初始化better-scroll
        setTimeout(() => {
            this._initScroll();
        }, 20);
    }
    componentDidUpdate() {
        // this.initBscroll();
    }
    _initScroll=()=> {
        if (!this.refs.wrapper) {
            return;
        }
        // better-scroll的初始化
        this.scroll = new BScroll(this.refs.wrapper, {
            probeType: this.probeType,
            click: this.click,
            scrollX: this.scrollX
        });

        // 是否派发滚动事件
        if (this.listenScroll || this.pulldown || this.pullup) {
            let me = this;
            this.scroll.on("scroll", pos => {
                if (this.listenScroll) {
                    me.$emit("scroll", pos);
                }

                if (this.pulldown) {
                    // 下拉动作
                    if (pos.y > 50) {
                        this.pulldownTip = {
                            text: "松开立即刷新",
                            rotate: "icon-rotate"
                        };
                    } else {
                        this.pulldownTip = {
                            text: "下拉刷新", // 松开立即刷新
                            rotate: "" // icon-rotate
                        };
                    }
                }

                if (this.pullup) {
                }
            });
        }

        // 是否派发滚动到底部事件，用于上拉加载
        if (this.pullup) {
            this.scroll.on("scrollEnd", () => {
                console.log("scrollEnd");
                console.log(this.scroll);
                // 滚动到底部
                if (this.scroll.y <= this.scroll.maxScrollY + 50) {
                    this.$emit("scrollToEnd");
                }
            });
        }

        // 是否派发顶部下拉事件，用于下拉刷新
        if (this.pulldown) {
            this.scroll.on("touchend", pos => {
                // 下拉动作
                if (pos.y > 50) {
                    setTimeout(() => {
                        // 重置提示信息
                        this.pulldownTip = {
                            text: "下拉刷新", // 松开立即刷新
                            rotate: "" // icon-rotate
                        };
                    }, 600);
                    this.$emit("pulldown");
                }
            });
        }

        // 是否派发列表滚动开始的事件
        if (this.beforeScroll) {
            this.scroll.on("beforeScrollStart", () => {
                this.$emit("beforeScroll");
            });
        }
    }
    disable=()=> {
        // 代理better-scroll的disable方法
        this.scroll && this.scroll.disable();
    }
    enable=()=> {
        // 代理better-scroll的enable方法
        this.scroll && this.scroll.enable();
    }
    refresh=()=> {
        // 代理better-scroll的refresh方法
        this.scroll && this.scroll.refresh();
    }
    scrollTo=()=> {
        // 代理better-scroll的scrollTo方法
        // this.scroll && this.scroll.scrollTo.apply(this.scroll, arguments);
    }
    scrollToElement=()=> {
        // 代理better-scroll的scrollToElement方法
        // this.scroll &&
        //     this.scroll.scrollToElement.apply(this.scroll, arguments);
    }

    render() {
        const { pulldownTip } = this.state;
        const { showIcon,status } = this.props;
        return (
            <div ref="wrapper" className="better-scroll-root">
                <div className="content-bg better-scroll-container">
                    <div>
                        <div v-if="pulldown" className="pulldown-tip">
                            <i
                                className="pull-icon indexicon icon-pull-down"
                                className={[pulldownTip.rotate]}
                            ></i>
                            <span className="tip-content">
                                {pulldownTip.text}
                            </span>
                        </div>
                        {showIcon ||status ? (
                            <div className="loading-pos">
                                {showIcon ? (
                                    <div className="loading-container">
                                        <div className="cube">
                                            <div className="side side1"></div>
                                            <div className="side side2"></div>
                                            <div className="side side3"></div>
                                            <div className="side side4"></div>
                                            <div className="side side5"></div>
                                            <div className="side side6"></div>
                                        </div>
                                    </div>
                                ) : null}
                                <span className="loading-connecting">
                                    {status}
                                </span>
                            </div>
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}
PullRefresh.defaultprops = {
    probeType: 1,
    click: true,
    scrollX: false,
    listenScroll: false,
    data: null,
    pullup: false,
    pulldown: false,
    beforeScroll: false,
    refreshDelay: 20,
    showIcon: false,
    status: "",
    pulldownUI: false,
    pullupUI: false
};
PullRefresh.propTypes = {
    probeType: PropTypes.number,
    click: PropTypes.bool,
    scrollX: PropTypes.bool,
    listenScroll: PropTypes.number,
    data: PropTypes.array,
    pullup: PropTypes.bool,
    pulldown: PropTypes.bool,
    beforeScroll: PropTypes.bool,
    refreshDelay: PropTypes.number,
    showIcon: PropTypes.bool,
    status: PropTypes.string,
    pulldownUI: PropTypes.bool,
    pullupUI: PropTypes.bool
};
export default PullRefresh;
