import React from "react";
import { Link } from "react-router-dom";
import creatHistory from 'history/createBrowserHistory';
import "./styles/page404.scss";
import Png404 from "@/assets/images/common/error/404.png";
const history = creatHistory();
const Page404 = (props) => {
    return (
        <div className="page404-wrapper">
            <div className="head404">
                <img src={Png404} alt=""/>                
            </div>
            <div className="txtbg404">
                <div className="txtbox">
                    <p>对不起，您请求的页面不存在</p>
                    <p className="paddingbox">请点击以下链接继续浏览网页</p>
                    <p>
                        <span οnClick={()=>{history.goBack()}}>
                            返回上一页面
                        </span>
                    </p>
                    <p>
                        <Link to="/">返回网站首页</Link>
                    </p>
                </div>
            </div>
        </div>
    );
};
export default Page404;
