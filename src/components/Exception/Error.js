import React from "react";
import "./styles/error.scss";

const Error = (error) => {
    console.log(1234,error)
    return (
        <div className="error-wrapper">
            <span>⚠️</span>
            <span>{error.errorInfo.message}</span>
        </div>
    );
};
export default Error;
