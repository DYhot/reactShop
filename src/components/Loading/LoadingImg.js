/**
 * Created by Dengyang on 12/1/2019.
 */
import React from "react";
import "./styles/loadingImg.scss";

class LoadingImg extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            positionY: 0,
            timer: null
        };
    }

    componentDidMount() {
        const timer = setInterval(() => {
            const {positionY}=this.state;
            let calculatePositionY=positionY+1;
            this.setState({
                positionY:calculatePositionY
            })
        }, 600);
        this.setState({
            timer
        })
    }
    componentWillUnmount(){
        const {timer}=this.state;
        clearInterval(timer);
    }
    render() {
        const {positionY}=this.state;
        return (
            <div className="loading_container">
                <div className="load_img"
                    style={{backgroundPositionY:-(positionY%7)*5*10+'px'}}
                >
                </div>
                <svg
                    className="load_ellipse"
                    xmlns="http://www.w3.org/2000/svg"
                    version="1.1"
                >
                    <ellipse
                        cx="26"
                        cy="10"
                        rx="26"
                        ry="10"
                        style={{fill:'#ddd',stroke:'none'}}
                    ></ellipse>
                </svg>
            </div>
        );
    }
}
export default LoadingImg;
