/**
 * Created by Dengyang on 12/1/2019.
 */
import React from "react";
import ReactDOM from "react-dom";
import { LoopCircleLoading } from "react-loadingg";
import "./styles/loading.scss";
const Loading = props => {
    return (
        <div className="loading-container">
            <LoopCircleLoading color="#fff" />
        </div>
    );
};
Loading.show = function(props) {
    if(!this.div){
        this.div = document.createElement("div");
        document.body.appendChild(this.div);
        ReactDOM.render(<Loading {...props} />, this.div);
    }
};
Loading.hide = function() {
    this.div && ReactDOM.unmountComponentAtNode(this.div); //从div中移除已挂载的Loading组件
    this.div && this.div.parentNode.removeChild(this.div); //移除挂载的容器
    this.div=null;
};
export default Loading;
