import React from "react";
import "./styles/loadingCircle.scss";

const Loading = show => {
    return (
        <div className={`loader-layer ${show ? "active" : ""}`}>
            <div className="spinner">
                <div className="double-bounce1"></div>
                <div className="double-bounce2">loading...</div>
                <div className="double-bounce3"></div>
            </div>
        </div>
    );
};
export default Loading;
