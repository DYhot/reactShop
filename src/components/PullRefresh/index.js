/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./styles/index.scss";
import BScroll from "better-scroll";
import DefaultImage from "@/components/DefaultImage";
import loadingGif from "@/assets/images/common/loading/loading.gif";
import CustomIcon from "@/components/Icon/CustomIcon";
import PropTypes from "prop-types";

const TIME_BOUNCE = 500;
const TIME_STOP = 500;
const PULLDOWN_THRESHOLD = 50;
const PULLUP_THRESHOLD = 10;
const STOP = 60;
const PULLDOWN_TIPS = {
    before: "下拉可以刷新",
    pulling: "释放刷新数据",
    loading: "正在刷新数据中...",
    finished: "加载完成"
};
const PULLUP_TIPS = {
    before: "上拉加载更多",
    loading: "加载中..."
};
class PullRefresh extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            rotateArrow: false, //是否旋转箭头
            // pullDown 组件配置
            pullDownConfig: {
                threshold: PULLDOWN_THRESHOLD, // 配置顶部下拉的距离来决定刷新时机
                stop: STOP // 回弹停留的距离
            },
            // pullUp 组件配置
            pullUpConfig: {
                threshold: PULLUP_THRESHOLD // 配置底部上拉的距离来决定刷新时机
            },
            pullDownTips: PULLDOWN_TIPS,
            pullUpTips: PULLUP_TIPS,
            pullDown: props.isPullDown, // 是否支持下拉刷新
            pullUp: props.isPullUp, // 是否支持上拉加载
            zIndex: 100, // z-index 配置
            noMoreTips: "已经到底了",
            // 下拉、上拉操作结束后的停留时间
            stayTime: TIME_STOP,
            beforePullDown: true, // 状态 下拉之前
            isPullingDown: false, // 状态 下拉过程中
            pullDownY: 0,
            isPullUpLoad: false, // 状态 上拉之前
            pullUpLoading: false, // 状态 上拉过程中
            downTipsText: "", //下拉显示的提示文字
            upTipsText: "" //下拉显示的提示文字
        };
        this.bscroll = null;
    }
    static defaultProps = {
        data: [],
        isPullDown: true, // 是否支持下拉刷新
        isPullUp: true, // 是否支持上拉加载
        freshTime: "", // 刷新时间配置
        // 是否无更多数据 - 只有开启Pullup的情况下传
        noMore: false,
        empty: false
    };
    componentDidMount() {}
    componentDidUpdate() {
        this.initBscroll();
    }
    tipsStyle = () => {
        return {
            zIndex: this.state.zIndex
        };
    };
    contentStyle = () => {
        const { zIndex, pullUp } = this.state;
        const { noMore } = this.props;
        return {
            zIndex: zIndex + 1,
            paddingBottom: noMore || pullUp ? "50px" : 0
        };
    };
    showNoMoreTips = () => {
        const { noMore } = this.props;
        const { pullUp, empty } = this.state;
        return noMore && pullUp && !empty;
    };
    pullingDownOut = () => {
        const { pullDownY, pullDownConfig } = this.state;
        return pullDownY > pullDownConfig.threshold || false;
    };
    /**
     * 初始化
     */
    initBscroll = () => {
        if (this.bscroll) {
            return;
        }
        const { pullDown, pullUp, pullDownConfig, pullUpConfig } = this.state;
        const config = {
            scrollY: true,
            bounce: {
                top: pullDown ? true : false,
                bottom: pullUp ? true : false
                // bottom: false
            },
            bounceTime: TIME_BOUNCE,
            pullDownRefresh: pullDown ? pullDownConfig : false,
            pullUpLoad: pullUp ? pullUpConfig : false
        };
        this.bscroll = new BScroll(this.refs.PullRefreshWrapper, config);
        this.bscroll.on("scroll", this.scrollHandler);
        if (pullDown) {
            this.bscroll.on("pullingDown", this.pullingDownHandler);
        }
        if (pullUp) {
            this.bscroll.on("pullingUp", this.pullingUpHandler);
        }
    };
    /**
     * 更新better scroll的dom
     */
    refreshDom = () => {
        return new Promise((resolve, reject) => {
            this.bscroll.refresh();
            resolve();
        });
    };
    /**
     * 下拉滚动持续触发
     * @param pos 滚动过程中的坐标
     */
    scrollHandler = pos => {
        // console.log("滚动过程中的坐标:",pos,this.bscroll.movingDirectionY);
        this.setState({
            pullDownY: pos.y,
            rotateArrow: pos.y > 40 ? true : false
        });
    };

    /**
     * 下拉滚动结束触发
     */
    pullingDownHandler = async () => {
        const { beforePullDown, isPullingDown } = this.state;
        if (!beforePullDown && isPullingDown) {
            return;
        }
        this.setState(
            {
                beforePullDown: false,
                isPullingDown: true
            },
            async () => {
                // 触发刷新操作
                await this.refeashData();
                // 下拉过程结束，进入回弹状态
                this.setState(
                    {
                        isPullingDown: false
                    },
                    () => {
                        this.finishPullDown();
                    }
                );
            }
        );
    };
    /**
     * 下拉触发pullDown事件
     */
    refeashData = async () => {
        return new Promise((resolve, reject) => {
            // this.$emit("pullDown", resolve);
            this.props.pullDown().then(res => {
                resolve();
            });
        });
    };
    /**
     * 上拉触发pullUp事件
     */
    getMore = () => {
        const { noMore } = this.props;
        if (noMore) {
            return;
        }
        return new Promise((resolve, reject) => {
            // this.$emit("pullUp", resolve);
            this.props.pullUp().then(res => {
                resolve();
            });
        });
    };
    /**
     * 下拉结束后调用的方法，用于处理停留时间等
     */
    finishPullDown = async () => {
        const stopTime = this.state.stayTime;
        await new Promise(resolve => {
            setTimeout(() => {
                this.bscroll.finishPullDown();
                resolve();
            }, stopTime);
        });
        setTimeout(() => {
            this.setState(
                {
                    beforePullDown: true,
                    rotateArrow: false
                },
                () => {
                    this.bscroll.refresh();
                }
            );
        }, TIME_BOUNCE);
    };
    /**
     * 上拉触发的事件
     */
    pullingUpHandler = async () => {
        const { noMore, empty } = this.props;
        // if (noMore || empty) return false;
        this.setState(
            {
                isPullUpLoad: true,
                pullUpLoading: true
            },
            async () => {
                await this.getMore();
                this.setState(
                    {
                        pullUpLoading: false
                    },
                    () => {
                        this.bscroll.finishPullUp();
                        this.bscroll.refresh();
                        this.setState({
                            isPullUpLoad: false
                        });
                    }
                );
            }
        );
    };
    /**
     * 上拉提示文字
     */
    upTips = () => {
        const { pullUp, isPullUpLoad, pullUpLoading } = this.state;
        const { noMore, empty } = this.props;
        if (!pullUp || empty) return null;
        const upTips = this.state.pullUpTips;
        let tips = "";
        if (noMore) {
            tips = this.state.noMoreTips;
        } else {
            tips =
                !isPullUpLoad && !pullUpLoading ? (
                    upTips.before
                ) : (
                    <span className="loading_gif">
                        <DefaultImage src={loadingGif} />
                        <span>{upTips.loading}</span>
                    </span>
                );
        }
        return <p className="PullRefresh_upTips">{tips}</p>;
    };
    /**
     * 下拉提示文字
     */
    downTips = () => {
        const {
            pullDown,
            pullDownTips,
            beforePullDown,
            isPullingDown
        } = this.state;
        const { freshTime, empty } = this.props;
        if (!pullDown) return null;
        const downTipsConfig = pullDownTips;
        let tips = "";
        let showLoading = false;
        if (beforePullDown && !isPullingDown) {
            tips = this.pullingDownOut()
                ? downTipsConfig.pulling
                : downTipsConfig.before;
        } else {
            showLoading = isPullingDown ? false : true;
            tips = isPullingDown
                ? downTipsConfig.loading
                : downTipsConfig.finished;
        }
        return (
            <div className="PullRefresh_downTips" style={this.tipsStyle()}>
                <div className="left_symbol">
                    {showLoading ? (
                        <DefaultImage src={loadingGif} />
                    ) : !empty ? (
                        <span
                            className={
                                this.state.rotateArrow
                                    ? "rotate_arrow"
                                    : "reset_arrow"
                            }
                        >
                            <CustomIcon
                                icon="#iconxiajiantou"
                                width="25px"
                                height="25px"
                                color="#fff"
                            />
                        </span>
                    ) : null}
                </div>
                <div className="right_text">
                    {!empty ? tips : null}
                    {freshTime ? (
                        <p>
                            <span>
                                最后更新:今天 {freshTime}
                                {this.state.rotateArrow}
                            </span>
                        </p>
                    ) : null}
                </div>
            </div>
        );
    };

    render() {
        const { empty } = this.props;
        return (
            <div className="PullRefreshComponent">
                <div className="PullRefresh">
                    <div
                        className="PullRefresh_wrapper"
                        ref="PullRefreshWrapper"
                    >
                        <div
                            className="PullRefresh_content"
                            style={this.contentStyle()}
                        >
                            {empty ? (
                                <div className="empty_wrapper">
                                    <span className="icon_wrapper">
                                        <CustomIcon
                                            icon="#iconzanwushuju"
                                            width="45px"
                                            height="45px"
                                            color="#999"
                                        />
                                        <p>暂无数据</p>
                                    </span>
                                </div>
                            ) : (
                                this.props.children
                            )}
                            {this.showNoMoreTips() && (
                                <div className="PullRefresh_noMore">
                                    {this.noMoreTips}
                                </div>
                            )}
                            {this.upTips()}
                        </div>
                        {this.downTips()}
                    </div>
                </div>
            </div>
        );
    }
}
PullRefresh.propTypes = {
    data: PropTypes.array, //数据源
    isPullDown: PropTypes.bool, // 是否支持下拉刷新
    isPullUp: PropTypes.bool, // 是否支持上拉加载
    freshTime: PropTypes.string, // 刷新时间配置
    // 是否无更多数据 - 只有开启Pullup的情况下传
    noMore: PropTypes.bool,
    empty: PropTypes.bool
};
export default PullRefresh;
