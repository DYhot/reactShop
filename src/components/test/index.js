/**
 * Created by Dengyang on 12/1/2019.
 */
import React from 'react'
import { Link} from 'react-router-dom'
// 状态管理
import { connect } from 'react-redux'
import {changeHeaderTabBar} from '../../redux/actions/tabbar'

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: true
        }
    }

    componentDidMount() {
        console.log(789,this.props)
    }

    clickToggle() {
        this.setState({
            active: arguments[0]
        })
    }

    render() {
        const {currentHeaderBar,toggleHeaderBar}=this.props;
        return (
            <div className="select-movies-Wrapper">
                <div className="select-movies-header-Wrapper">
                    <ul>
                        <li onClick={() => toggleHeaderBar(true)}
                            className={currentHeaderBar ? 'nav-item-active' : ''}>
                            <Link to="/selectmovies" className="navList">
                                精选
                            </Link>
                        </li>
                        <li onClick={() => toggleHeaderBar(false)}
                            className={currentHeaderBar? '' : 'nav-item-active'}>
                            <Link to="/viewmovies" className="navList">
                                看片
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    let {updateTabState} = state;
    return {
        currentHeaderBar: updateTabState.headerTabBarState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        toggleHeaderBar: (m) => {
            dispatch(changeHeaderTabBar(m));
        }
    }
};
const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);
export default HeaderContainer;

