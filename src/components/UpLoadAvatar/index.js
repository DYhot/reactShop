/**
 * Created by DengYang Li on 04/05/2019.
 */
import React from "react";
import "./index.scss";
//引入Cropper图片裁剪组件
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import Loading from "@/components/Loading/Loading";

class UpLoadAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayLoading: false,
            cropperData: "",
            showCropModal: false,
            title: " "
        };
        this.fReader = new FileReader();
        this.closureTime = 0;
    }

    componentDidMount() {
        this.props.onRef(this);
    }
    /**
     * input onChange事件
     * @param e
     * @return
     */
    onChange = e => {
        //此处是崩溃点 相机调用的频率越高，崩溃越快
        //弹出加载动画
        this.openLoading();
        let file = e.currentTarget.files[0]; //object-Blob //96K 的文件转换成 base64 是 130KB
        //用户取消操作
        if (file == undefined) {
            return;
        }
        this.fReader = new FileReader();
        let tempTimer = setTimeout(() => {
            this.fReader.readAsDataURL(file);
            this.fReader.onload = e => {
                this.zip(e.target.result); //压缩逻辑
            };
            file = null;
            tempTimer = null;
            // }, 500);
        }, 0);
    };

    /**
     * 显示loading组件
     * @param
     * @return
     */
    openLoading = () => {
        this.setState({
            displayLoading: true,
            showCropModal: true
        });
    };
    /**
     * 图片压缩
     * @param base64
     * @return
     */
    zip = base64 => {
        let img = new Image();
        let canvas = document.createElement("canvas");
        let ctx = canvas.getContext("2d");
        let compressionRatio = 0.5;
        //获取用户拍摄图片的旋转角度
        let orientation = this.getOrientation(this.base64ToArrayBuffer(base64)); //1 0°  3 180°  6 90°  8 -90°
        img.src = base64;
        img.onload = () => {
            let width = img.width,
                height = img.height;
            //图片旋转到 正向
            if (orientation == 3) {
                canvas.width = width;
                canvas.height = height;
                ctx.rotate(Math.PI);
                ctx.drawImage(img, -width, -height, width, height);
            } else if (orientation == 6) {
                canvas.width = height;
                canvas.height = width;
                ctx.rotate(Math.PI / 2);
                ctx.drawImage(img, 0, -height, width, height);
            } else if (orientation == 8) {
                canvas.width = height;
                canvas.height = width;
                ctx.rotate(-Math.PI / 2);
                ctx.drawImage(img, -width, 0, width, height);
            } else {
                //不旋转原图
                canvas.width = width;
                canvas.height = height;
                ctx.drawImage(img, 0, 0, width, height);
            }

            //第一次粗压缩
            let base64 = canvas.toDataURL("image/jpeg", compressionRatio); //0.1-表示将原图10M变成1M 10-表示将原图1M变成10M
            //100保证图片容量 0.05保证不失真
            console.log(
                "第一次粗压缩",
                base64.length / 1024,
                "kb，压缩率",
                compressionRatio
            );
            //第二次细压缩
            while (base64.length / 1024 > 500 && compressionRatio > 0.01) {
                console.log("while");
                // compressionRatio -= 0.01;
                compressionRatio -= 0.1;
                base64 = canvas.toDataURL("image/jpeg", compressionRatio); //0.1-表示将原图10M变成1M 10-表示将原图1M变成10M
                console.log(
                    "第二次细压缩",
                    base64.length / 1024,
                    "kb，压缩率",
                    compressionRatio
                );
            }
            this.setCropperDate(
                canvas.toDataURL("image/jpeg", compressionRatio)
            );
        };
    };

    /**
     * 拍照第一次压缩后为cropper组件赋值
     * @param imgDataBase64 图片的base64
     * @return
     */
    setCropperDate = imgDataBase64 => {
        this.state.cropperData = imgDataBase64;
        //定时器的作用，上面的imgDataBase64赋值，属于大数据赋值操作，消耗资源过大，加上定时器等待大数据赋值成功内存释放之后再渲染UI，不会出现白屏
        let tempTimer = setTimeout(() => {
            this.setState({
                displayLoading: false,
                showCropModal: true
            });
            clearTimeout(tempTimer);
            // }, 300);
        }, 0);
    };

    /**
     * base64转ArrayBuffer对象
     * @param base64
     * @return buffer
     */
    base64ToArrayBuffer = base64 => {
        base64 = base64.replace(/^data\:([^\;]+)\;base64,/gim, "");
        var binary = atob(base64);
        var len = binary.length;
        var buffer = new ArrayBuffer(len);
        var view = new Uint8Array(buffer);
        for (var i = 0; i < len; i++) {
            view[i] = binary.charCodeAt(i);
        }
        return buffer;
    };
    /**
     * 获取jpg图片的exif的角度
     * @param
     * @return
     */
    getOrientation = arrayBuffer => {
        var dataView = new DataView(arrayBuffer);
        var length = dataView.byteLength;
        var orientation;
        var exifIDCode;
        var tiffOffset;
        var firstIFDOffset;
        var littleEndian;
        var endianness;
        var app1Start;
        var ifdStart;
        var offset;
        var i;
        // // Only handle JPEG image (start by 0xFFD8)
        if (dataView.getUint8(0) === 0xff && dataView.getUint8(1) === 0xd8) {
            offset = 2;
            while (offset < length) {
                if (
                    dataView.getUint8(offset) === 0xff &&
                    dataView.getUint8(offset + 1) === 0xe1
                ) {
                    app1Start = offset;
                    break;
                }
                offset++;
            }
        }
        if (app1Start) {
            exifIDCode = app1Start + 4;
            tiffOffset = app1Start + 10;
            if (
                this.getStringFromCharCode(dataView, exifIDCode, 4) === "Exif"
            ) {
                endianness = dataView.getUint16(tiffOffset);
                littleEndian = endianness === 0x4949;
                if (littleEndian || endianness === 0x4d4d /* bigEndian */) {
                    if (
                        dataView.getUint16(tiffOffset + 2, littleEndian) ===
                        0x002a
                    ) {
                        firstIFDOffset = dataView.getUint32(
                            tiffOffset + 4,
                            littleEndian
                        );
                        if (firstIFDOffset >= 0x00000008) {
                            ifdStart = tiffOffset + firstIFDOffset;
                        }
                    }
                }
            }
        }
        if (ifdStart) {
            length = dataView.getUint16(ifdStart, littleEndian);
            for (i = 0; i < length; i++) {
                offset = ifdStart + i * 12 + 2;
                if (
                    dataView.getUint16(offset, littleEndian) ===
                    0x0112 /* Orientation */
                ) {
                    // 8 is the offset of the current tag's value
                    offset += 8;
                    // Get the original orientation value
                    orientation = dataView.getUint16(offset, littleEndian);
                    // Override the orientation with its default value for Safari (#120)
                    if (true) {
                        dataView.setUint16(offset, 1, littleEndian);
                    }
                    break;
                }
            }
        }
        return orientation;
    };

    /**
     * Unicode码转字符串  ArrayBuffer对象 Unicode码转字符串
     * @param
     * @return
     */
    getStringFromCharCode = (dataView, start, length) => {
        var str = "";
        var i;
        for (i = start, length += start; i < length; i++) {
            str += String.fromCharCode(dataView.getUint8(i));
        }
        return str;
    };
    /**
     * 无线逆时针旋转图片
     * @param
     * @return
     */
    rotateCrop = () => {
        this.refs.cropper.rotate(-90);
    };

    /**
     * 在裁剪组件中确认裁剪
     * @param
     * @return
     */
    confirmCrop = () => {
        //节流
        if (Date.now() - this.closureTime < 2000) {
            return;
        }
        this.closureTime = Date.now();
        document.getElementById("cropModal").style.visibility = "hidden";
        // this.setState({
        //     displayLoading: true
        // });
        let tempTimer = setTimeout(() => {
            //获取裁剪后的图片base64 向服务器传递500KB以内的图片
            let compressionRatio = 0.5;
            let cropperData = this.refs.cropper
                .getCroppedCanvas()
                .toDataURL("image/jpeg", compressionRatio);
            while (cropperData.length / 1024 > 500 && compressionRatio > 0.1) {
                compressionRatio -= 0.1;
                cropperData = this.refs.cropper
                    .getCroppedCanvas()
                    .toDataURL("image/jpeg", compressionRatio);
            }
            this.state.cropperData = null;
            this.refs.cropper.clear(); //去除裁剪框
            //this.refs.cropper.destroy();//需要修改npm包
            this.props.upload(cropperData); //向服务器提交base64图片数据
            cropperData = null;
            //必须先拿到cropper数据 关闭裁剪框 显示加载框
            this.setState({ showCropModal: false });
            clearTimeout(tempTimer);
            // }, 300);
        }, 0);
    };

    /**
     * 在裁剪组件中取消裁剪
     * @param
     * @return
     */
    cancelCrop = () => {
        this.state.cropperData = null;
        this.refs.cropper.clear();
        this.setState({
            showCropModal: false
        });
    };
    /**
     * 触发input[type="file"]选择图片
     * @param
     * @return
     */
    selectImage = () => {
        this.refs.onChange.dispatchEvent(new MouseEvent("click"));
    };
    render() {
        return (
            <div id="testPage" className="contianer">
                {/*图片裁剪组件*/}
                {this.state.showCropModal ? (
                    <div className="cropModal" id="cropModal">
                        <Cropper
                            className="crop"
                            ref="cropper"
                            src={this.state.cropperData}
                            style={{ maxHeight: "100%", width: "100%" }}
                            //0-默认-没有任何限制 1-限制裁剪框不超过canvas画布边缘 2-如果是长图-限制图片不超过cropper的最高可视区域-同时裁剪框不超过canvas画布边缘
                            viewMode={2}
                            dragMode="none"
                            minCanvasWidth={285}
                            //隐藏棋盘背景色
                            background={false}
                            //裁剪框内部的横竖虚线可见
                            guides={false}
                            //裁剪框内部的十字线可见
                            center={false}
                            //可旋转原图
                            rotatable={true}
                            //可缩放原图
                            scalable={true}
                            // 这个是设置比例的参数
                            aspectRatio={1}
                            // crop={(e)=>{this.crop(e)}}
                        />
                        <div className="btn">
                            <div
                                className="cropperBtn"
                                onClick={this.cancelCrop}
                            >
                                取消
                            </div>
                            <div
                                className="cropperBtn"
                                onClick={this.confirmCrop}
                            >
                                确认
                            </div>
                            <div
                                className="cropperBtn"
                                onClick={this.rotateCrop}
                            >
                                旋转
                            </div>
                        </div>
                    </div>
                ) : null}
                {this.state.displayLoading ? Loading.show() : Loading.hide()}
                <input
                    type="file"
                    onChange={(e)=>{this.onChange(e)}}
                    className="getImg"
                    title="title"
                    hidden="hidden"
                    id="fileinput"
                    ref="onChange"
                    accept="image/*"
                />
            </div>
        );
    }
}

export default UpLoadAvatar;
